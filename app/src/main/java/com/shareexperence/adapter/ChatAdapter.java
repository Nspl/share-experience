package com.shareexperence.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.shareexperence.R;
import com.shareexperence.Utils.CommonUtils;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Gettersetter;

import java.util.ArrayList;


public class ChatAdapter extends BaseAdapter {
	LayoutInflater inflator;
	Context mContext;
	 ArrayList<Gettersetter> arrlist;
	Dialog   dialogItem;
	PlaceHolder holder;
	int ego ;
	public ChatAdapter(Context c, ArrayList<Gettersetter> arrChat) {
		// TODO Auto-generated constructor stub
		this.mContext=c;
		inflator=LayoutInflater.from(mContext);
		this.arrlist=arrChat;
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrlist.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arrlist.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public int getItemViewType(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		// TODO Auto-generated method stub

		if(convertView==null){
			convertView=inflator.inflate(R.layout.adap_singlechat, null);
			holder=new PlaceHolder();


			holder.llrecieveimage=(RelativeLayout)convertView.findViewById(R.id.llrecieveimage);
			holder.llsentimage=(RelativeLayout)convertView.findViewById(R.id.llsentimage);


			holder.imagersent=(ImageView)convertView.findViewById(R.id.imagersent);
			holder.imagerecive=(ImageView)convertView.findViewById(R.id.imagerecive);

			holder.datereciver=(TextView) convertView.findViewById(R.id.datereciver);
			holder.datemy=(TextView) convertView.findViewById(R.id.datemy);
			
			convertView.setTag(holder);
		
			
		}
		else{
			holder=(PlaceHolder)convertView.getTag();
		}




		if(arrlist.get(position).getId().equals(GeneralValues.get_user_id((Activity)mContext))){

				holder.llrecieveimage.setVisibility(View.VISIBLE);
				holder.llsentimage.setVisibility(View.GONE);


			CommonUtils.getDisplayImage(mContext, arrlist.get(position).getStrimage(), holder.imagerecive);

			holder.datereciver.setText(arrlist.get(position).getDate().replace(" ", "\n"));

		}else{
				holder.llrecieveimage.setVisibility(View.GONE);
				holder.llsentimage.setVisibility(View.VISIBLE);




			CommonUtils.getDisplayImage(mContext, arrlist.get(position).getStrimage(), holder.imagersent  );
			holder.datemy.setText(arrlist.get(position).getDate().replace(" ", "\n"));

		}


		holder.imagerecive.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent intent = new Intent(
						Intent.ACTION_VIEW,
						Uri.parse(arrlist.get(position).getFilevideo()));
				intent.setDataAndType(Uri
								.parse(arrlist.get(position).getFilevideo()),
						"video/mp4");
				mContext.startActivity(intent);


			}
		});

		holder.imagersent.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent intent = new Intent(
						Intent.ACTION_VIEW,
						Uri.parse(arrlist.get(position).getFilevideo()));
				intent.setDataAndType(Uri
								.parse(arrlist.get(position).getFilevideo()),
						"video/mp4");
				mContext.startActivity(intent);

			}
		});




//---------------for image--------
	/*	if(!arrlist.get(position).getId().equals(GeneralValues.get_userid((Activity)mContext))){
			holder.llrecieveimage.setVisibility(View.VISIBLE);
			holder.llsentimage.setVisibility(View.GONE);



			CommonUtilities.getDisplayImage(mContext, arrlist.get(position).getChatimage(), holder.imagerecive);
 			CommonUtilities.getDisplayImage(mContext, arrlist.get(position).getStrimage(), holder.imgRecieveuser);
		}else{
			holder.llrecieveimage.setVisibility(View.GONE);
			holder.llsentimage.setVisibility(View.VISIBLE);


			CommonUtilities.getDisplayImage(mContext, arrlist.get(position).getChatimage(), holder.imagersent);

			CommonUtilities.getDisplayImage(mContext, arrlist.get(position).getStrimage(), holder.imgSentuser);
		}

*/




		return convertView;
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return getCount();
	}

	public class PlaceHolder{
		ImageView imgRecieve,imgSent,imgSentuser,imagersent,imagerecive,imgRecieveuser;
		RelativeLayout llSent,llRecieve,llrecieveimage,llsentimage;
		TextView datereciver,datemy;
	}

/*
	*/


}