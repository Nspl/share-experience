package com.shareexperence.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.shareexperence.R;


public class MyCustomAdapter extends ArrayAdapter<String> {

	String[] objects;

	public MyCustomAdapter(Context context, int textViewResourceId,
			String[] objects) {
		super(context, textViewResourceId, objects);
		this.objects = objects;

		// TODO Auto-generated constructor stub
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View v = null;
		// If this is the initial dummy entry, make it hidden
		if (position == 0) {
			TextView tv = new TextView(getContext());
			tv.setHeight(0);
			tv.setVisibility(View.GONE);
			v = tv;
		} else {
			// Pass convertView as null to prevent reuse of special case views
			v = super.getDropDownView(position, null, parent);
		}
		// Hide scroll bar because it appears sometimes unnecessarily, this does
		// not prevent scrolling
		parent.setVerticalScrollBarEnabled(false);
		return v;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		return getCustomView(position, convertView, parent);
	}

	public View getCustomView(final int position, View convertView,
			ViewGroup parent) {
		// TODO Auto-generated method stub
		// return super.getView(position, convertView, parent);
		LayoutInflater inflater = LayoutInflater.from(getContext());
		View row = inflater.inflate(R.layout.spinner_text, parent, false);
		TextView label = (TextView) row.findViewById(R.id.text);
		label.setText(objects[position]);
		label.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

			}
		});
		return row;
	}
}