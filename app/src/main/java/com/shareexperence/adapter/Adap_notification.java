package com.shareexperence.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.shareexperence.R;
import com.shareexperence.Utils.CommonUtils;
import com.shareexperence.Utils.Gettersetter;

import java.util.ArrayList;


public class Adap_notification extends BaseAdapter  {
	int flag=0,ePos;
	ViewHolder view;
	Context context;
	ArrayList<Gettersetter> arrfeed;
	Dialog dialogItem;
	Animation bounceAnimation,listAnimation;
	String c;

	public Adap_notification(Context context, ArrayList<Gettersetter> arrfeed) {
		this.context = context;
	 	this.arrfeed = arrfeed;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		//return arrstate.size();
		return arrfeed.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arrfeed.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(final int arg0, View convertView, ViewGroup arg2) {
		ViewHolder holder = null;

		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {

			convertView = mInflater.inflate(R.layout.adap_notification, null);
			holder = new ViewHolder();


		 	holder.profile_image = (ImageView)convertView.findViewById(R.id.profile_image);


			holder.time= (TextView) convertView.findViewById(R.id.time);
			holder.notitext= (TextView) convertView.findViewById(R.id.notitext);

 			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		try {

			long time=Long.parseLong(arrfeed.get(arg0).getDate())*1000;
			c= CommonUtils.getTWTimeString(time);


		} catch (Exception e) {
			// TODO: handle exception
		}

		try {

			if (arrfeed.get(arg0).getLabel().equalsIgnoreCase("submitagreement")) {

				//holder.notitext.setText(arrfeed.get(arg0).getStrName()+" diliked your "+arrfeed.get(arg0).getStrtittle() + " review");

	/*String colorText= arrfeed.get(arg0).getStrName() +" sent " +"<font color=\"#FF0000\"><bold>"	+ arrfeed.get(arg0).getStrtittle()	+ "</bold></font>"
 						+ " Agreement to you. " ;*/


				holder.notitext.setText(arrfeed.get(arg0).getTag());
				CommonUtils.getDisplayImage(context, arrfeed.get(arg0).getStrimage(), holder.profile_image);
				holder.time.setText(c);


			} else if (arrfeed.get(arg0).getLabel().equalsIgnoreCase("sendmessage")) {

				//holder.notitext.setText(arrfeed.get(arg0).getStrName()+" diliked your "+arrfeed.get(arg0).getStrtittle() + " review");

				/*String colorText= arrfeed.get(arg0).getStrName()  +"<font color=\"#FF0000\"><bold>"	+" Sent "	+ "</bold></font>"
						+ "  you Message. " ;
*/

				holder.notitext.setText(arrfeed.get(arg0).getTag());
				CommonUtils.getDisplayImage(context, arrfeed.get(arg0).getStrimage(), holder.profile_image);
				holder.time.setText(c);


			}else if(arrfeed.get(arg0).getLabel().equalsIgnoreCase("updateagreement")){

			/*	String colorText= arrfeed.get(arg0).getStrName() +" sent Update " +"<font color=\"#FF0000\"><bold>"	+ arrfeed.get(arg0).getStrtittle()	+ "</bold></font>"
						+ " Agreement to you. " ;*/


				holder.notitext.setText(arrfeed.get(arg0).getTag());
				CommonUtils.getDisplayImage(context, arrfeed.get(arg0).getStrimage(), holder.profile_image);
				holder.time.setText(c);

			}else if(arrfeed.get(arg0).getLabel().equalsIgnoreCase("acceptagreement")){

			/*	String colorText= arrfeed.get(arg0).getStrName() +" sent Update " +"<font color=\"#FF0000\"><bold>"	+ arrfeed.get(arg0).getStrtittle()	+ "</bold></font>"
						+ " Agreement to you. " ;*/


				holder.notitext.setText(arrfeed.get(arg0).getTag());
				CommonUtils.getDisplayImage(context, arrfeed.get(arg0).getStrimage(), holder.profile_image);
				holder.time.setText(c);

			}else if(arrfeed.get(arg0).getLabel().equalsIgnoreCase("rejectagreement")){

			/*	String colorText= arrfeed.get(arg0).getStrName() +" sent Update " +"<font color=\"#FF0000\"><bold>"	+ arrfeed.get(arg0).getStrtittle()	+ "</bold></font>"
						+ " Agreement to you. " ;*/


				holder.notitext.setText(arrfeed.get(arg0).getTag());
				CommonUtils.getDisplayImage(context, arrfeed.get(arg0).getStrimage(), holder.profile_image);
				holder.time.setText(c);

			}





		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		//holder.tv_name.setText(list.get(arg0));
		return convertView;
		
	 
	}
	
	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return getCount();
	}

	class ViewHolder {

		public ImageView profile_image ;


		public TextView notitext ,time ;

	}


}