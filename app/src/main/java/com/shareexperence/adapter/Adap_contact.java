package com.shareexperence.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.shareexperence.R;
import com.shareexperence.Utils.Gettersetter;
import com.volokh.danylo.video_player_manager.manager.PlayerItemChangeListener;
import com.volokh.danylo.video_player_manager.manager.SingleVideoPlayerManager;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.meta.MetaData;
import com.volokh.danylo.video_player_manager.ui.SimpleMainThreadMediaPlayerListener;
import com.volokh.danylo.video_player_manager.ui.VideoPlayerView;

import java.util.ArrayList;


public class Adap_contact extends BaseAdapter  {
	int flag=0,ePos;
	ViewHolder view;
	Context context;
	ArrayList<Gettersetter> arrfeed;
	Dialog dialogItem;
	Animation bounceAnimation,listAnimation;


	VideoPlayerView player_detail;
	private VideoPlayerManager<MetaData> mVideoPlayerManager = new SingleVideoPlayerManager(new PlayerItemChangeListener() {
		@Override
		public void onPlayerItemChanged(MetaData metaData) {

		}
	});


	public Adap_contact(Context context, ArrayList<Gettersetter> arrfeed) {
		this.context = context;
		this.arrfeed = arrfeed;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		 return arrfeed.size();

	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arrfeed.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(final int arg0, View convertView, ViewGroup arg2) {
		ViewHolder holder = null;

		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {

			convertView = mInflater.inflate(R.layout.adap_contact, null);
			holder = new ViewHolder();

			holder.progress= (ProgressBar) convertView.findViewById(R.id.progress);
 			holder.Name = (TextView) convertView.findViewById(R.id.Username);
 			holder.Tittle= (TextView) convertView.findViewById(R.id.Tittle);

 			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		try {

			 try {




				player_detail.addMediaPlayerListener(new SimpleMainThreadMediaPlayerListener() {
					@Override
					public void onVideoPreparedMainThread() {
						// We hide the cover when video is prepared. Playback is about to start


						Log.e("preparing video", "prepare");
						try {



						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onVideoStoppedMainThread() {
						// We show the cover when video is stopped

						Log.e("Stoped video", "Stoped");
						try {


						} catch (Exception e) {
							e.printStackTrace();
						}


					}

					@Override
					public void onBufferingUpdateMainThread(int percent) {
						super.onBufferingUpdateMainThread(percent);

						Log.e("buffering----", "bufeer");
						try {



						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onVideoCompletionMainThread() {
						// We show the cover when video is completed

						Log.e("completddd----", "compltetttt");

						try {



						} catch (Exception e) {
							e.printStackTrace();
						}


					}
				});

				mVideoPlayerManager.playNewVideo(null, player_detail, arrfeed.get(arg0).getFilevideo());

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}


		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		//holder.tv_name.setText(list.get(arg0));
		return convertView;
		
	 
	}
	
	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return getCount();
	}

	class ViewHolder {

        public ProgressBar progress ;
		public VideoPlayerView player_detail;
        public RelativeLayout main ;
		public TextView Name  ,Tittle;

	}


}