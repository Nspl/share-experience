package com.shareexperence.adapter;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.desarrollodroide.libraryfragmenttransactionextended.FragmentTransactionExtended;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.rey.material.app.BottomSheetDialog;
import com.shareexperence.R;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Gettersetter;
import com.shareexperence.Utils.Globals;
import com.shareexperence.Utils.Methods;
import com.shareexperence.activites.ActivityDdexpActivity;
import com.shareexperence.activites.MainActivity;
import com.shareexperence.activites.MessageCamera;
import com.shareexperence.activites.Messages;
import com.shareexperence.activites.Profile;
import com.shareexperence.fragments.ContactFragment;
import com.shareexperence.fragments.MyprofileFragment;
import com.shareexperence.fragments.ProfileFragment;
import com.shareexperence.redcontact.RedcontactFragment;
import com.volokh.danylo.video_player_manager.manager.PlayerItemChangeListener;
import com.volokh.danylo.video_player_manager.manager.SingleVideoPlayerManager;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.meta.MetaData;
import com.volokh.danylo.video_player_manager.ui.MediaPlayerWrapper;
import com.volokh.danylo.video_player_manager.ui.SimpleMainThreadMediaPlayerListener;
import com.volokh.danylo.video_player_manager.ui.VideoPlayerView;

import java.util.ArrayList;

public class ViewPagerAdapter extends PagerAdapter implements ContactFragment.Positionvideo {
    // Declare Variables
    Context context;
    ProgressBar progress;


    RelativeLayout main;
    TextView Name, Tittle;
    ArrayList<String> flag;
    LayoutInflater inflater;
    DisplayImageOptions options;
    ArrayList<Gettersetter> arrfeed;
    Fragment fragment;
    ImageView tumbnail, experience_txt, profile, home, msg, contact;
    CountDownTimer Timer;
    Bundle bundle;
    private static int counter = 0;
    private String stringVal;
    String Ttittle, nName, Uuserid, Thumb, Eeventid, Vvideo;
    Uri myUri;

    BottomSheetDialog mBottomSheetDialog;

    ProgressBar dot_progress_bar;
    VideoPlayerView player_detail;
    MediaPlayerWrapper mMediaPlayer;
    AudioManager audioManager;

    Handler handler;

    EasyVideoPlayer player;


    public VideoPlayerManager<MetaData> mVideoPlayerManager = new SingleVideoPlayerManager(new PlayerItemChangeListener() {
        @Override
        public void onPlayerItemChanged(MetaData metaData) {

        }
    });


    public ViewPagerAdapter(Context context, ArrayList<Gettersetter> arrfeed) {

        this.context = context;
        this.arrfeed = arrfeed;
        ContactFragment.setmListener(this);


    }

    public ViewPagerAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return arrfeed.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    public float getPageWidth(int position) {
        return 1.0f;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.adap_contact, container,
                false);

        dot_progress_bar = (ProgressBar) itemView.findViewById(R.id.dot_progress_bar);
        Name = (TextView) itemView.findViewById(R.id.Username);
        Tittle = (TextView) itemView.findViewById(R.id.Tittle);
        player_detail = (VideoPlayerView) itemView.findViewById(R.id.player_detail);
        //    player = (EasyVideoPlayer) itemView.findViewById(R.id.player);

        main = (RelativeLayout) itemView.findViewById(R.id.main);
        experience_txt = (ImageView) itemView.findViewById(R.id.experience_txt);
        profile = (ImageView) itemView.findViewById(R.id.profile);
        home = (ImageView) itemView.findViewById(R.id.home);
        msg = (ImageView) itemView.findViewById(R.id.msg);

        contact = (ImageView) itemView.findViewById(R.id.contact);

        handler = new Handler();


/*        player.disableControls();
        player.hideControls();
        player.setCallback(this);*/

        Getvideoplayer_Values() ;


        ((ViewPager) container).addView(itemView);


        if (GeneralValues.get_trial((Activity) context).equalsIgnoreCase("0") && position == 0) {

            try {

                Name.setText(arrfeed.get(0).getStrName().replace(" ", "\n"));
                Tittle.setText(arrfeed.get(0).getStrtittle().replace(" ", "\n"));

                mVideoPlayerManager.playNewVideo(null, player_detail, arrfeed.get(0).getFilevideo());


                //  Timer();

            } catch (Exception e) {
                e.printStackTrace();
            }
            Name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mVideoPlayerManager.stopAnyPlayback();
                   // mute();
                    // TODO Auto-generated method stub
                    Log.e("bnknkjn", "" + player.isPlaying());

                    bundle = new Bundle();
                    bundle.putString("key", "other");
                    bundle.putString("userid", arrfeed.get(0).getId());
                    bundle.putString("tittle", arrfeed.get(0).getStrtittle());
                    bundle.putString("exprncid", arrfeed.get(0).getExperinceId());
                    ProfileFragment fragments = new ProfileFragment();
                    fragments.setArguments(bundle);



                }
            });

            experience_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                 //   mute();
                    mVideoPlayerManager.stopAnyPlayback();
                    Intent qn = new Intent((Activity) context, ActivityDdexpActivity.class);
                    context.startActivity(qn);


                }
            });


            profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mVideoPlayerManager.stopAnyPlayback();
                    GeneralValues.set_profile((Activity) context, "1");
                 //   mute();
                    Intent qn = new Intent((Activity) context, Profile.class);
                    context.startActivity(qn);


                }
            });

            msg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mVideoPlayerManager.stopAnyPlayback();
                    GeneralValues.set_msgtype((Activity) context, "0");
                 //   mute();
                    Intent qn = new Intent((Activity) context, Messages.class);
                    context.startActivity(qn);


                }
            });


            home.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mVideoPlayerManager.stopAnyPlayback();

                    GeneralValues.set_msgtype((Activity) context, "0");
                  //  mute();
                    Intent qn = new Intent((Activity) context, MainActivity.class);
                    context.startActivity(qn);


                }
            });

            contact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mVideoPlayerManager.stopAnyPlayback();
                    if (arrfeed.get(0).getId().equalsIgnoreCase(GeneralValues.get_user_id((Activity) context))) {

                    } else {
                        dialogZeoposton();
                    }

                }
            });


        } else {

        }


        return itemView;
    }


    @Override
    public void getvideo(int Id) {


        counter = Id;
        Log.e("ID>>>", ">>    " + counter);
        try {

            Name.setText(arrfeed.get(counter).getStrName().replace(" ", "\n"));
            Tittle.setText(arrfeed.get(counter).getStrtittle().replace(" ", "\n"));

            try {
                mVideoPlayerManager.stopAnyPlayback();
            } catch (Exception e) {
                e.printStackTrace();
            }


            mVideoPlayerManager.playNewVideo(null, player_detail, arrfeed.get(counter).getFilevideo());


        } catch (Exception e) {
            e.printStackTrace();
        }

        Name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             //   mute();
                mVideoPlayerManager.stopAnyPlayback();
                bundle = new Bundle();
                bundle.putString("key", "other");
                bundle.putString("userid", arrfeed.get(counter).getId());
                bundle.putString("tittle", arrfeed.get(counter).getStrtittle());
                bundle.putString("exprncid", arrfeed.get(counter).getExperinceId());

                ProfileFragment fragments = new ProfileFragment();
                fragments.setArguments(bundle);


            }
        });


        experience_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //    mute();
                mVideoPlayerManager.stopAnyPlayback();
                Intent qn = new Intent((Activity) context, ActivityDdexpActivity.class);
                context.startActivity(qn);


            }
        });


        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             //   mute();

                mVideoPlayerManager.stopAnyPlayback();
                GeneralValues.set_profile((Activity) context, "1");

                Intent qn = new Intent((Activity) context, Profile.class);
                context.startActivity(qn);


            }
        });

        msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mVideoPlayerManager.stopAnyPlayback();
                GeneralValues.set_msgtype((Activity) context, "0");
             //   mute();
                Intent qn = new Intent((Activity) context, Messages.class);
                context.startActivity(qn);


            }
        });


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mVideoPlayerManager.stopAnyPlayback();
          //      mute();
                Intent qn = new Intent((Activity) context, MainActivity.class);
                context.startActivity(qn);

            }
        });


        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (arrfeed.get(counter).getId().equalsIgnoreCase(GeneralValues.get_user_id((Activity) context))) {

                } else {
                    Forgot_dialog();
                }

            }
        });

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView( (RelativeLayout) object);


        Log.e("on distroy item", ">>  on distroy item  " );
        try {
            mVideoPlayerManager.stopAnyPlayback();
        } catch (Exception e) {
            e.printStackTrace();
        }


        mVideoPlayerManager.playNewVideo(null, player_detail, arrfeed.get(counter).getFilevideo());


    }











    public void Forgot_dialog() {
        mBottomSheetDialog = new BottomSheetDialog(context, R.style.Material_App_BottomSheetDialog);
        View dialog = LayoutInflater.from(context).inflate(R.layout.cotact_dialog, null);


        ImageView blue = (ImageView) dialog.findViewById(R.id.blue);
        LinearLayout contactus = (LinearLayout) dialog.findViewById(R.id.contactus);
        TextView TouchRed = (TextView) dialog.findViewById(R.id.TouchRed);

        TextView Touchblue = (TextView) dialog.findViewById(R.id.Touchblue);

        TouchRed.setText("WITH " + arrfeed.get(counter).getStrName());
        Touchblue.setText("WITH " + arrfeed.get(counter).getStrName());


        LinearLayout cancel = (LinearLayout) dialog.findViewById(R.id.cancel);

        // if decline button is clicked, close the custom dialog
        blue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBottomSheetDialog.dismiss();


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (Methods.checkPermissionForCamera((Activity) context)) {

                            if (Methods.checkPermissionForGallary((Activity) context)) {
                                if (Methods.checkPermissionForAudio((Activity) context)) {
                                    if (Methods.checkPermissionForWritestorage((Activity) context)) {
                                     //   mute();
                                        Log.e("Userid<><", arrfeed.get(counter).getId());

                                        Intent in = new Intent(context, MessageCamera.class);
                                        in.putExtra("userid", arrfeed.get(counter).getId());
                                        in.putExtra("tittle", arrfeed.get(counter).getStrtittle());
                                        in.putExtra("exprncid", arrfeed.get(counter).getExperinceId());
                                        context.startActivity(in);

                                    } else {
                                        Methods.requestPermissionForWriteStorage((Activity) context);
                                    }

                                } else {
                                    Methods.requestPermissionForAudio((Activity) context);
                                }

                            } else {
                                Methods.requestPermissionForGallry((Activity) context);
                            }

                        } else {
                            Methods.requestPermissionForCamera((Activity) context);
                        }

                    }
                }, 400);


            }
        });

        // if decline button is clicked, close the custom dialog

        contactus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBottomSheetDialog.dismiss();

             //   mute();
                bundle = new Bundle();
                bundle.putString("Name", arrfeed.get(counter).getStrName());
                bundle.putString("otherid", arrfeed.get(counter).getId());

                RedcontactFragment fragments = new RedcontactFragment();
                fragments.setArguments(bundle);

                Globals.FRAGMENT_MANAGER = ((FragmentActivity) context).getFragmentManager();
                Globals.fragmentTransaction = Globals.FRAGMENT_MANAGER.beginTransaction();
                FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(context, Globals.fragmentTransaction, MyprofileFragment.newInstance(), fragments, Globals.frag_id);
                fragmentTransactionExtended.addTransition(FragmentTransactionExtended.FLIP_HORIZONTAL);
                fragmentTransactionExtended.commit();

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBottomSheetDialog.dismiss();

            }
        });


        mBottomSheetDialog.contentView(dialog)
                .show();

    }


    public void dialogZeoposton() {
        mBottomSheetDialog = new BottomSheetDialog(context, R.style.Material_App_BottomSheetDialog);
        View dialog = LayoutInflater.from(context).inflate(R.layout.cotact_dialog, null);


        ImageView blue = (ImageView) dialog.findViewById(R.id.blue);
        LinearLayout contactus = (LinearLayout) dialog.findViewById(R.id.contactus);
        TextView TouchRed = (TextView) dialog.findViewById(R.id.TouchRed);

        TextView Touchblue = (TextView) dialog.findViewById(R.id.Touchblue);

        TouchRed.setText("WITH " + arrfeed.get(0).getStrName());
        Touchblue.setText("WITH " + arrfeed.get(0).getStrName());


        LinearLayout cancel = (LinearLayout) dialog.findViewById(R.id.cancel);

        // if decline button is clicked, close the custom dialog
        blue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBottomSheetDialog.dismiss();


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (Methods.checkPermissionForCamera((Activity) context)) {

                            if (Methods.checkPermissionForGallary((Activity) context)) {
                                if (Methods.checkPermissionForAudio((Activity) context)) {
                                    if (Methods.checkPermissionForWritestorage((Activity) context)) {

                                        Log.e("Userid<><", arrfeed.get(0).getId());
                                   //     mute();
                                        Intent in = new Intent(context, MessageCamera.class);
                                        in.putExtra("userid", arrfeed.get(0).getId());
                                        in.putExtra("tittle", arrfeed.get(0).getStrtittle());
                                        in.putExtra("exprncid", arrfeed.get(0).getExperinceId());
                                        context.startActivity(in);

                                    } else {
                                        Methods.requestPermissionForWriteStorage((Activity) context);
                                    }

                                } else {
                                    Methods.requestPermissionForAudio((Activity) context);
                                }

                            } else {
                                Methods.requestPermissionForGallry((Activity) context);
                            }

                        } else {
                            Methods.requestPermissionForCamera((Activity) context);
                        }

                    }
                }, 400);


            }
        });

        // if decline button is clicked, close the custom dialog

        contactus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBottomSheetDialog.dismiss();
               // mute();

                bundle = new Bundle();
                bundle.putString("Name", arrfeed.get(0).getStrName());
                bundle.putString("otherid", arrfeed.get(0).getId());

                RedcontactFragment fragments = new RedcontactFragment();
                fragments.setArguments(bundle);

                Globals.FRAGMENT_MANAGER = ((FragmentActivity) context).getFragmentManager();
                Globals.fragmentTransaction = Globals.FRAGMENT_MANAGER.beginTransaction();
                FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(context, Globals.fragmentTransaction, MyprofileFragment.newInstance(), fragments, Globals.frag_id);
                fragmentTransactionExtended.addTransition(FragmentTransactionExtended.FLIP_HORIZONTAL);
                fragmentTransactionExtended.commit();

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBottomSheetDialog.dismiss();

            }
        });


        mBottomSheetDialog.contentView(dialog)
                .show();

    }


    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        Log.e("datattata", "hjkjn================");
       // mute();
    }

    private void mute() {
        audioManager = (AudioManager) ((Activity) context).getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
    }

    public void unmute() {
        audioManager = (AudioManager) ((Activity) context).getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
    }

public void Getvideoplayer_Values(){
    player_detail.addMediaPlayerListener(new SimpleMainThreadMediaPlayerListener() {
        @Override
        public void onVideoPreparedMainThread() {
            // We hide the cover when video is prepared. Playback is about to start
            try {

                try {
                    Log.e("onVideoPreparedMainThread>>>.......", "onVideoPreparedMainThread");
                    unmute();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onVideoStoppedMainThread() {
            // We show the cover when video is stopped
            try {
                Log.e("onVideoStoppedMainThread>>>.......", "onVideoStoppedMainThread");
                // dot_progress_bar.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onBufferingUpdateMainThread(int percent) {
            super.onBufferingUpdateMainThread(percent);

            Log.e("onBufferingUpdateMainThread>>>.......", "onBufferingUpdateMainThread");

        }

        @Override
        public void onVideoCompletionMainThread() {
            // We show the cover when video is completed
            try {
                Log.e("onVideoCompletionMainThread>>>.......", "onVideoCompletionMainThread");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    });


}
}