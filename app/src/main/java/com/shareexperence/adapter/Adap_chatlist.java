package com.shareexperence.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shareexperence.R;
import com.shareexperence.Utils.CommonUtils;
import com.shareexperence.Utils.Gettersetter;

import java.util.ArrayList;

import static com.shareexperence.R.id.profile_image;


public class Adap_chatlist extends BaseAdapter  {
	int flag=0,ePos;
	ViewHolder view;
	Context context;
	ArrayList<Gettersetter> arrstate;
	Dialog dialogItem;
	Animation bounceAnimation,listAnimation;
	public Adap_chatlist(Context context, ArrayList<Gettersetter> arrstate) {
		this.context = context;
		this.arrstate = arrstate;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrstate.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arrstate.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(final int arg0, View convertView, ViewGroup arg2) {
		ViewHolder holder = null;

		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {


			convertView = mInflater.inflate(R.layout.adap_allmesages, null);
			holder = new ViewHolder();




			holder.name = (TextView)convertView.findViewById(R.id.name);
 			holder.profileImage = (ImageView)convertView.findViewById(profile_image);
			holder.main = (LinearLayout) convertView.findViewById(R.id.main);


			convertView.setTag(holder);
			
		} else {
			holder = (ViewHolder) convertView.getTag();
		}


	/*	String CurrentString = arrstate.get(arg0).getStrcalstatus();

		StringTokenizer tokens = new StringTokenizer(CurrentString, " ");
		String first = tokens.nextToken();// this will contain "Fruit"
		String second = tokens.nextToken();*/

		try {

 			holder.name.setText(arrstate.get(arg0).getStrName() );


				CommonUtils.getDisplayImage(context,arrstate.get(arg0).getStrimage(),holder.profileImage);




		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		holder.profileImage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String large = arrstate.get(arg0).getStrimage();
				fullimage(large);

			}
		});





		//holder.tv_name.setText(list.get(arg0));
		return convertView;

	}
	
	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return position;
	}


	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return getCount();
	}

	class ViewHolder {

		public TextView name,noti,tittle,time ;
		public ImageView profileImage ;
public LinearLayout main ;

	
	}

	public void fullimage(String large) {

		ImageView closeimage, popUpImage;

		dialogItem = new Dialog(context,
				android.R.style.Theme_Translucent_NoTitleBar);
		dialogItem.setContentView(R.layout.full_image);
		popUpImage = (ImageView) dialogItem.findViewById(R.id.image);
		closeimage = (ImageView) dialogItem.findViewById(R.id.closeImage);

		CommonUtils.getDisplayImage(context, large, popUpImage);

		closeimage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialogItem.dismiss();
			}
		});

		dialogItem.show();
	}

}