package com.shareexperence.adapter;

import android.support.v4.view.ViewPager;

/**
 * Created by netset on 13/10/16.
 */
public class DetailOnPageChangeListener extends ViewPager.SimpleOnPageChangeListener {

    private int currentPage;

    @Override
    public void onPageSelected(int position) {
        currentPage = position;
    }

    public final int getCurrentPage() {
        return currentPage;
    }

}
