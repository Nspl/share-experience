package com.shareexperence.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.shareexperence.R;
import com.shareexperence.Utils.CommonUtils;
import com.shareexperence.Utils.Gettersetter;

import java.util.ArrayList;


public class Adap_myexperince extends BaseAdapter  {
	int flag=0,ePos;
	ViewHolder view;
	Context context;
	ArrayList<Gettersetter> arrfeed;
	Dialog dialogItem;
	Animation bounceAnimation,listAnimation;
	String c;

	public Adap_myexperince(Context context, ArrayList<Gettersetter> arrfeed) {
		this.context = context;
	 	this.arrfeed = arrfeed;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		//return arrstate.size();
		return arrfeed.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arrfeed.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(final int arg0, View convertView, ViewGroup arg2) {
		ViewHolder holder = null;

		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {

			convertView = mInflater.inflate(R.layout.adap_myexprnc, null);
			holder = new ViewHolder();


		 	holder.profile_image = (ImageView)convertView.findViewById(R.id.profile_image);


 			holder.notitext= (TextView) convertView.findViewById(R.id.text);

 			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}


		try {

			holder.notitext.setText(arrfeed.get(arg0).getStrtittle());
			CommonUtils.getDisplayImage(context,arrfeed.get(arg0).getStrimage(),holder.profile_image);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		//holder.tv_name.setText(list.get(arg0));
		return convertView;
		
	 
	}
	
	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return getCount();
	}

	class ViewHolder {

		public ImageView profile_image ;


		public TextView notitext ,time ;

	}


}