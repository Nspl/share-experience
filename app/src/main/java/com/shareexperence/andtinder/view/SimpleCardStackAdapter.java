package com.shareexperence.andtinder.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.desarrollodroide.libraryfragmenttransactionextended.FragmentTransactionExtended;
import com.shareexperence.R;
import com.shareexperence.Utils.CommonUtils;
import com.shareexperence.Utils.Globals;
import com.shareexperence.andtinder.model.CardModel;
import com.shareexperence.fragments.ActivityHomeFragment;
import com.shareexperence.fragments.MyprofileFragment;


public final class SimpleCardStackAdapter extends CardStackAdapter {

    Context c;
    ProgressBar pro;
    WebView webview;
    Bundle bundle ;
    public SimpleCardStackAdapter(Context mContext) {
        super(mContext);
        this.c = mContext;


    }

    @Override
    public View getCardView(int position, final CardModel model, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.std_card_inner, parent, false);
            assert convertView != null;
        }
 /*       if (model.getPro_video().equals("")) {*/
            RelativeLayout video_frame_stck = (RelativeLayout) convertView.findViewById(R.id.video_frame_stck);
            video_frame_stck.setVisibility(View.GONE);
            ((ImageView) convertView.findViewById(R.id.imageis)).setVisibility(View.VISIBLE);
            CommonUtils.getDisplayImage(c,model.getCardImageDrawable(),(ImageView) convertView.findViewById(R.id.imageis));
       //     CommonUtils.imageLoader.displayImage(model.getCardImageDrawable(), ((ImageView) convertView.findViewById(R.id.imageis)), SyncStateContract.Constants.options);
  /*      }*/ /*else {
            ((ImageView) convertView.findViewById(R.id.imageis)).setVisibility(View.GONE);
            RelativeLayout video_frame_stck = (RelativeLayout) convertView.findViewById(R.id.video_frame_stck);
            video_frame_stck.setVisibility(View.VISIBLE);
            webview = (WebView) convertView.findViewById(R.id.web);
            pro = (ProgressBar) convertView.findViewById(R.id.progressBar_di);
            webview.getSettings().setJavaScriptEnabled(true);
            webview.getSettings().setAppCacheEnabled(true);
            webview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
            webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            webview.getSettings().setSaveFormData(true);
            webview.setWebViewClient(new myWebClient());
            webview.loadUrl(model.getPro_video());


        }*/

        ((TextView) convertView.findViewById(R.id.description)).setText(model.getTitle() );

        ((ImageView) convertView.findViewById(R.id.imageis)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getContext(), "clicked", Toast.LENGTH_LONG).show();
                String user_id = model.getUserid();
                String videolink= model.getPro_video() ;
                String Id = model.getDescription() ;
                String tittle= model.getTitle() ;
                String Thumb = model.getCardImageDrawable() ;

                Log.e("HOME SCREEN/>>>user_id", user_id+"><<><"+videolink);


                bundle = new Bundle();
                bundle.putString("Tittle",tittle);
                bundle.putString("user_id",user_id) ;
                bundle.putString("videolink",videolink);
                bundle.putString("Id",Id);
                bundle.putString("Thumb",Thumb);


                ActivityHomeFragment fragments = new ActivityHomeFragment();
                fragments.setArguments(bundle);

                Globals.FRAGMENT_MANAGER = ((Activity)c).getFragmentManager();
                Globals.fragmentTransaction = Globals.FRAGMENT_MANAGER.beginTransaction();
                FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(c, Globals.fragmentTransaction, MyprofileFragment.newInstance(), fragments , Globals.frag_id);
                fragmentTransactionExtended.addTransition(FragmentTransactionExtended.SLIDE_HORIZONTAL);
                fragmentTransactionExtended.commit();





                // c.startActivity(new Intent(c, InfoActivity.class).putExtra("userid", user_id));
              /*  Bundle bundle = new Bundle();
                bundle.putString("userid", user_id);
                InfoProfileFragment fragment2 = new InfoProfileFragment();
                fragment2.setArguments(bundle);

                FragmentTransaction fragmentTransaction = Sparksfly_HomeFragment.fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment2);
                fragmentTransaction.addToBackStack("backk");
                fragmentTransaction.commit();*/
            }
        });

        return convertView;
    }

    public class myWebClient extends WebViewClient {


        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);


        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            pro.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);

            pro.setVisibility(View.GONE);
        }
    }

}
