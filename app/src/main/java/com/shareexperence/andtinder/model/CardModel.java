/**
 * AndTinder v0.1 for Android
 *
 * @Author: Enrique López Mañas <eenriquelopez@gmail.com>
 * http://www.lopez-manas.com
 *
 * TAndTinder is a native library for Android that provide a
 * Tinder card like effect. A card can be constructed using an
 * image and displayed with animation effects, dismiss-to-like
 * and dismiss-to-unlike, and use different sorting mechanisms.
 *
 * AndTinder is compatible with API Level 13 and upwards
 *
 * @copyright: Enrique López Mañas
 * @license: Apache License 2.0
 */

package com.shareexperence.andtinder.model;

import android.graphics.drawable.Drawable;

public class CardModel {

	private String   title;
	private String   userid;
	private String   description,pro_video;
	private String cardImageDrawable;
	private String cardLikeImageDrawable;
	private String cardDislikeImageDrawable;

    private OnCardDismissedListener mOnCardDismissedListener = null;

    private OnClickListener mOnClickListener = null;

    public interface OnCardDismissedListener {
        void onLike();
        void onDislike();
		void OnsuperLike();
    }

    public interface OnClickListener {
        void OnClickListener();
    }

	public CardModel(String title, String description, Drawable drawable, String pro_video) {
		this(null, null, (String)null,null,null);
	}

	public CardModel(String title, String description, String cardImage,String userid,String pro_video) {
		this.title = title;
		this.description = description;
		this.cardImageDrawable = cardImage;
		this.userid=userid;
		this.pro_video=pro_video;
	}

	/*public CardModel(String title, String description, String cardImage) {
		this.title = title;
		this.description = description;
		this.cardImageDrawable = cardImage;
	}*/

	public String getTitle() {
		return title;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCardImageDrawable() {
		return cardImageDrawable;
	}

	public void setCardImageDrawable(String cardImageDrawable) {
		this.cardImageDrawable = cardImageDrawable;
	}

	public String getCardLikeImageDrawable() {
		return cardLikeImageDrawable;
	}

	public void setCardLikeImageDrawable(String cardLikeImageDrawable) {
		this.cardLikeImageDrawable = cardLikeImageDrawable;
	}

	public String getCardDislikeImageDrawable() {
		return cardDislikeImageDrawable;
	}

	public void setCardDislikeImageDrawable(String cardDislikeImageDrawable) {
		this.cardDislikeImageDrawable = cardDislikeImageDrawable;
	}

    public void setOnCardDismissedListener( OnCardDismissedListener listener ) {
        this.mOnCardDismissedListener = listener;
    }

    public OnCardDismissedListener getOnCardDismissedListener() {
       return this.mOnCardDismissedListener;
    }

	public String getPro_video() {
		return pro_video;
	}

	public void setPro_video(String pro_video) {
		this.pro_video = pro_video;
	}

	public void setOnClickListener(OnClickListener listener ) {
        this.mOnClickListener = listener;
    }

    public OnClickListener getOnClickListener() {
        return this.mOnClickListener;
    }
}
