package com.shareexperence.redcontact;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.desarrollodroide.libraryfragmenttransactionextended.SlidingRelativeLayout;
import com.shareexperence.R;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Globals;
import com.shareexperence.activites.Home;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class RedcontactFragment extends Fragment implements MainAsynListener<String>{

    @Bind(R.id.main) SlidingRelativeLayout main;
    @Bind(R.id.R1) RelativeLayout r1;
    @Bind(R.id.text1) TextView text1;
    @Bind(R.id.text2) TextView text2;
    @Bind(R.id.text3) TextView text3;
    @Bind(R.id.Date) TextView date;
    @Bind(R.id.text5) TextView text5;
    @Bind(R.id.Email) EditText email;
    @Bind(R.id.text6) TextView text6;
    @Bind(R.id.Phone) EditText phone;
    @Bind(R.id.signup)
    ImageView signup;
    Bundle bundle ;
    String Name,Otherid ;
    Calendar myCalendar ;
    DatePickerDialog.OnDateSetListener dateSetListener ;

    public static RedcontactFragment newInstance() {
        return new RedcontactFragment();
    }

    public RedcontactFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.redcontact, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        bundle = getArguments();
        if (bundle != null) {

            try {

                Name = bundle.getString("Name");
                Otherid = bundle.getString("otherid");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        String colorText= "I am intrested in "+"<font color=\"#ffffff\"><bold>"	+ Name	+ "</bold></font>" +" offer"  ;


        Log.e("Phone number><<>",GeneralValues.get_phn(getActivity()));

        text1.setText(Html.fromHtml(colorText));
        email.setText(GeneralValues.get_email(getActivity()));
        phone.setText(GeneralValues.get_phn(getActivity()));


            myCalendar = Calendar.getInstance();

        dateSetListener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


    }


    @OnClick(R.id.signup) void setSignup(){

        if (date.getText().toString().length() == 0){
            Toast.makeText(getActivity(),"Please select date",Toast.LENGTH_SHORT).show();

        }else if (phone.getText().toString().length() == 0){
            Toast.makeText(getActivity(),"Please enter Phone Number",Toast.LENGTH_SHORT).show();
        }else {

            Globals.getterList = new ArrayList<>();
            Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));
            Globals.getterList.add(new ParamsGetter("other_user_id", Otherid));
            Globals.getterList.add(new ParamsGetter("name", Name));
            Globals.getterList.add(new ParamsGetter("email", GeneralValues.get_email(getActivity())));
            Globals.getterList.add(new ParamsGetter("phone_number", phone.getText().toString()));
            Globals.getterList.add(new ParamsGetter("contact_date", date.getText().toString()));


            new MainAsyncTask(getActivity(), Globals.Webservice.ContactUsWithName, 1, RedcontactFragment.this, "Form", Globals.getterList).execute();

        }


    }


    @OnClick(R.id.Date)void setdate(){
        try {
            DatePickerDialog dialog =   new DatePickerDialog(getActivity(), dateSetListener, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)) ;

            dialog.getDatePicker().setMinDate(myCalendar.getTimeInMillis());
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
    private void updateLabel() {

        String myFormat = "dd-MMM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        date.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {
        if (isSucess) {
            if (result != null) {
                try {

                    if (flag == 1) {
                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        String msg = Globals.jsonObj.getString("message");
                        if (status.equalsIgnoreCase("true")) {


                            final SweetAlertDialog pDialog_logout = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                            pDialog_logout.setCancelable(false);
                            pDialog_logout.setCanceledOnTouchOutside(false);
                            pDialog_logout.setTitleText("Success");
                            pDialog_logout.setContentText("Email Sent Successfully");
                            pDialog_logout.setConfirmText("OK");
                            pDialog_logout.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    pDialog_logout.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                            pDialog_logout.dismissWithAnimation();

                                            Intent in = new Intent(getActivity(), Home.class);
                                            getActivity().startActivity(in);
                                            getActivity().finish();

                                        }
                                    }, 500);
                                }
                            });

                            pDialog_logout.show();


                        } else {

                            Snackbar.make(getView(), "Something Went Wrong", Snackbar.LENGTH_SHORT).show();

                        }

                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        }




    }

    @Override
    public void onPostError(int flag) {

    }
}
