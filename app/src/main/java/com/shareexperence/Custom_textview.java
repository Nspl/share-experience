package com.shareexperence;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by netset on 27/1/17.
 */

public class Custom_textview extends TextView {

    public Custom_textview(Context context, AttributeSet atttr){
        super(context,atttr);
        this.setTypeface(getTypeface().createFromAsset(context.getAssets(),"fonts/gotham.ttf"));
    }
}
