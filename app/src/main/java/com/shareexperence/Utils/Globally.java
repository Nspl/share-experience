package com.shareexperence.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class Globally {


	SharedPreferences sharedpreferences;
	public static String registrationId = "";
	public static SharedPreferences preferences, prefs;
	public static int NOTIFICATION_ID = 1;
 	public static JSONObject obj, PROFILE_JSON,USER_POST_JSON, FOLLOWERS_POST_JSON, MSG_JSON;
	public static JSONArray JSON_ARRAY, USER_POST_ARRAY, FOLLOWERS_POST_ARRAY, COMMENTS_ARRAY, SEARCH_COMMENTS_ARRAY, 
	USER_COMMENTS_ARRAY,  MESSAGE_ARRAY, USER_ARRAY;
 	public static JSONObject SEARCH_USER_JSON = new JSONObject();
	public static JSONArray SEARCH_ARRAY = new JSONArray();
 	public static String STATUS = "", MESSAGE = "", USER_ID = "",mConsumerKey = "",mSecretKey = "" ;
 	public static File mediaStorageDir;
 	public static Intent i;
	public static int in;
	public static int POSITION;
	public static boolean isFollow = true, GET_USER_PROFILE = false, IS_SHARE_FLYER = false, 
			IS_LOCAL = false, REFRESH_FOLLOWER = false, IS_ACTIVITY = false, IS_DELETE = false ;
	public static String tabNumber = "", IS_ALLOW = "", TOKEN = "", SECRET = "", FACEBOOK_ID = "", friendID = "";
	public  static  Boolean SparKGiven=false;
	
	public static ArrayAdapter<String> adapter1 ;
	public static ArrayList<String> places = new ArrayList<String>();
	public static BitmapFactory.Options options = new BitmapFactory.Options();
	public static Bitmap bitmap;
	public static String PUSH_RESPONSE = "", PUSH_ID = "", msgId = "", NOTIFICATION_TYPE = "";
 	static LayoutInflater inflater;
	static View layout;
	static TextView text;
	static Toast toast;

	public static Typeface PT_SANS_STYLE, PT_SANS_BOLD_STYLE;
	public static String PT_SANS_FONT = "fonts/PTS55F.ttf";
	public static String PT_SANS_BOLD = "fonts/PT_sans_bold.ttf";
	public static boolean checkConnection = false;
	public static String INTERNET_MSG = "Not connected to Internet";
	public static JSONObject JSON ;
	public static String IS_FOLLOWER_FOLLOWING = "";
	public static Bitmap cropedd ;

	public static int TAB_NUMBER = 0;
	public static final int PICK_FROM_CAMERA = 1;
	
	
	public static void showToast(Context cxt, String message)
	{
		Toast.makeText(cxt, message, Toast.LENGTH_SHORT).show();
	}
	
			
				// Set Login status -------------------
				public static void setRegStatus(String key, String value, Context context) {
				    prefs = PreferenceManager.getDefaultSharedPreferences(context);
				    SharedPreferences.Editor editor = prefs.edit();
				    editor.putString(key, value);
				    editor.commit();
				}
				
				// Get Login status -------------------
				public static String getRegStatus(String key, Context context) {
				    preferences = PreferenceManager.getDefaultSharedPreferences(context);
				    return preferences.getString(key, "");
				}
				
				
				// Set  UID -------------------
				public static void setuserID(String key, String value, Context context) {
				     prefs = PreferenceManager.getDefaultSharedPreferences(context);
				    SharedPreferences.Editor editor = prefs.edit();
				    editor.putString(key, value);
				    editor.commit();
				}
				
				// Get  UID -------------------
				public static String getUserID(String key, Context context) {
				    preferences = PreferenceManager.getDefaultSharedPreferences(context);
				    return preferences.getString(key, "");
				}
				
				
				// Set  UserName -------------------
				public static void setuserName(String key, String value, Context context) {
				     prefs = PreferenceManager.getDefaultSharedPreferences(context);
				    SharedPreferences.Editor editor = prefs.edit();
				    editor.putString(key, value);
				    editor.commit();
				}
				
				// Get  USerName -------------------
				public static String getUserName(String key, Context context) {
				    preferences = PreferenceManager.getDefaultSharedPreferences(context);
				    return preferences.getString(key, "");
				}
				
				
				// Set  Email -------------------
				public static void setEmail(String key, String value, Context context) {
				     prefs = PreferenceManager.getDefaultSharedPreferences(context);
				    SharedPreferences.Editor editor = prefs.edit();
				    editor.putString(key, value);
				    editor.commit();
				}
				
				// Get  Email -------------------
				public static String getEmail(String key, Context context) {
				    preferences = PreferenceManager.getDefaultSharedPreferences(context);
				    return preferences.getString(key, "");
				}
				
				
				// Set  Email -------------------
				public static void setNotificationStatus(String key, String value, Context context) {
				     prefs = PreferenceManager.getDefaultSharedPreferences(context);
				    SharedPreferences.Editor editor = prefs.edit();
				    editor.putString(key, value);
				    editor.commit();
				}
				// Get  Email -------------------
				public static String getNotificationStatus(String key, Context context) {
				    preferences = PreferenceManager.getDefaultSharedPreferences(context);
				    return preferences.getString(key, "");
				}

				
				// Set User Profile -------------------
				public static void setUserProfile(String key, String firstName, String key1, String lastName,
						String key2, String about, String key3, String website, String key4, String location,
						String key5, String image, Context context) {
				    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
				    SharedPreferences.Editor editor = prefs.edit();
				    editor.putString(key, firstName);
				    editor.putString(key1, lastName);
				    editor.putString(key2, about );
				    editor.putString(key3, website);
				    editor.putString(key4, location);
				    editor.putString(key5, image);
				    editor.commit();
				}
				// Get User Profile  -------------------
				public static String getUserProfile(String key, Context context) {
				    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
				    return preferences.getString(key, "");
				    
				}
				
				// Set Last Name -------------------
				public static void setLsstName(String key, String value, Context context) {
				    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
				    SharedPreferences.Editor editor = prefs.edit();
				    editor.putString(key, value);
				    editor.commit();
				}
				
				// Get Last Name  -------------------
				public static String getLastName(String key, Context context) {
				    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
				    return preferences.getString(key, "");
				}
				
				
				
				// Set Twitter Login status -------------------
				public static void setTwitterStatus(String key, String value, Context context) {
				    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
				    SharedPreferences.Editor editor = prefs.edit();
				    editor.putString(key, value);
				    editor.commit();
				}
				
				// Get Login status -------------------
				public static String getTwitterStatus(String key, Context context) {
				    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
				    return preferences.getString(key, "");
				}
			

				// Set RegId -------------------
				public static void setRegId(String key, String value, Context context) {
				    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
				    SharedPreferences.Editor editor = prefs.edit();
				    editor.putString(key, value);
				    editor.commit();
				}
				// Get RegId -------------------
				public static String getRegId(String key, Context context) {
				    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
				    return preferences.getString(key, "");
				}
				
				// Set RegId -------------------
				public static void setLogoImage(String key, String value, Context context) {
				    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
				    SharedPreferences.Editor editor = prefs.edit();
				    editor.putString(key, value);
				    editor.commit();
				}
				// Get RegId -------------------
				public static String getLogoImage(String key, Context context) {
				    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
				    return preferences.getString(key, "");
				}

		/*
				public  static void getDisplayImage(Context context, String string,ImageView imgView){
					  
					  DisplayImageOptions  options = new DisplayImageOptions.Builder()
					      .showImageOnLoading(R.drawable.default_profile)
					      .showImageForEmptyUri(R.drawable.default_profile)
					      .showImageOnFail(R.drawable.default_profile).cacheInMemory(true)
					      .cacheOnDisk(true).considerExifParams(true)
					      .build();
					  
					  ImageLoader.getInstance()
					     .displayImage(string, imgView, options);
					  
					 }
				*/
				
/*
				 *//*CUSTOM MSG POPUP *//*
				public static void showMessage(Context cxt, String msg) {
					inflater = ((Activity)cxt).getLayoutInflater();
					layout = inflater.inflate(R.layout.viewww,
							(ViewGroup) ((Activity)cxt).findViewById(R.id.toast_layout_root));

					text = (TextView) layout.findViewById(R.id.text);
					//text.setWidth(500);
					//text.setHeight(50);
					text.setText(msg);

					toast = new Toast(cxt);
					toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM, 0, 130);
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setView(layout);
					toast.show();
				}
				*/
				
				
	/*			 *//*CUSTOM MSG POPUP FOR SHORT TIME  *//*
				public static void showMessageShort(Context cxt, String msg) {
					inflater = ((Activity)cxt).getLayoutInflater();
					layout = inflater.inflate(R.layout.viewww,
							(ViewGroup) ((Activity)cxt).findViewById(R.id.toast_layout_root));

					text = (TextView) layout.findViewById(R.id.text);
					text.setText(msg);

					toast = new Toast(cxt);
					toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM, 0, 130);
					toast.setDuration(Toast.LENGTH_SHORT);
					toast.setView(layout);
					toast.show();
					
					Handler handler = new Handler();
		            handler.postDelayed(new Runnable() {
		               @Override
		               public void run() {
		                   toast.cancel(); 
		               }
		        }, 500);
				}
				
				*/
				

				/*  public static void registerDevice(Context cxt) {
					     
					     googleCloud = GoogleCloudMessaging.getInstance(cxt);
					     if(getRegId("regId", cxt)!=null){
					     registrationId = getRegId("regId", cxt);
					     }
					   //  regid = getRegistrationId(Search_location_taxi.this);
					    Log.e("SAVE IDDDD", ": "+registrationId);
					     if (registrationId.isEmpty()) {
					    	 
					      registerBackground(cxt);
					       }
					    }
*/

/*
				   @SuppressWarnings("unchecked")
				   private static void registerBackground(final Context cxtt) {
				       new AsyncTask() {
				        @Override
				     protected Object doInBackground(Object... params) {
				      // TODO Auto-generated method stub
				                try {
				                    if (googleCloud == null) {
				                    	googleCloud = GoogleCloudMessaging.getInstance(cxtt);
				                    }
				                    registrationId = googleCloud.register("1001963791794");  //645253428826
				                //    Log.e("REGIDSDDDDDDDDD", ": "+registrationId);
				                    
				                    // Save the regid - no need to register again.
				                    setRegId("regId", registrationId, cxtt); //(, );
				                    
				                  
				                } catch (IOException ex) {
				                 ex.printStackTrace();
				                }
				               
				      return null;
				     }
				          


				     
				       }.execute(null, null, null);
				   }
				   */
				   
				   
			public static  String getCurrentTime(String localTime){
				Calendar cal = Calendar.getInstance(TimeZone.getDefault());  //"GMT+1:00"
		         Date currentLocalTime = cal.getTime();
		         
				DateFormat date, time ; 
				
				date = new SimpleDateFormat("yyyy-mm-dd"); 
				time = new SimpleDateFormat("HH:mm:ss");
				
				date.setTimeZone(TimeZone.getDefault()); 
				time.setTimeZone(TimeZone.getDefault());
				
	             localTime = date.format(currentLocalTime) + " " + time.format(currentLocalTime);
	             return localTime;
			}
				   
    //  ================== Set Media path ===================
    public static File getOutputMediaFile( String timeStamp) 
    {
    	File mediaStorageDir=null;
    	mediaStorageDir=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"Imojes");
    	 if (!mediaStorageDir.exists())
    	 {
             if (!mediaStorageDir.mkdirs()){
              Log.d("imojes: ", "failed to create directory");
             return null;
             }
         }
    	// Create a media file name
        
         File mediaFile=null;
         mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
         return mediaFile;
    }
	
    
 // Create URI file----
 	public static Uri getOutputMediaFileUri(int type) {
 		return Uri.fromFile(getOutputMediaFile(type));
 	}
 	
 	

	public static File getOutputMediaFile(int type) {
		
		// External sdcard location
		File mediaStorageDir = new File(Environment.getExternalStorageDirectory(),"Wallflyer"); 

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				// Log.d("IMAGE_DIRECTORY_NAME", "Oops! Failed create " + "Wallflyer" + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		File mediaFile;
		if (type == PICK_FROM_CAMERA) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
		}  else {
			return null;
		}

		return mediaFile;
	}

	
	
    //  ================== Set Media path ===================
    public static File getOutputFilterFile( String timeStamp) 
    {
    	mediaStorageDir=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"Wallflyer/FilterImages");
    	 if (!mediaStorageDir.exists())
    	 {
             if (!mediaStorageDir.mkdirs()){
             // Log.d("wallflyer", "failed to create directory");
             return null;
             }
         }
    	
    	 // Create a media file name
         File mediaFile=null;
         mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
         return mediaFile;
    }
    
    
    public static File createFolders() {

        File baseDir;

        if ( android.os.Build.VERSION.SDK_INT < 8 ) {
            baseDir = Environment.getExternalStorageDirectory();
        } else {
            baseDir = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES );
        }

        if ( baseDir == null ) return Environment.getExternalStorageDirectory();

        File aviaryFolder = new File( baseDir, "Wallflyer/FilterImages" );

        if ( aviaryFolder.exists() ) return aviaryFolder;
        if ( aviaryFolder.mkdirs() ) return aviaryFolder;

        return Environment.getExternalStorageDirectory();
    }
    
    
    
    // =================== Set url Image on Bitmap ======================
	public static Bitmap getBitmapFromURL(String src) {
	    try {
	        URL url = new URL(src);
	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setDoInput(true);
	        connection.connect();
	        InputStream input = connection.getInputStream();
	        Bitmap myBitmap = BitmapFactory.decodeStream(input);
	        return myBitmap;
	    } catch (IOException e) {
	        e.printStackTrace();
	        return null;
	    }
	}
	
	
	
	public static final boolean isInternetOn(Context context) {
        boolean wifiAvailable = false;
        boolean mobileAvailable = false;
        ConnectivityManager conManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = conManager.getAllNetworkInfo();
        for (NetworkInfo netInfo : networkInfo) {
            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (netInfo.isConnected())
                    wifiAvailable = true;
            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (netInfo.isConnected())
                    mobileAvailable = true;
        }
        return wifiAvailable || mobileAvailable;
    }


	public static String convertStreamToString(InputStream is) {
	    String line = "";
	    StringBuilder total = new StringBuilder();
	    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
	    try {
	        while ((line = rd.readLine()) != null) {
	            total.append(line);
	        }
	    } catch (Exception e) {
	    }
	    return total.toString();
	}

	
	
	
	// turn on gps Dialog-------------
	 public static void showGPSDisabledAlertToUser(final Context context){
	        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
	        alertDialogBuilder.setMessage("GPS is disabled. Would you like to enable it?")
	        .setCancelable(true)
	        .setPositiveButton("Enable GPS",
	                new DialogInterface.OnClickListener(){
	            public void onClick(DialogInterface dialog, int id){
	                Intent callGPSSettingIntent = new Intent(
	                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	                context.startActivity(callGPSSettingIntent);
	                dialog.cancel();
	            }
	        });
	        alertDialogBuilder.setNegativeButton("Cancel",
	                new DialogInterface.OnClickListener(){
	            public void onClick(DialogInterface dialog, int id){
	                dialog.cancel();
	            }
	        });
	        AlertDialog alert = alertDialogBuilder.create();
	        alert.show();
	    }




/*
	public static void getDisplayImage(Context context, String string,
									   ImageView imgView) {

		DisplayImageOptions options = new DisplayImageOptions.Builder()
				.showImageForEmptyUri( R.drawable.no_image)
				.showImageOnLoading( R.drawable.no_image)
				.showImageOnFail( R.drawable.no_image).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true).build();

		ImageLoader.getInstance().displayImage(string, imgView, options);

	}
*/




}
