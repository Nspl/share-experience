package com.shareexperence.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class GeneralValues {
	static SharedPreferences preferences;
	static SharedPreferences.Editor editor;

	public static void set_user_id(Activity activity, String uid) {

		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		editor = preferences.edit();
		editor.putString("UID", uid);
		editor.commit();

	}

	public static String get_user_id(Context context) {
		preferences = PreferenceManager.getDefaultSharedPreferences(context);
		String user_id = preferences.getString("UID", "");
		return user_id;
	}

	public static void set_user_name(Activity activity, String uid) {

		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		editor = preferences.edit();
		editor.putString("user_name", uid);
		editor.commit();

	}

	public static String get_user_name(Activity activity) {
		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		String user_id = preferences.getString("user_name", "");
		return user_id;
	}
	public static void set_email(Activity activity, String uid) {

		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		editor = preferences.edit();
		editor.putString("email", uid);
		editor.commit();

	}

	public static String get_email(Activity activity) {
		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		String user_id = preferences.getString("email", "");
		return user_id;
	}
	public static void set_phn(Activity activity, String uid) {

		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		editor = preferences.edit();
		editor.putString("passwrd", uid);
		editor.commit();

	}

	public static String get_phn(Activity activity) {
		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		String user_id = preferences.getString("passwrd", "");
		return user_id;
	}
	public static void set_last_name(Activity activity, String uid) {

		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		editor = preferences.edit();
		editor.putString("last_name", uid);
		editor.commit();

	}

	public static String get_last_name(Activity activity) {
		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		String user_id = preferences.getString("last_name", "");
		return user_id;
	}

	public static void set_user_image(Activity activity, String uid) {

		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		editor = preferences.edit();
		editor.putString("image", uid);
		editor.commit();

	}

	public static String get_user_image(Activity activity) {
		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		String user_id = preferences.getString("image", "");
		return user_id;
	}


	public static void set_current_lat(Activity activity, String uid) {

		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		editor = preferences.edit();
		editor.putString("user_type", uid);
		editor.commit();

	}

	public static String get_current_lat(Activity activity) {
		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		String user_id = preferences.getString("user_type", "");
		return user_id;
	}

public static void set_gcmid(Context activity, String uid) {

		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		editor = preferences.edit();
		editor.putString("gcmid", uid);
		editor.commit();

	}

	public static String get_gcmid(Context activity) {
		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		String user_id = preferences.getString("gcmid", "");
		return user_id;
	}



	public static void set_current_long(Activity activity, String uid) {

		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		editor = preferences.edit();
		editor.putString("image", uid);
		editor.commit();

	}

	public static String get_current_long(Activity activity) {
		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		String user_id = preferences.getString("image", "");
		return user_id;
	}
	public static void set_fcmId(Activity activity, String uid) {

		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		editor = preferences.edit();
		editor.putString("image", uid);
		editor.commit();

	}

	public static String get_fcmId(Activity activity) {
		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		String user_id = preferences.getString("image", "");
		return user_id;
	}
	public static void set_loginbool(Activity activity, Boolean rid) {

		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		editor = preferences.edit();
		editor.putBoolean("bool", rid);
		editor.commit();

	}

	public static Boolean get_loginbool(Activity activity) {
		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		Boolean user_id = preferences.getBoolean("bool", false);
		return user_id;
	}


	public static void set_logintype(Activity activity, String uid) {

		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		editor = preferences.edit();
		editor.putString("qwe", uid);
		editor.commit();

	}

	public static String get_logintype(Activity activity) {
		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		String user_id = preferences.getString("qwe", "");
		return user_id;
	}


	public static void set_profile(Activity activity, String uid) {

		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		editor = preferences.edit();
		editor.putString("plm", uid);
		editor.commit();

	}

	public static String get_profile(Activity activity) {
		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		String user_id = preferences.getString("plm", "");
		return user_id;
	}
	public static void set_cameratype(Activity activity, String uid) {

		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		editor = preferences.edit();
		editor.putString("image", uid);
		editor.commit();

	}

	public static String get_cameratype(Activity activity) {
		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		String user_id = preferences.getString("image", "");
		return user_id;
	}



	public static void set_msgtype(Activity activity, String uid) {

		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		editor = preferences.edit();
		editor.putString("msg", uid);
		editor.commit();

	}

	public static String get_msgtype(Activity activity) {
		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		String user_id = preferences.getString("msg", "");
		return user_id;
	}

	public static void set_trial(Activity activity, String uid) {

		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		editor = preferences.edit();
		editor.putString("msg", uid);
		editor.commit();

	}

	public static String get_trial(Activity activity) {
		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		String user_id = preferences.getString("msg", "");
		return user_id;
	}
}
