package com.shareexperence.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Display;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.shareexperence.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;


public class CommonUtils {
	static Context ctx;
	public static int TYPE_WIFI = 1;
	public static int TYPE_MOBILE = 2;
	public static int TYPE_NOT_CONNECTED = 0;
	static ActionBar actionBar;
	public static TextView txtTitle,txtRightTitle,toast_text_view1,toast_text;
	static ImageView imgLeft,imgRight;
	public static	 RelativeLayout relLayout1,relLayout2;
	public static SimpleDateFormat sdf;
	public static String currenttime="";
	
	public static int setWidth(Context c) {
		Display display = ((Activity) c).getWindowManager().getDefaultDisplay();
		int w = display.getWidth();
		int width = ((w / 2) + 100);
		return width;
	}

	public static int getWidth(Context c) {
		Display display = ((Activity) c).getWindowManager().getDefaultDisplay();
		int w = display.getWidth();
		
		return w;
	}

	public static String dateandtime(){

		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		currenttime=sdf.format(new Date());
		return currenttime;

	}
	public static void hideSoftKeyboard(Activity activity) {
		InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
	}
/*	public static void actionbarImplement(Activity _Context,String strTitle,String strRighttTitle,Integer leftImage,Integer righttImage ){

		actionBar = (ActionBar)_Context.findViewById(R.id.actionbar);
		relLayout1=(RelativeLayout)actionBar.findViewById(R.id.relLayout1);
		relLayout2=(RelativeLayout)actionBar.findViewById(R.id.relLayout2);
		txtTitle=(TextView)actionBar.findViewById(R.id.txtTitle);

		txtRightTitle=(TextView)actionBar.findViewById(R.id.txtRightTitle);
		imgLeft=(ImageView)actionBar.findViewById(R.id.img1ActionBar);
		imgRight=(ImageView)actionBar.findViewById(R.id.img2ActionBar);

		txtTitle.setText(strTitle);
		txtRightTitle.setText(strRighttTitle);
		imgLeft.setBackgroundResource(leftImage);
		imgRight.setBackgroundResource(righttImage);
		relLayout1.setEnabled(false);
		relLayout2.setEnabled(false);
		  *//*
			 toastview=(View)_Context.findViewById(R.id.toastview);
			toast_text =(TextView)toastview.findViewById(R.id.toast_text_view1);
*//*

	}*/

	public static void getDisplayImage(Context context, String string,
									   ImageView imgView) {

		DisplayImageOptions options = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.no_image)
		 .showImageOnLoading(R.drawable.no_image)

				.showImageOnFail(R.drawable.no_image).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true).build();

		ImageLoader.getInstance().displayImage(string, imgView, options);

	}

	public static String getAge(String  strDate) throws ParseException{
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd.mm.yyyy");
	       Date date = sdf.parse(strDate);
	       Calendar c = Calendar.getInstance();
	       c.setTime(date);
	       Calendar today = Calendar.getInstance();
	       long l = today.getTimeInMillis()
	         - c.getTimeInMillis();
	       long d = l / (24 * 60 *  60 * 1000);
	       int f = (int) d / 365;
	       String DOB = String.valueOf(f);
	       Log.e("dddddddddddddddddd", "" + d + "       " + f);
		
		
	       return DOB;
		
	}




	public static boolean getConnectivityStatus(Context context) {

		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (null != activeNetwork) {
			if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
				return true;

			if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
				return true;
		}
		return false;

	}
	public static boolean isInternetAccessible(Context context) {
		if (getConnectivityStatus(context)) {
			try {
				HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
				urlc.setRequestProperty("User-Agent", "Test");
				urlc.setRequestProperty("Connection", "close");
				urlc.setConnectTimeout(1500);
				urlc.connect();
				return (urlc.getResponseCode() == 200);
			} catch (IOException e) {
				Log.e("IS ACCESSIBLE", "Couldn't check internet connection", e);
			}
		} else {
			Log.d("IS ACCESSIBLE", "Internet not available!");
		}
		return false;
	}
	public static String saveimagetosdcard(Context ctx, Bitmap photo) {

		// Bitmap bitmap = null;
		FileOutputStream output;
		File file=null;

		
		try {
			file=getOutputMediaFile();

			output = new FileOutputStream(file);

			// Compress into png format image from 0% - 100%
			photo.compress(Bitmap.CompressFormat.PNG, 100, output);
			output.flush();
			output.close();

		}

		catch (Exception e) {
			Toast.makeText(ctx, "Try Again.", Toast.LENGTH_SHORT).show();
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return file.getAbsolutePath();
	}

	// Create URI file----
	public static Uri getOutputMediaFileUri() {
		return Uri.fromFile(getOutputMediaFile());
	}


	public static File getOutputMediaFile() {
		 
	    // External sdcard location
	    File mediaStorageDir = 
	            Environment
	                    .getExternalStorageDirectory();
	    Log.e("Absoletete Path", mediaStorageDir.getAbsolutePath());
	 // Create a new folder in SD Card
	 		File dir = new File(mediaStorageDir.getAbsolutePath() + "/ShareExperence/");
	 		if(!dir.exists()){
				dir.mkdirs();
	 		}
	 		Log.e("DIrectoryyy", dir.toString());
	    // Create a media file name
	 		
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
	            Locale.getDefault()).format(new Date());
	    


	    File mediaFile;
	 
	        mediaFile = new File(dir.getPath() + File.separator
	                + "IMG_" + timeStamp + ".png");
	    
	 
	    return mediaFile;
	}	
		
	// /////////////////////Email Validation
		public static boolean validateEmailId(String email) {
			// TODO Auto-generated method stub
			final Pattern EMAIL_ADDRESS_PATTERN = Pattern
					.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
							+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
							+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+");

			boolean validEmail = EMAIL_ADDRESS_PATTERN.matcher(email).matches();
			// validate email address
			return validEmail;

		}


	public static void openInternetDialog(Context c) {
		ctx = c;
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
		alertDialogBuilder.setTitle("Internet Alert!");
		alertDialogBuilder
				.setMessage(
						"You are not connected to Internet..Please Enable Internet!")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
								final Intent intent = new Intent(Intent.ACTION_MAIN, null);
								intent.addCategory(Intent.CATEGORY_LAUNCHER);
								final ComponentName cn = new ComponentName(
										"com.android.settings",
										"com.android.settings.wifi.WifiSettings");
								intent.setComponent(cn);
								intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								ctx.startActivity(intent);
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	
	public static Bitmap getCircularBitmap(Bitmap bitmap) {

		Bitmap output;
		if (bitmap.getWidth() > bitmap.getHeight()) {
			output = Bitmap.createBitmap(bitmap.getHeight(),
					bitmap.getHeight(), Config.ARGB_8888);
		} else {
			output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(),
					Config.ARGB_8888);
		}

		Canvas canvas = new Canvas(output);
		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

		float r = 0;
		if (bitmap.getWidth() > bitmap.getHeight()) {
			r = bitmap.getHeight() / 2;
		} else {
			r = bitmap.getWidth() / 2;
		}

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawCircle(r, r, r, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);
		return output;
	}
	
	
	 public static void buildAlertMessageNoGps(final Context mContext) {
		     AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		      builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
		             .setCancelable(false)
		             .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		                 public void onClick( DialogInterface dialog,   int id) {
		                	 mContext.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
		                 }
		             })
		             .setNegativeButton("No", new DialogInterface.OnClickListener() {
		                 public void onClick( DialogInterface dialog, int id) {
		                      dialog.cancel();
		                 }
		             });
		       AlertDialog alert = builder.create();
		      alert.show();
		  
		 }



	public static int getExifOrientation(String filepath) {
		int degree = 0;
		ExifInterface exif = null;
		try {
			exif = new ExifInterface(filepath);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		if (exif != null) {
			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION, -1);
			if (orientation != -1) {
				// We only recognise a subset of orientation tag values.
				switch (orientation) {
					case ExifInterface.ORIENTATION_ROTATE_90:
						degree = 90;
						break;
					case ExifInterface.ORIENTATION_ROTATE_180:
						degree = 180;
						break;
					case ExifInterface.ORIENTATION_ROTATE_270:
						degree = 270;
						break;
				}

			}
		}

		return degree;
	}
		
		
		public static String getDate(long milliSeconds, String dateFormat)
		{
		    // Create a DateFormatter object for displaying date in specified format.
		    SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

		    // Create a calendar object that will convert the date and time value in milliseconds to date. 
		     Calendar calendar = Calendar.getInstance();
		     calendar.setTimeInMillis(milliSeconds);
		     return formatter.format(calendar.getTime());
		}
	public static void DialogBox(Context context, String msg) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setMessage(msg);

		alertDialogBuilder.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				//  Toast.makeText(MainActivity.this,"You clicked yes button",Toast.LENGTH_LONG).show();
				//finish();
			}
		});


		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}
		
/*	public static String getDirectionsUrl(LatLng origin,LatLng dest){
		 
		  String url = "";
      // Origin of route
		try {
			
		
      String str_origin = "origin="+origin.latitude+","+origin.longitude;

      // Destination of route
      String str_dest = "destination="+dest.latitude+","+dest.longitude;

      // Sensor enabled
      String sensor = "sensor=false";

      // Building the parameters to the web service
      String parameters = str_origin+"&"+str_dest+"&"+sensor+"&units=imperial";

      // Output format
      String output = "json";

      // Building the url to the web service
      url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
      return url;
  }*/

	public static String streamToString(InputStream is) throws IOException {
		String str = "";

		if (is != null) {
			StringBuilder sb = new StringBuilder();
			String line;

			try {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is));

				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}

				reader.close();
			} finally {
				is.close();
			}

			str = sb.toString();
		}

		return str;
	}

	public static String getTWTimeString(Long fromdate) {

		long then;
		then = fromdate;
		//Log.e("time", then + "");
		Date date = new Date(then);

		StringBuffer dateStr = new StringBuffer();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		Calendar now = Calendar.getInstance();

		int year = yearBetween(calendar.getTime(), now.getTime());
		int months = monthBetween(calendar.getTime(), now.getTime());
		int days = daysBetween(calendar.getTime(), now.getTime());
		int minutes = minuteBetween(calendar.getTime(), now.getTime());
		int hours = hoursBetween(calendar.getTime(), now.getTime());
	//	Log.e("days", "" + days + " aaaaa " + hours);

		if (days == 0) {

			int second = secondsBetween(calendar.getTime(), now.getTime());
			if (minutes > 60) {

				// dateStr.append(hours).append(hours > 1 ? "h" : "h");

				if (hours == 1) {
					dateStr.append(hours).append(" hour ago");
				} else if (hours > 1 && hours <= 24) {
					dateStr.append(hours).append(" hours ago");
				}

			} else {

				if (second <= 10) {
					dateStr.append("Now");
				} else if (second > 10 && second <= 30) {
					dateStr.append("few seconds ago");
				} else if (second > 30 && second <= 60) {
					dateStr.append(second).append(" seconds ago");
				} else if (second >= 60 && minutes <= 60) {
					dateStr.append(minutes).append(" minutes ago");
				}
			}
		} else if (hours >= 24 && days == 1) {

			dateStr.append(days).append(" day ago");

		} else if (hours > 24 && days <= 31 && days > 1) {

			dateStr.append(days).append(" days ago");
		} else if (days > 31 && days < 365) {

			dateStr.append(months).append(" month ago");
		} else if (days > 365) {

			dateStr.append(year).append(" year ago");

		} else {
			dateStr.append(sdf.format(date));

		}

		return dateStr.toString();
	}

	public static int secondsBetween(Date d1, Date d2) {
		return (int) ((d2.getTime() - d1.getTime()) / DateUtils.SECOND_IN_MILLIS);
	}

	public static int minuteBetween(Date d1, Date d2) {
		return (int) ((d2.getTime() - d1.getTime()) / DateUtils.MINUTE_IN_MILLIS);
	}

	public static int hoursBetween(Date d1, Date d2) {
		return (int) ((d2.getTime() - d1.getTime()) / DateUtils.HOUR_IN_MILLIS);
	}

	public static int daysBetween(Date d1, Date d2) {
		return (int) ((d2.getTime() - d1.getTime()) / DateUtils.DAY_IN_MILLIS);
	}

	public static int monthBetween(Date d1, Date d2) {
		return (int) ((d2.getTime() - d1.getTime()) / (DateUtils.DAY_IN_MILLIS * 30));
	}

	public static int yearBetween(Date d1, Date d2) {
		return (int) ((d2.getTime() - d1.getTime()) / (DateUtils.YEAR_IN_MILLIS));
	}

	public static Boolean chkIfHitWebserviceOrNot(Long timeinmili) {
		Boolean bool = false;
		Date currentdate = new Date(System.currentTimeMillis());
		Date Nextdate = new Date(timeinmili);
		System.out.println("CURRENT  " + currentdate);
		System.out.println("Next " + Nextdate);
		int i = CommonUtils.secondsBetween(currentdate, Nextdate);
		System.out.println("TIME LEFT " + i);
		if (i < 0) {
			bool = true;
		} else {
			bool = false;
		}

		return bool;

	}


}
