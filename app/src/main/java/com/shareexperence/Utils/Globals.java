package com.shareexperence.Utils;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.shareexperence.R;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

;

/**
 * Created by netset on 4/8/2016.
 */
public class Globals {
    public static FragmentManager FRAGMENT_MANAGER, FM_TAB2;
            public static Fragment fragment;
             ;
     public static FragmentTransaction fragmentTransaction,fragmentTranSearch;

    public static JSONObject jsonObj, jsnObj1;
    public static JSONArray jsonArr;
   //  public static ParamsGetter paramsGetter=new ParamsGetter();
public  static int frag_id = R.id.frag_container ;
    public static String tag="P",sGCMID="",Accept="Accept",Reject="Reject",Open="Open";
    public static String strFCM_ID="";
//public static LatLng currentlatlng  ;
    public static String strlat = "";
    public static String strlong = "";
 public static List<ParamsGetter> getterList=new ArrayList<>();
    public static String CategoryName = "";
    public static String mConsumerKey = "";
    public static String mSecretKey = "";
    public static String SECRET = "";
    public static  AudioManager am;
    public static String TOKEN = "";
public static boolean fragmentcheck = false ;
public static String Tittle = "" ;
public static  String Key= "" ;
    public static  String ExpID= "" ;
    public static  String ID= "" ;

    public static boolean Homecheck = false;

    public static ArrayList<Gettersetter> Videopostion = new ArrayList<Gettersetter>();

    public static class Webservice{

        public static String Register="register";
        public static String Login="login";
        public static String Getprofile="getProfile";
        public static String Editprofile="editProfile";
        public static String GetAllExpreience="getAllExperience";
        public static String AddExperience="addExperience";
        public static String SendMsg="sendMessage";
        public static String Getallmsg="getUserAllMessage";
        public static String Singlechat="messageSingleThread";
        public static String SubmitAgreement="submitAggrement";
        public static String DeleteSingleMessage="deleteSingleMessage";
        public static String ContactUsWithName="contactUsWithName";
        public static String Logout="logout";
        public static String GetNoti="getNotification";
        public static String GetAgreement="getAgreement";
        public static String ChangeAggreementStatus="changeAggreementStatus";
        public static String Editagrement="editAggrement";
        public static String Foundagreement="getUserAceeptedAgreement";
        public static String DeleteNoti="deleteSingleNotification";
        public static String MyExperiences="getMyExperience";
        public static String DeletemyExp="deleteMyExperience";

    }

/*
    public static void Logout(final Context c){

       final SweetAlertDialog pDialog_logout = new SweetAlertDialog(c, SweetAlertDialog.WARNING_TYPE);
        pDialog_logout.setCancelable(false);
        pDialog_logout.setCanceledOnTouchOutside(false);
        pDialog_logout.setTitleText("Logout?");
        pDialog_logout.setContentText("Do you want to logout?");
        pDialog_logout.setConfirmText("Yes");
        pDialog_logout.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {

                pDialog_logout.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(c instanceof FragmentActivity)
                        GeneralValues.set_user_id((FragmentActivity)c,"");
                        else if(c instanceof AppCompatActivity)
                            GeneralValues.set_user_id((AppCompatActivity)c,"");

                        Intent in=new Intent(c, Splash.class);
                        c.startActivity(in);
                        ((FragmentActivity)c).finish();

                    }
                }, 1000);

            }
        });
        pDialog_logout.setCancelText("No");
        pDialog_logout.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog_logout.dismissWithAnimation();
            }
        });
        pDialog_logout.show();

    }
    public static void Success(final Context c,final Class classs,String str_msg){

        final SweetAlertDialog pDialog_logout = new SweetAlertDialog(c, SweetAlertDialog.SUCCESS_TYPE);
        pDialog_logout.setCancelable(false);
        pDialog_logout.setCanceledOnTouchOutside(false);
        pDialog_logout.setTitleText("Success");
        pDialog_logout.setContentText(str_msg);
        pDialog_logout.setConfirmText("OK");
        pDialog_logout.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {

                pDialog_logout.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Intent in=new Intent(c, classs);
                        c.startActivity(in);
                        ((FragmentActivity)c).finish();

                    }
                }, 1000);

            }
        });

        pDialog_logout.show();

    }
*/



    public static boolean CheckPermissions(Context c,String permission,int requestcode){
// Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(c,
                permission)
                != PackageManager.PERMISSION_GRANTED) {
            Log.e("checkSelfPermission","PERMISSION_GRANTED");
// Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(((Activity)c),
                    permission)) {
                Log.e("checkSelfPermission","shouldShowRequestPermissionRationale");
// Show an expanation to the user asynchronously -- don't block
// this thread waiting for the user's response! After the user
// sees the explanation, try again to request the permission.
                if(c instanceof AppCompatActivity) {
                    Log.e("AppCompatActivity", "true");
                    ActivityCompat.requestPermissions(((AppCompatActivity) c),
                            new String[]{permission},
                            requestcode);
                }
                else if(c instanceof FragmentActivity){
                    Log.e("FragmentActivity","true");
                    ActivityCompat.requestPermissions(((FragmentActivity) c),
                            new String[]{permission},
                            requestcode);}
                else if(c instanceof  Activity){
                    ActivityCompat.requestPermissions((( Activity) c),
                            new String[]{permission},
                            requestcode);}

            } else {
// No explanation needed, we can request the permission.
                if(c instanceof AppCompatActivity)
                    ActivityCompat.requestPermissions(((AppCompatActivity) c),
                            new String[]{permission},
                            requestcode);
                else if(c instanceof FragmentActivity){
                    ActivityCompat.requestPermissions(((FragmentActivity) c),
                            new String[]{permission},
                            requestcode);}

                else if(c instanceof  Activity){
                    ActivityCompat.requestPermissions((( Activity) c),
                            new String[]{permission},
                            requestcode);}
                Log.e("checkSelfPermission","no shouldShowRequestPermissionRationale");
// MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
// app-defined int constant. The callback method gets the
// result of the request.
            }
            return false;
        }else{
            return true;
        }
    }
}
