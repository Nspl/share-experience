package com.shareexperence;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.widget.RelativeLayout;

import com.crashlytics.android.Crashlytics;
import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.shareexperence.Utils.Font;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.activites.Login;
import com.shareexperence.activites.MainActivity;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

public class Splash extends Activity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "testapp5057@gmail.com";
    private static final String TWITTER_SECRET = "apptest";


    @Bind(R.id.dot_progress_bar)
    DotProgressBar dot_progress_bar;

    @Bind(R.id.main)
    RelativeLayout main;

    CountDownTimer timer, counter;
    private long mInterval, seconds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig), new Crashlytics());
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);
      //  showHashKey(getApplicationContext());




        Font.setDefaultFont(this, "DEFAULT", "font_maaxbold.otf");
        Font.setDefaultFont(this, "MONOSPACE", "font_maaxbold.otf");
        Font.setDefaultFont(this, "SANS_SERIF", "font_maaxbold.otf");

        handler=new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (GeneralValues.get_loginbool(Splash.this)) {

                    startActivity(new Intent(Splash.this, MainActivity.class));
                    finish();

                } else {
                    startActivity(new Intent(Splash.this, Login.class));
                    finish();
                }

            }
        }, 3500);


        counter = new MyCount(3000, 1000);
        counter.start();

     //   mExplosionField = ExplosionField.attach2Window(Splash.this);



        //	v.startAnimation(aSlideUp);

        new Thread(null, runnable, "").start();

    }


	/*public static void showHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.purl.purlieu", PackageManager.GET_SIGNATURES); //Your            package name here
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", "===KeyHash: " +Base64.encodeToString(md.digest(), Base64.DEFAULT));
                }
        } catch (NameNotFoundException e) {
         e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
         e.printStackTrace();

        }
    }
	*/

    public static void showHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.shareexperence", PackageManager.GET_SIGNATURES); //Your            package name here
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", "===KeyHash: " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();

        }
    }
    public class MyCount extends CountDownTimer {
        Context mContext;

        public MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        public void onTick(long millisUntilFinished) {
            mInterval = millisUntilFinished;

        }

        public void onFinish() {
            counter.cancel();

            try {
             } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    Runnable runnable = new Runnable() {

        @Override
        public void run() {
            try {
                Thread.sleep(3500);
                handler.dispatchMessage(new Message());

            } catch (InterruptedException e) {
                e.printStackTrace();
                finish();
            }

        }
    };

    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {


                startActivity(new Intent(Splash.this, Login.class));
                finish();

        }
    };

}