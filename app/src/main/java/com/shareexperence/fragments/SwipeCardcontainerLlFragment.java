package com.shareexperence.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.shareexperence.R;
import com.shareexperence.Utils.Font;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Globals;
import com.shareexperence.andtinder.model.CardModel;
import com.shareexperence.andtinder.view.CardContainer;
import com.shareexperence.andtinder.view.SimpleCardStackAdapter;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SwipeCardcontainerLlFragment extends Fragment implements MainAsynListener<String> {

    @Bind(R.id.layoutview)
    CardContainer mCardContainer;
    Context thiscontext;
    CardModel cardModel;
    View dislike, like;
    SimpleCardStackAdapter adapter;
    @Bind(R.id.refresh)
    ImageView refresh;
    String user_id, title, thumbnail, id,video;
    int position;

    public static SwipeCardcontainerLlFragment newInstance() {
        return new SwipeCardcontainerLlFragment();
    }

    public SwipeCardcontainerLlFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        thiscontext = container.getContext();
        return inflater.inflate(R.layout.swipe_cardcontainer_ll, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        Font.setDefaultFont(getActivity(), "DEFAULT", "fontmaaxbold.otf");
        Font.setDefaultFont(getActivity(), "MONOSPACE", "fontmaaxbold.otf");
        Font.setDefaultFont(getActivity(), "SANS_SERIF", "fontmaaxbold.otf");

    }

    @OnClick(R.id.refresh)
    void setRefresh() {
        mCardContainer.clearStack();
        adapter=null;
        adapter=new SimpleCardStackAdapter(thiscontext);
        Globals.getterList = new ArrayList<>();
        Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));


        new MainAsyncTask(getActivity(), Globals.Webservice.GetAllExpreience, 1, SwipeCardcontainerLlFragment.this, "Form", Globals.getterList).execute();

    }

    @Override
    public void onResume() {
        super.onResume();
        adapter = new SimpleCardStackAdapter(thiscontext);
        Globals.getterList = new ArrayList<>();
        Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));


        new MainAsyncTask(getActivity(), Globals.Webservice.GetAllExpreience, 1, SwipeCardcontainerLlFragment.this, "Form", Globals.getterList).execute();


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {

        if (isSucess) {
            if (result != null) {
                try {
                    if (flag == 1) {

                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        mCardContainer.clearStack();
                        if (status.equalsIgnoreCase("true")) {

                            JSONArray ja = Globals.jsonObj.getJSONArray("data");
                            for (int arr = 0; arr < ja.length(); arr++) {
                                JSONObject json1 = ja.getJSONObject(arr);

                                user_id = json1.getString("user_id");
                                title = json1.getString("title");
                                thumbnail = json1.getString("thumbnail");
                                id = json1.getString("id");
                                video= json1.getString("video");
                                cardModel = new CardModel(title, id, thumbnail, user_id, video);
                                adapter.add(cardModel);
                            }

                            mCardContainer.setAdapter(adapter);
                            position = mCardContainer.getChildCount();
                            cardModel.setOnClickListener(new CardModel.OnClickListener() {
                                @Override
                                public void OnClickListener() {
                                    int pos = mCardContainer.getChildCount();
                                    Log.e("Swipeable Cards", "I am pressing the card " + pos);
                                }
                            });
                            cardModel.setOnCardDismissedListener(new CardModel.OnCardDismissedListener() {
                                @Override
                                public void onLike() {
                                    int pos = mCardContainer.getChildCount();
                                    int val = pos - 1;
                                    int current = (val - position) + 1;
                                    Log.e("total cards", " ??>>" + Math.abs(current));
                                    current = Math.abs(current);
                                    CardModel CurrentcardModel = (CardModel) mCardContainer.getAdapter().getItem(current);
                                    String user_id = CurrentcardModel.getUserid();
                                    Log.e("Swipeable Cards", "I like the card" + " " + user_id);
                                    // likeUserStatus(user_id, "L");
                                }

                                @Override
                                public void onDislike() {
                                    int pos = mCardContainer.getChildCount();
                                    int val = pos - 1;
                                    int current = (val - position) + 1;
                                    Log.e("total cards", " ??>>" + Math.abs(current));
                                    current = Math.abs(current);
                                    CardModel CurrentcardModel = (CardModel) mCardContainer.getAdapter().getItem(current);
                                    String user_id = CurrentcardModel.getUserid();
                                    Log.e("Swipeable Cards", "I dislike the card" + " " + user_id);
                                    // likeUserStatus(user_id, "D");
                                }

                                @Override
                                public void OnsuperLike() {
                                    int pos = mCardContainer.getChildCount();
                                    int val = pos - 1;
                                    int current = (val - position) + 1;
                                    Log.e("total cards", " ??>>" + Math.abs(current));
                                    current = Math.abs(current);
                                    CardModel CurrentcardModel = (CardModel) mCardContainer.getAdapter().getItem(current);
                                    String user_id = CurrentcardModel.getUserid();
                                    Log.e("Swipeable Cards", "I Superlike the card" + " " + user_id);
                                   /* if (Constants.SparKGiven.equals("false"))
                                        likeUserStatus(user_id, "S");
                                }*/
                                }
                            });

                            // age.setText(first);


                        } else {

                        }
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        }


    }

    @Override
    public void onPostError(int flag) {

    }
}
