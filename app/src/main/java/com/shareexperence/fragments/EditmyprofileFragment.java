package com.shareexperence.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.InputFilter;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.shareexperence.R;
import com.shareexperence.Utils.CommonUtils;
import com.shareexperence.Utils.Font;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Globals;
import com.shareexperence.Utils.Methods;
import com.shareexperence.activites.ActivityDdexpActivity;
import com.shareexperence.activites.MainActivity;
import com.shareexperence.activites.Messages;
import com.shareexperence.activites.ProfilevideoCamera;
import com.shareexperence.activites.SettingActivity;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class EditmyprofileFragment extends Fragment implements MainAsynListener<String> {

    @Bind(R.id.main) RelativeLayout main;
    @Bind(R.id.fm) FrameLayout fm;

    @Bind(R.id.experience_txt) ImageView experienceTxt;
     @Bind(R.id.profile) ImageView profile;
    @Bind(R.id.r2) RelativeLayout r2;
     @Bind(R.id.input_name) EditText input_name;

    @Bind(R.id.input_age) TextView inputAge;

    @Bind(R.id.input_sex) TextView input_sex;

    @Bind(R.id.submit) ImageView submit;

    @Bind(R.id.input_from) AutoCompleteTextView input_from;

    @Bind(R.id.input_pref) EditText inputPref;

    @Bind(R.id.about) EditText about;



    @Bind(R.id.scropl)
    ScrollView scropl;

    @Bind(R.id.input_phn) EditText inputPhn;
    @Bind(R.id.setting) ImageView setting;


    @Bind(R.id.home) ImageView home;
    @Bind(R.id.msg) ImageView msg;
    @Bind(R.id.addphoto)
    LinearLayout addphoto;

    @Bind(R.id.videol)
    LinearLayout videol;


    @Bind(R.id.scrool)
    LinearLayout scrool;


Bundle bundle;
String Name ,email,location,gender ,About,Id,telephone,username,hash_tag_search,age;

    File file;
    Uri selectedImage;
    String image_path = "";

    ArrayList<String> country  ;
    Handler handler ;
    ArrayAdapter<String> stringArrayAdapter ;

    Calendar myCalendar ;
    DatePickerDialog.OnDateSetListener dateSetListener ;

// OnSwipeTouchListener swipe;
    GestureDetector gestureDetector;

    public static EditmyprofileFragment newInstance() {
        return new EditmyprofileFragment();
    }

    public EditmyprofileFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.editmyprofile, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Font.setDefaultFont(getActivity(), "DEFAULT", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "MONOSPACE", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "SANS_SERIF", "font_maaxbold.otf");

        main.setTranslationY(-getWidth());
        main.setAlpha(0);

        main.animate().setDuration(600).translationYBy(getWidth()).alpha(1);

        inputPref.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

          GeneralValues.set_profile(getActivity(),"0");


        country=new ArrayList<>();
        handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < getResources().getStringArray(R.array.country_arrays).length; i++) {
                        country.add(getResources().getStringArray(R.array.country_arrays)[i]);
                    }
                    stringArrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.places_custom, R.id.text1, country);
                    input_from.setAdapter(stringArrayAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 2000);


        myCalendar = Calendar.getInstance();

        dateSetListener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

    }

    public int getWidth() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int w = display.getWidth();
        return w;
    }


    @OnClick(R.id.input_age) void Setinput_age(){
        new DatePickerDialog(getActivity(), dateSetListener, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @OnClick(R.id.input_sex) void Setgender(){
        selectImage();
    }

    @OnClick(R.id.videol) void Setvideol(){
       Intent in = new Intent(getActivity(), ProfilevideoCamera.class);
        startActivity(in);
        getActivity().finish();
    }



    @OnClick(R.id.submit) void Setsubmit(){
       Editprofile();
    }


    @OnClick(R.id.setting) void setting(){


        Intent qn= new Intent (getActivity(), SettingActivity.class);
        getActivity().startActivity(qn);

    }



    @OnClick(R.id.addphoto) void addphoto(){

        if(Methods.checkPermissionForGallary(getActivity())) {
            Intent intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, 2);
        } else{
           Methods.requestPermissionForGallry(getActivity());
        }


    }

    @OnClick(R.id.home) void sethome(){

        Intent qn= new Intent (getActivity(), MainActivity.class);
        getActivity().startActivity(qn);


    }



    @OnClick(R.id.experience_txt)void setAddexp(){


        Intent qn= new Intent (getActivity(), ActivityDdexpActivity.class);
        getActivity().startActivity(qn);




    }

    @OnClick(R.id.profile) void profile(){



    }

    @OnClick(R.id.msg)void setmsg(){

        GeneralValues.set_msgtype(getActivity(),"0");

        Intent qn= new Intent (getActivity(), Messages.class);
        startActivity(qn);


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    public  void Editprofile(){

        Globals.getterList = new ArrayList<>();
        Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));
        Globals.getterList.add(new ParamsGetter("full_name", input_name.getText().toString()));
        Globals.getterList.add(new ParamsGetter("telephone",  inputPhn.getText().toString()));
        Globals.getterList.add(new ParamsGetter("username", ""));
        Globals.getterList.add(new ParamsGetter("gender", input_sex.getText().toString()));
        Globals.getterList.add(new ParamsGetter("location", input_from.getText().toString()));
        Globals.getterList.add(new ParamsGetter("age", inputAge.getText().toString()));
        Globals.getterList.add(new ParamsGetter("about", about.getText().toString()));
        Globals.getterList.add(new ParamsGetter("hash_tag_search", inputPref.getText().toString()));
        Globals.getterList.add(new ParamsGetter("notification_status", ""));


        if (file != null) {
            Log.e("Send image path...", "" + file);

            Globals.getterList.add(new ParamsGetter("image", file));
        }

        new MainAsyncTask(getActivity(), Globals.Webservice.Editprofile, 2, EditmyprofileFragment.this, "Multipart", Globals.getterList).execute();

    }

    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {
        if (isSucess) {
            if (result != null) {

                try {
                     if (flag == 2) {
                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        if (status.equalsIgnoreCase("true")) {


                            final SweetAlertDialog pDialog_logout = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                            pDialog_logout.setCancelable(false);
                            pDialog_logout.setCanceledOnTouchOutside(false);
                            pDialog_logout.setTitleText("Success");
                            pDialog_logout.setContentText("Profile Updated Successfully");
                            pDialog_logout.setConfirmText("OK");
                            pDialog_logout.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {

                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    pDialog_logout.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {


                                            pDialog_logout.dismissWithAnimation();
                                            Globals.getterList= new ArrayList<>();
                                            Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));


                                            new MainAsyncTask(getActivity(), Globals.Webservice.Getprofile,1,EditmyprofileFragment.this,"Form", Globals.getterList).execute();

                                        }
                                    }, 500);
                                }
                            });

                            pDialog_logout.show();

                        }
                     }else if(flag==1){

                         Globals.jsonObj = new JSONObject(result);
                         String status = Globals.jsonObj.getString("status");
                         if (status.equalsIgnoreCase("true")) {
                           r2.setVisibility(View.VISIBLE);
                              JSONArray ja = Globals.jsonObj.getJSONArray("data");
                             for (int arr = 0; arr < ja.length(); arr++) {
                                 JSONObject json1 = ja.getJSONObject(arr);

                                 email = json1.getString("email");
                                 Name = json1.getString("full_name");
                                 location = json1.getString("location");
                                 gender = json1.getString("gender");
                                 Id = json1.getString("user_id");

                                 age = json1.getString("age");
                                 telephone = json1.getString("telephone");
                                 username = json1.getString("username");
                                 hash_tag_search = json1.getString("hash_tag_search");
                                 About = json1.getString("about");


                             }


      /*    String CurrentString = Name ;
 		StringTokenizer tokens = new StringTokenizer(CurrentString, " ");
		String first = tokens.nextToken();
		String second = tokens.nextToken();
*/

                              input_name.setText(Name);
                          //   input_from.setHint(Name);
                             if (gender.equalsIgnoreCase("M")) {
                                 input_sex.setText("Male");
                             } else if (gender.equalsIgnoreCase("F")) {
                                 input_sex.setText("Female");
                             }
                             input_from.setText(location);

                             if (age.equalsIgnoreCase("")){
                                 inputAge.setText("Add birth date");
                             }else {
                                 inputAge.setText(age);
                             }


                             if (hash_tag_search.equalsIgnoreCase("")){
                                 inputPref.setText("Keyword in search");
                             }else {
                                 inputPref.setText(hash_tag_search);
                             }

                             if (About.equalsIgnoreCase("")){
                                 about.setText("Something about me");
                             }else {
                                 about.setText(About);
                             }

                             if (telephone.equalsIgnoreCase("")){
                                 inputPhn.setText("Add phone");
                             }else {
                                 inputPhn.setText(telephone);
                             }


                         //    inputUser.setHint(username);

                         }else {

                             r2.setVisibility(View.VISIBLE);
                         }

                     }
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        }

    }

    @Override
    public void onPostError(int flag) {

    }

    public void selectImage() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.spinner_text);


        LinearLayout Male = (LinearLayout) dialog
                .findViewById(R.id.ll_gallery);
        Male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                input_sex.setText("Male");
                dialog.dismiss();
            }
        });
        LinearLayout Female = (LinearLayout) dialog
                .findViewById(R.id.ll_cancel);
        Female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                input_sex.setText("Female");

                dialog.dismiss();
            }
        });
        dialog.show();

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == 2) {

                if (resultCode != Activity.RESULT_CANCELED) {
                    try {
                        selectedImage = data.getData();
                        if (selectedImage != null) {
                            String[] filePath = { MediaStore.Images.Media.DATA };
                            Cursor c = getActivity().getContentResolver().query(
                                    selectedImage, filePath, null, null, null);
                            c.moveToFirst();
                            int columnIndex = c.getColumnIndex(filePath[0]);
                            String picturePath = c.getString(columnIndex);
                            c.close();

                            Bitmap bitmap;
                            int rotation_degree = CommonUtils
                                    .getExifOrientation(picturePath);
                            Log.e("ORIENT", rotation_degree + "");

                            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                            bitmapOptions.inSampleSize = 2;
                            bitmap = BitmapFactory.decodeFile(picturePath,
                                    bitmapOptions);
                            Matrix matrix = new Matrix();
                            matrix.postRotate(rotation_degree);
                            bitmap = Bitmap.createBitmap(bitmap, 0, 0,
                                    bitmap.getWidth(), bitmap.getHeight(),
                                    matrix, true);
                            image_path = CommonUtils.saveimagetosdcard(
                                    getActivity(), bitmap);
                            file = new File(image_path);



                            Editprofile();


                        }
                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                }
            }
        }
    }
    private void updateLabel() {

        String myFormat = "dd.MM.yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        inputAge.setText(sdf.format(myCalendar.getTime()));
    }


    @Override
    public void onResume() {
        super.onResume();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                try {
                    Globals.getterList= new ArrayList<>();
                    Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));


                    new MainAsyncTask(getActivity(), Globals.Webservice.Getprofile,1,EditmyprofileFragment.this,"Form", Globals.getterList).execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, 1000);







    }





}
