package com.shareexperence.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shareexperence.R;
import com.shareexperence.Utils.Font;
import com.shareexperence.andtinder.model.CardModel;
import com.shareexperence.andtinder.view.CardContainer;
import com.shareexperence.andtinder.view.SimpleCardStackAdapter;

/**
 * Created by netset on 11/10/16.
 */
public class SwipeFragment extends Fragment{
    private CardContainer mCardContainer;
    Context thiscontext;
    CardModel cardModel;
    View dislike, like;
    SimpleCardStackAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.swipe_cardcontainer_ll, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Font.setDefaultFont(getActivity(), "DEFAULT", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "MONOSPACE", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "SANS_SERIF", "font_maaxbold.otf");

        mCardContainer = (CardContainer) view.findViewById(R.id.layoutview);
        adapter = new SimpleCardStackAdapter(thiscontext);

    }



}
