package com.shareexperence.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.shareexperence.R;
import com.shareexperence.Utils.Font;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Gettersetter;
import com.shareexperence.Utils.Globals;
import com.shareexperence.activites.ActivityDdexpActivity;
import com.shareexperence.activites.MainActivity;
import com.shareexperence.activites.Profile;
import com.shareexperence.activites.SinglechatactiActivity;
import com.shareexperence.adapter.Adap_chatlist;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MessagesFragment extends Fragment implements MainAsynListener<String> {

    @Bind(R.id.main)  RelativeLayout main;
    @Bind(R.id.R1) RelativeLayout r1;
    @Bind(R.id.experience_txt) ImageView experienceTxt;

    @Bind(R.id.home) ImageView home;
    @Bind(R.id.msg)
    ImageView msg;
    @Bind(R.id.profile) ImageView profile;


    @Bind(R.id.Comment)
    TextView Comment;

    @Bind(R.id.list)
    ListView list;
    ArrayList<Gettersetter> arrchat;
    Gettersetter getset;
    String ID,Name,Image ,post_title,experience_id,profile_image;
    Adap_chatlist adap ;

    Bundle bundle ;

  //  OnSwipeTouchListener swipe;
    GestureDetector gestureDetector;

    public static MessagesFragment newInstance() {
        return new MessagesFragment();
    }

    public MessagesFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.messages, null);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Font.setDefaultFont(getActivity(), "DEFAULT", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "MONOSPACE", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "SANS_SERIF", "font_maaxbold.otf");

        arrchat = new ArrayList<>();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                main.setVisibility(View.VISIBLE);

                YoYo.with(Techniques.SlideInUp).duration(1000).playOn(main);

            }
        }, 200);


        GeneralValues.set_msgtype(getActivity(),"0");




    }

    public int getWidth() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int w = display.getWidth();
        return w;
    }




    @OnClick(R.id.profile)void setprofile(){

          Intent qn= new Intent (getActivity(), Profile.class);
        getActivity().startActivity(qn);

    }


    @OnClick(R.id.experience_txt)void setAddexp(){
        Intent qn= new Intent (getActivity(), ActivityDdexpActivity.class);
        getActivity().startActivity(qn);


    }
     @OnClick(R.id.home)void sethome(){


        Intent qn= new Intent (getActivity(), MainActivity.class);
        getActivity().startActivity(qn);



    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);



    }


    @Override
    public void onDetach() {
        super.onDetach();


    }

    @Override
    public void onResume() {
        super.onResume();

        main.setVisibility(View.GONE);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                try {
                    Globals.getterList= new ArrayList<>();
                    Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));

                    new MainAsyncTask(getActivity(), Globals.Webservice.Getallmsg,1,MessagesFragment.this,"Form", Globals.getterList).execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, 1000);


    }

    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {
        if (isSucess) {
            if (result != null) {

                try {

                    if (flag==1) {
                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        if (status.equalsIgnoreCase("true")) {
                            arrchat.clear();
                            Comment.setVisibility(View.GONE);
                            main.setVisibility(View.VISIBLE);
                            JSONArray ja = Globals.jsonObj.getJSONArray("data");
                            for (int arr = 0; arr < ja.length(); arr++) {
                                JSONObject json1 = ja.getJSONObject(arr);

                                Name = json1.getString("full_name");
                                ID = json1.getString("other_user_id");
                                post_title= json1.getString("title");
                                experience_id= json1.getString("experience_id");
                                profile_image= json1.getString("profile_image");

                                getset = new Gettersetter();
                                getset.setStrName(Name);
                                getset.setStrimage(profile_image);
                                getset.setId(ID);
                                getset.setStrtittle(post_title);
                                getset.setExperinceId(experience_id);

                                arrchat.add(getset);

                            }

                            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                    Intent in = new Intent(getActivity(), SinglechatactiActivity.class);
                                    in.putExtra("otherid",arrchat.get(position).getId());

                                    in.putExtra("name",arrchat.get(position).getStrName());
                                    in.putExtra("image",arrchat.get(position).getStrimage());
                                    in.putExtra("tittle",arrchat.get(position).getStrtittle());
                                    in.putExtra("expid",arrchat.get(position).getExperinceId());
                                     startActivity(in);


                                }
                            });

                            adap = new Adap_chatlist(getActivity(), arrchat);
                            list.setAdapter(adap);
                            adap.notifyDataSetChanged();



                        }else{
                            main.setVisibility(View.VISIBLE);
                            arrchat.clear();
                            Comment.setVisibility(View.VISIBLE);
                        }

                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        }


    }

    @Override
    public void onPostError(int flag) {

    }




}




