package com.shareexperence.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.desarrollodroide.libraryfragmenttransactionextended.SlidingRelativeLayout;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.shareexperence.R;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Globals;
import com.shareexperence.Utils.Methods;
import com.shareexperence.activites.Login;
import com.shareexperence.activites.MainActivity;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;
import com.shareexperence.twitter.TwitterApp;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterSession;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import twitter4j.conf.ConfigurationBuilder;

public class SignupFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, MainAsynListener<String> {
    DatePickerDialog datePickerDialog;
    int mHour, mMinute, mYear, mMonth, mDay;
    @Bind(R.id.main)
    SlidingRelativeLayout main;


    @Bind(R.id.input_email)
    EditText inputEmail;

    @Bind(R.id.input_name)
    EditText inputName;
    @Bind(R.id.input_pswd)
    EditText input_pswd;

    String twitterName = "", twitterUserId = "";

    @Bind(R.id.fb)
    ImageView fb;

    @Bind(R.id.google)
    ImageView google;
    @Bind(R.id.twitter)
    ImageView twitter;


    @Bind(R.id.gender)
    TextView gender;

    @Bind(R.id.input_from)
    AutoCompleteTextView input_from;

     String formatted_date;
    @Bind(R.id.signup)
    ImageView signup;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;
    int check_value;
    ArrayList<String> country;
    Handler handler;
    ArrayAdapter<String> stringArrayAdapter;

    String FBid, Name;
    GoogleApiClient mGoogleApiClient;
    GoogleSignInOptions gso;
    FirebaseAuth mAuth;
    private HashMap<String, String> userInfoHashmap = new HashMap<String, String>();
    ArrayList<String> Facebook = new ArrayList<String>();
    Animation emptyAnimation;

    //twitter
    public static String twitterId = "";
    private TwitterApp mTwitter;
    ConfigurationBuilder builder;
    twitter4j.conf.Configuration conf;
    String url = "",unique_id;
    public static final String CONSUMER_KEY = "Cush3HXKFUOq14bkB5Q6urx82",
            CONSUMER_SECRET = "QwPrIp2tLvLS4p4O3HFDBiWGus9zwJuU4t4c9cEw7EhD7q5HVz",
            accessToken = "2421532010-Ui7SFqRxq087E8l647iK3lHPlMs8rCKOMJ0N47a",
            accessTokenSecret = "dvCrwWmPY59zS2QJ3yifPLZ2NinwEYXtVARyylVDtEdYN";
    public static CallbackManager callbackManager;
    private static final String TAG = "LoginFragment";


    public static SignupFragment newInstance() {
        return new SignupFragment();
    }

    public SignupFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.signup, null);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //Twitterr
        emptyAnimation = AnimationUtils.loadAnimation(getActivity(),
                R.anim.empty_animate);

        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                  GeneralValues.get_gcmid(getActivity());
        Log.e("Genervalues <>>><<<<<<<",GeneralValues.get_gcmid(getActivity())) ;

        Log.e("sdfkefjh<>>><<<<<<<",Globals.sGCMID) ;



         /*================= Twitter Related =================*/
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        mTwitter = new TwitterApp(getActivity(), CONSUMER_KEY, CONSUMER_SECRET);
        //-------------//

        ButterKnife.bind(this, view);

        Facebook.addAll(Arrays.asList("public_profile, email, user_friends, user_location, user_birthday, publish_actions"));


        LoginManager.getInstance().logOut();


        //--------------------------//
        country = new ArrayList<>();
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < getResources().getStringArray(R.array.country_arrays).length; i++) {
                        country.add(getResources().getStringArray(R.array.country_arrays)[i]);
                    }
                    stringArrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.places_custom, R.id.text1, country);
                    input_from.setAdapter(stringArrayAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 2000);

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        try {
            if (mGoogleApiClient == null) {
                mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                        .addConnectionCallbacks(this)
                        .enableAutoManage((FragmentActivity) getActivity(), null)
                        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                        .build();
            }
            if (!mGoogleApiClient.isConnected()) {
                mGoogleApiClient.connect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @OnClick(R.id.twitter)
    void settwiter() {


        mTwitter.setListener(mTwLoginDialogListener);
        mTwitter.resetAccessToken();

        if (mTwitter.hasAccessToken() == true) {
            try {
                mTwitter.updateStatus(url);
            } catch (Exception e) {
                e.printStackTrace();
            }
            mTwitter.resetAccessToken();
        } else {
            mTwitter.authorize();
        }
        //  Log.e("", "Twitter Id: " + twitterId);

    }


    @OnClick(R.id.google)
    void setgoogle() {

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, 6);

    }

    @OnClick(R.id.gender)
    void strgender() {
        selectImage();
    }


    @OnClick(R.id.fb)
    void setfb() {
        LoginManager.getInstance().logInWithReadPermissions(getActivity(), Facebook);
        LoginManager.getInstance().registerCallback(Login.callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //  Log.e("SUCCESS","SUCCESS");
                GraphRequestt(AccessToken.getCurrentAccessToken(), "id,name,email,age_range,link");
            }

            @Override
            public void onCancel() {
                //   Log.e("CANCEL","CANCEL");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e("error ", "error " + error);
            }
        });
    }

    //FACEBOOK
    public void GraphRequestt(AccessToken accessToken, String fields_to_fetch) {
        try {
            if (AccessToken.getCurrentAccessToken() != null) {
                GraphRequest request = GraphRequest.newMeRequest(
                        accessToken, new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                // Application code


                                try {
                                    //   Log.e("RESULT",    object.getString("name"));

                                    Name = object.getString("name");
                                    FBid = object.getString("id");
                                    String FBEmail = object.getString("email") ;

                                    ///   Log.e("Email Facebiook", FBEmail) ;


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Log.e("Genervalues <>>><<<<<<<",GeneralValues.get_gcmid(getActivity())) ;

                                Log.e("sdfkefjh<>>><<<<<<<",Globals.sGCMID) ;



                                Log.e("<><<<<<<<Name<><<><<><", Name + "<<<><><><FID<><<><<><" + FBid);
                                Globals.getterList = new ArrayList<>();
                                Globals.getterList.add(new ParamsGetter("email", ""));
                                Globals.getterList.add(new ParamsGetter("password", ""));
                                Globals.getterList.add(new ParamsGetter("social_media_id", FBid));
                                Globals.getterList.add(new ParamsGetter("full_name", Name));
                                Globals.getterList.add(new ParamsGetter("gender", gender.getText().toString().trim()));
                                Globals.getterList.add(new ParamsGetter("location", input_from.getText().toString().trim()));
                                Globals.getterList.add(new ParamsGetter("device_id", GeneralValues.get_gcmid(getActivity())));
                                Globals.getterList.add(new ParamsGetter("login_type", "F"));
                                Globals.getterList.add(new ParamsGetter("device_type", "A"));
                                // Globals.getterList.add(new ParamsGetter("_token","kvYQWD0t0nDGgnNPRkmypUTG5soGEUGiMWOEeOvL"));


                                new MainAsyncTask(getActivity(), Globals.Webservice.Register, 3, SignupFragment.this, "Form", Globals.getterList).execute();


                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,age_range,link");
                request.setParameters(parameters);
                request.executeAsync();


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String getFormatedDate(String initialDate) {
        String formattedDate = "";
        if (!initialDate.equalsIgnoreCase("")) {
            Date date = null;
            SimpleDateFormat initialDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            try {
                date = initialDateFormatter.parse(initialDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat requiredDateFormatter = new SimpleDateFormat("dd-MMMM-yyyy");
            formattedDate = requiredDateFormatter.format(date);
        }
        return formattedDate;
    }


    @OnClick(R.id.signup)
    void setSign() {

        if (checkFields()) {
             if (Methods.checkPermissionForAudio(getActivity())) {

                Simplesignup();
            } else {
                Methods.requestPermissionForAudio(getActivity());
            }

        }


    }

    public void Simplesignup() {

        Log.e("Genervalues <>>><<<<<<<",GeneralValues.get_gcmid(getActivity())) ;

        Log.e("sdfkefjh<>>><<<<<<<",Globals.sGCMID) ;


        Globals.getterList = new ArrayList<>();
        Globals.getterList.add(new ParamsGetter("email", inputEmail.getText().toString().trim()));
        Globals.getterList.add(new ParamsGetter("password", input_pswd.getText().toString().trim()));
        Globals.getterList.add(new ParamsGetter("social_media_id", ""));
        Globals.getterList.add(new ParamsGetter("full_name", inputName.getText().toString().trim()));
        Globals.getterList.add(new ParamsGetter("gender", gender.getText().toString().trim()));
        Globals.getterList.add(new ParamsGetter("location", input_from.getText().toString().trim()));
        Globals.getterList.add(new ParamsGetter("device_id", GeneralValues.get_gcmid(getActivity())));
        Globals.getterList.add(new ParamsGetter("login_type", "O"));
        Globals.getterList.add(new ParamsGetter("device_type", "A"));


        new MainAsyncTask(getActivity(), Globals.Webservice.Register, 1, SignupFragment.this, "Form", Globals.getterList).execute();




    }


    public boolean checkFields() {

        if (inputName.getText().toString().trim().length() == 0) {
            inputName.startAnimation(emptyAnimation);
            Snackbar.make(getView(), "Please enter username", Snackbar.LENGTH_SHORT).show();
            return false;
        } else if (inputEmail.getText().toString().trim().length() == 0) {
            inputEmail.startAnimation(emptyAnimation);
            Snackbar.make(getView(), "Please Enter Email Address", Snackbar.LENGTH_SHORT).show();
            return false;
        } else if (!validateEmailId(inputEmail.getText().toString())) {
            inputEmail.startAnimation(emptyAnimation);
            Snackbar.make(getView(), "Please Enter Valid Email", Snackbar.LENGTH_SHORT).show();

             return false;
        }



        else if (gender.getText().toString().trim().length() == 0) {
            gender.startAnimation(emptyAnimation);
            Snackbar.make(getView(), "Please Select Gender", Snackbar.LENGTH_SHORT).show();
            return false;
        } else if (input_pswd.getText().toString().trim().length() == 0) {
            input_pswd.startAnimation(emptyAnimation);
            Snackbar.make(getView(), "Please Enter Password", Snackbar.LENGTH_SHORT).show();
            return false;
        } else if (input_from.getText().toString().trim().length() == 0) {
            input_from.startAnimation(emptyAnimation);
            Snackbar.make(getView(), "Please Enter LOcation", Snackbar.LENGTH_SHORT).show();
            return false;
        } else
            return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public void selectImage() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.spinner_text);


        LinearLayout Male = (LinearLayout) dialog
                .findViewById(R.id.ll_gallery);
        Male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gender.setText("Male");
                dialog.dismiss();
            }
        });
        LinearLayout Female = (LinearLayout) dialog
                .findViewById(R.id.ll_cancel);
        Female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gender.setText("Female");

                dialog.dismiss();
            }
        });
        dialog.show();

    }

    //-----google +


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 6) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            Globals.fragment.onActivityResult(requestCode, resultCode, data);
            //  MainActivity.callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        // twitterLoginButton.onActivityResult(requestCode, resultCode, data);


    }


    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("GOOGLE SIGN IN", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
// Signed in successfully, show authenticated UI.
            final GoogleSignInAccount acct = result.getSignInAccount();

            // Log.e("Google id",acct.getId())     ;

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Log.e("Genervalues <>>><<<<<<<",GeneralValues.get_gcmid(getActivity())) ;

                    Log.e("sdfkefjh<>>><<<<<<<",Globals.sGCMID) ;



                    Globals.getterList = new ArrayList<>();
                    Globals.getterList.add(new ParamsGetter("email", acct.getEmail()));
                    Globals.getterList.add(new ParamsGetter("password", ""));
                    Globals.getterList.add(new ParamsGetter("social_media_id", acct.getId()));
                    Globals.getterList.add(new ParamsGetter("full_name", acct.getDisplayName()));
                    Globals.getterList.add(new ParamsGetter("gender", gender.getText().toString().trim()));
                    Globals.getterList.add(new ParamsGetter("location", input_from.getText().toString().trim()));
                    Globals.getterList.add(new ParamsGetter("device_id", GeneralValues.get_gcmid(getActivity())));
                    Globals.getterList.add(new ParamsGetter("login_type", "G"));
                    Globals.getterList.add(new ParamsGetter("device_type", "A"));
                    // Globals.getterList.add(new ParamsGetter("_token","kvYQWD0t0nDGgnNPRkmypUTG5soGEUGiMWOEeOvL"));


                    new MainAsyncTask(getActivity(), Globals.Webservice.Register, 2, SignupFragment.this, "Form", Globals.getterList).execute();


                }
            });


        } else {
// Signed out, show unauthenticated UI.
        }
    }

    private boolean validateEmailId(String email) {
        // TODO Auto-generated method stub
        final Pattern EMAIL_ADDRESS_PATTERN = Pattern
                .compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
                        + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
                        + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+");

        boolean validEmail = EMAIL_ADDRESS_PATTERN.matcher(email).matches();
        // validate email address
        if (!validEmail) {
            return false;
            // return false;
        }

        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    public void login(Result<TwitterSession> result) {

        //Creating a twitter session with result's data
        TwitterSession session = result.data;

        //Getting the username from session
        final String username = session.getUserName();

        //This code will fetch the profile image URL
        //Getting the account service of the user logged in


    }


    /*================= Twitter Related =================*/
    private TwitterApp.TwDialogListener mTwLoginDialogListener = new TwitterApp.TwDialogListener() {
        public void onError(String value) {
            mTwitter.resetAccessToken();
        }

        public void onComplete(String value) {
            try {

                //   GeneralValues.set_twitterid(getActivity(), twitterId);
                //  Toast.makeText(getContext(), "---- id" + twitterId, Toast.LENGTH_LONG).show();
                //   Log.e("", "Twitter Id: " + twitterId);

                twitterUserId = TwitterApp.getTwitterID();

                twitterName = TwitterApp.getUsername();

                //    Log.e("", "Twitter Name: " + twitterName);
                //  Log.e("", "Twitter Id: " +  twitterUserId);
                Log.e("Genervalues <>>><<<<<<<",GeneralValues.get_gcmid(getActivity())) ;

                Log.e("sdfkefjh<>>><<<<<<<",Globals.sGCMID) ;




                Globals.getterList = new ArrayList<>();
                Globals.getterList.add(new ParamsGetter("email", ""));
                Globals.getterList.add(new ParamsGetter("password", ""));
                Globals.getterList.add(new ParamsGetter("social_media_id", twitterUserId));
                Globals.getterList.add(new ParamsGetter("full_name", twitterName));
                Globals.getterList.add(new ParamsGetter("gender", gender.getText().toString().trim()));
                Globals.getterList.add(new ParamsGetter("location", input_from.getText().toString().trim()));
                Globals.getterList.add(new ParamsGetter("device_id", GeneralValues.get_gcmid(getActivity())));
                Globals.getterList.add(new ParamsGetter("login_type", "G"));
                Globals.getterList.add(new ParamsGetter("device_type", "A"));
                // Globals.getterList.add(new ParamsGetter("_token","kvYQWD0t0nDGgnNPRkmypUTG5soGEUGiMWOEeOvL"));


                new MainAsyncTask(getActivity(), Globals.Webservice.Register, 3, SignupFragment.this, "Form", Globals.getterList).execute();


            } catch (Exception e) {
                e.printStackTrace();
            }
            mTwitter.resetAccessToken();
        }
    };


    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {
        if (isSucess) {
            if (result != null) {
                try {

                    if (flag == 1) {
                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        String msg = Globals.jsonObj.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            GeneralValues.set_loginbool(getActivity(), true);
                            GeneralValues.set_user_id(getActivity(),
                                    Globals.jsonObj.getString("user_id"));

                            final SweetAlertDialog pDialog_logout = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                            pDialog_logout.setCancelable(false);
                            pDialog_logout.setCanceledOnTouchOutside(false);
                            pDialog_logout.setTitleText("Success");
                            pDialog_logout.setContentText("Sign Up Successfully");
                            pDialog_logout.setConfirmText("OK");
                            pDialog_logout.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    pDialog_logout.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                            pDialog_logout.dismissWithAnimation();

                                            Intent in = new Intent(getActivity(), MainActivity.class);
                                            getActivity().startActivity(in);
                                            getActivity().finish();

                                        }
                                    }, 500);
                                }
                            });

                            pDialog_logout.show();


                        } else {

                            Snackbar.make(getView(), "Please check Email or Password", Snackbar.LENGTH_SHORT).show();

                        }

                    } else if (flag == 2) {
                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        String msg = Globals.jsonObj.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            GeneralValues.set_loginbool(getActivity(), true);
                            GeneralValues.set_user_id(getActivity(),
                                    Globals.jsonObj.getString("user_id"));

                            final SweetAlertDialog pDialog_logout = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                            pDialog_logout.setCancelable(false);
                            pDialog_logout.setCanceledOnTouchOutside(false);
                            pDialog_logout.setTitleText("Success");
                            pDialog_logout.setContentText("Sign Up Successfully");
                            pDialog_logout.setConfirmText("OK");
                            pDialog_logout.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    pDialog_logout.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                            pDialog_logout.dismissWithAnimation();

                                            Intent in = new Intent(getActivity(), MainActivity.class);
                                            getActivity().startActivity(in);
                                            getActivity().finish();

                                        }
                                    }, 500);
                                }
                            });

                            pDialog_logout.show();
                        }
                    } else if (flag == 3) {
                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        String msg = Globals.jsonObj.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            GeneralValues.set_loginbool(getActivity(), true);
                            GeneralValues.set_user_id(getActivity(),
                                    Globals.jsonObj.getString("user_id"));



                            final SweetAlertDialog pDialog_logout = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                            pDialog_logout.setCancelable(false);
                            pDialog_logout.setCanceledOnTouchOutside(false);
                            pDialog_logout.setTitleText("Success");
                            pDialog_logout.setContentText("Sign Up Successfully");
                            pDialog_logout.setConfirmText("OK");
                            pDialog_logout.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    pDialog_logout.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                            pDialog_logout.dismissWithAnimation();

                                            Intent in = new Intent(getActivity(), MainActivity.class);
                                            getActivity().startActivity(in);
                                            getActivity().finish();

                                        }
                                    }, 500);
                                }
                            });

                            pDialog_logout.show();
                        }

                    } else if (flag == 4) {

                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        String msg = Globals.jsonObj.getString("message");
                        if (status.equalsIgnoreCase("true") && msg.equalsIgnoreCase("Successfully login")) {

                            GeneralValues.set_user_id(getActivity(),
                                    Globals.jsonObj.getString("user_id"));

                             GeneralValues.set_loginbool(getActivity(), true);

                            Intent in = new Intent(getActivity(), MainActivity.class);
                            startActivity(in);
                            getActivity().finish();
                        } else if (status.equalsIgnoreCase("true") && msg.equalsIgnoreCase("succesfully register with social")) {

                            GeneralValues.set_user_id(getActivity(),
                                    Globals.jsonObj.getString("user_id"));
                            GeneralValues.set_loginbool(getActivity(), true);

                        }


                    } else if (flag == 5) {

                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        String msg = Globals.jsonObj.getString("message");
                        if (status.equalsIgnoreCase("true") && msg.equalsIgnoreCase("Successfully login")) {

                            GeneralValues.set_user_id(getActivity(),
                                    Globals.jsonObj.getString("user_id"));

                             GeneralValues.set_loginbool(getActivity(), true);

                            Intent in = new Intent(getActivity(), MainActivity.class);
                            startActivity(in);
                            getActivity().finish();
                        } else if (status.equalsIgnoreCase("true") && msg.equalsIgnoreCase("succesfully register with social")) {

                            GeneralValues.set_user_id(getActivity(),
                                    Globals.jsonObj.getString("user_id"));
                            GeneralValues.set_loginbool(getActivity(), true);


                        }


                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        }

    }

    @Override
    public void onPostError(int flag) {
    }


}