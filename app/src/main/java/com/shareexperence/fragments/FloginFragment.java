package com.shareexperence.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.desarrollodroide.libraryfragmenttransactionextended.FragmentTransactionExtended;
import com.desarrollodroide.libraryfragmenttransactionextended.SlidingRelativeLayout;
import com.rey.material.widget.Button;
import com.shareexperence.R;
import com.shareexperence.Utils.Font;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Globals;
import com.shareexperence.Utils.Methods;
import com.shareexperence.activites.MainActivity;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class FloginFragment extends Fragment implements MainAsynListener<String> {

    @Bind(R.id.main)
    SlidingRelativeLayout main;
    @Bind(R.id.shimmer_tv)
    ImageView shimmer_tv;
    @Bind(R.id.input_name) EditText inputName;
    @Bind(R.id.input_pass) EditText inputPass;
    @Bind(R.id.sign)
    LinearLayout sign;
    @Bind(R.id.login)
    Button login;

     Handler handler ;
    Animation emptyAnimation ;

    public static FloginFragment newInstance() {
        return new FloginFragment();
    }

    public FloginFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.flogin, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Font.setDefaultFont(getActivity(), "DEFAULT", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "MONOSPACE", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "SANS_SERIF", "font_maaxbold.otf");

   ;

        emptyAnimation = AnimationUtils.loadAnimation(getActivity(),
                R.anim.empty_animate);

 GeneralValues.set_logintype(getActivity(),"0");




        if (Methods.checkPermissionForCamera(getActivity())){

 }else {

     Methods.requestPermissionForCamera(getActivity());

 }

    }

    @OnClick(R.id.sign)void setSign(){

        Globals.fragment= SignupFragment.newInstance();
        Globals.FRAGMENT_MANAGER = getActivity().getFragmentManager();
        Globals.fragmentTransaction = Globals.FRAGMENT_MANAGER.beginTransaction();
        FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(getActivity(), Globals.fragmentTransaction, FloginFragment.newInstance(), Globals.fragment, Globals.frag_id);
        fragmentTransactionExtended.addTransition(FragmentTransactionExtended.SLIDE_HORIZONTAL);
        fragmentTransactionExtended.commit();
    }


    @OnClick(R.id.login)void setlogin(){

        if (checkFields()) {


       if (Methods.checkPermissionForAudio(getActivity())){

           Globals.getterList= new ArrayList<>();
           Globals.getterList.add(new ParamsGetter("email", inputName.getText().toString().trim()));
           Globals.getterList.add(new ParamsGetter("password",  inputPass.getText().toString().trim()));
            Globals.getterList.add(new ParamsGetter("device_id", GeneralValues.get_gcmid(getActivity())));
            Globals.getterList.add(new ParamsGetter("device_type","A"));

           new MainAsyncTask(getActivity(), Globals.Webservice.Login,1,FloginFragment.this,"Form", Globals.getterList).execute();

        }else {
          Methods.requestPermissionForAudio(getActivity());

       }

 }

}


    public boolean checkFields() {

        if (inputName.getText().toString().trim().length() == 0) {
            inputName.startAnimation(emptyAnimation);

            Snackbar.make(getView(),"Please enter Email", Snackbar.LENGTH_SHORT).show();

            return false;
        } /* else if (!validateEmailId(inputName.getText().toString())) {
            inputName.startAnimation(emptyAnimation);
            Snackbar.make(getView(),"Email not valid.",Snackbar.LENGTH_SHORT).show();
            return false;
        }*/

        else if (inputPass.getText().toString().trim().length() == 0) {
            inputPass.startAnimation(emptyAnimation);
            Snackbar.make(getView(),"Please Enter Password", Snackbar.LENGTH_SHORT).show();
            return false;
        }
        else
            return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
    public static void showMessage( Context context) {
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View layout = inflater.inflate(R.layout.trial,
                (ViewGroup) ((Activity)context).findViewById(R.id.main));

        Toast toast = new Toast(context);
        toast.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 70);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {
        if (isSucess) {
            if (result != null) {
                try {

                    if (flag==1) {
                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        if (status.equalsIgnoreCase("true")) {

                            GeneralValues.set_loginbool(getActivity(), true);
                            GeneralValues.set_user_id(getActivity(),
                                    Globals.jsonObj.getString("user_id"));

                            final SweetAlertDialog pDialog_logout = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                            pDialog_logout.setCancelable(false);
                            pDialog_logout.setCanceledOnTouchOutside(false);
                            pDialog_logout.setTitleText("Success");
                            pDialog_logout.setContentText("Login Successfully");
                            pDialog_logout.setConfirmText("OK");
                            pDialog_logout.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {

                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    pDialog_logout.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {


                                            pDialog_logout.dismissWithAnimation();

                                            Intent in = new Intent(getActivity(), MainActivity.class);
                                            startActivity(in);
                                            getActivity().finish();


                                        }
                                    }, 1000);
                                }
                            });

                            pDialog_logout.show();

                        }else {

                            Snackbar.make(getView(),"Please check UserName or Password", Snackbar.LENGTH_SHORT).show();

                        }

                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public void onPostError(int flag) {

    }
}
//c2Vp4y30Okc:APA91bGKdAcFxrD_n66Az3JAvk8L6q6u7mC1yrdVSJMiOq82qLZP8TkvP8O0grekHSIIUyZIao2TY5hCcTlK5k9tc6nVeJTySXD5m9t-5e7h1AGldnarN9pPO_T3XuEJs_1wEx5IUWba