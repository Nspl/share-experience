package com.shareexperence.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.desarrollodroide.libraryfragmenttransactionextended.SlidingRelativeLayout;
import com.shareexperence.R;
import com.shareexperence.Splash;
import com.shareexperence.Utils.Font;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Globals;
import com.shareexperence.Utils.Methods;
import com.shareexperence.activites.ActivityDdexpActivity;
import com.shareexperence.activites.Home;
import com.shareexperence.activites.Messages;
import com.shareexperence.activites.Profile;
import com.shareexperence.adapter.ViewPagerAdapter;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class ActivityHomeFragment extends Fragment implements MainAsynListener<String> {
    @Bind(R.id.main)
    SlidingRelativeLayout main;
    @Bind(R.id.experience_txt) ImageView experience_txt;

    @Bind(R.id.log) ImageView log;

    @Bind(R.id.main1)
    LinearLayout main1;
    @Bind(R.id.home)
    ImageView home;
    @Bind(R.id.msg)
    ImageView msg;

    @Bind(R.id.logout)
    ImageView logout;

    private SweetAlertDialog pDialog_logout;

    @Bind(R.id.play) RelativeLayout play;
    /*
       @Bind(R.id.subheading_txt) TextView subheadingTxt;
   */
    @Bind(R.id.profile) ImageView profile;


    String email,Name,location,gender ,Id,username,telephone,hash_tag_search,about,notification_status;

    ViewPagerAdapter adapter;
  //  OnSwipeTouchListener swipe ;
    GestureDetector gestureDetector;


    public static ActivityHomeFragment newInstance() {
        return new ActivityHomeFragment();
    }

    public ActivityHomeFragment() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_home, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);


        Font.setDefaultFont(getActivity(), "DEFAULT", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "MONOSPACE", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "SANS_SERIF", "font_maaxbold.otf");
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        GeneralValues.set_trial(getActivity(),"1");


        if (Methods.checkPermissionForGallary(getActivity())){}else{
            Methods.requestPermissionForGallry(getActivity());
        }

        GeneralValues.set_logintype(getActivity(),"1");

    }


    public int getWidth() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int w = display.getWidth();
        return w;
    }

    @OnClick(R.id.play) void play(){
        Intent qn= new Intent (getActivity(), Home.class);
        getActivity().startActivity(qn);


    }
    @Override
    public void onResume() {
        super.onResume();
        Globals.getterList= new ArrayList<>();
        Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));


        new MainAsyncTask(getActivity(), Globals.Webservice.Getprofile,1,ActivityHomeFragment.this,"Form", Globals.getterList,false).execute();
    }

    @OnClick(R.id.profile) void profile(){


        Intent qn= new Intent (getActivity(), Profile.class);
        getActivity().startActivity(qn);

    }




    @OnClick(R.id.logout)void setlogout(){

        pDialog_logout = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE);
        pDialog_logout.setCancelable(true);
        pDialog_logout.setTitleText("Logout?");
        pDialog_logout.setContentText("Do you want to logout?");
        pDialog_logout.setConfirmText("Yes");
        pDialog_logout.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {

                Logout();

            }
        });
        pDialog_logout.setCancelText("No");
        pDialog_logout.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {

            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog_logout.dismissWithAnimation();

            }
        });

        pDialog_logout.show();


    }


    public  void Logout(){
        Globals.getterList= new ArrayList<>();
        Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));

        new MainAsyncTask(getActivity(), Globals.Webservice.Logout,3,ActivityHomeFragment.this,"Form", Globals.getterList).execute();

    }




    @OnClick(R.id.experience_txt)void setAddexp(){

        Intent qn= new Intent (getActivity(), ActivityDdexpActivity.class);
        getActivity().startActivity(qn);



    }
    @OnClick(R.id.msg)void setmsg(){


        GeneralValues.set_msgtype(getActivity(),"0");

        Intent qn= new Intent (getActivity(), Messages.class);
        startActivity(qn);


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {
        if (isSucess) {
            if (result != null) {
                try {
                    if (flag==1) {

                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        if (status.equalsIgnoreCase("true")) {

                            JSONArray ja = Globals.jsonObj.getJSONArray("data");
                            for (int arr = 0; arr < ja.length(); arr++) {
                                JSONObject json1 = ja.getJSONObject(arr);

                                email = json1.getString("email");
                                Name= json1.getString("full_name");
                                location= json1.getString("location");
                                gender = json1.getString("gender");
                                Id= json1.getString("user_id");

                                telephone= json1.getString("telephone");
                                username= json1.getString("username");
                                hash_tag_search= json1.getString("hash_tag_search");
                                about= json1.getString("about");
                                notification_status= json1.getString("notification_status");

                            }


                            GeneralValues.set_email(getActivity(),email);
                            GeneralValues.set_phn(getActivity(),telephone);
                            GeneralValues.set_user_name(getActivity(),Name);





                        }else{

                        }
                    }else if (flag==3){
                        Globals.jsonObj = new JSONObject(result);
                        String status1 = Globals.jsonObj.getString("status");
                        if (status1.equalsIgnoreCase("true")) {

                            pDialog_logout.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    pDialog_logout.dismissWithAnimation();
                                    GeneralValues.set_loginbool((getActivity()), false);

                                    Intent i = new Intent(getActivity(), Splash.class);
                                    getActivity().startActivity(i);
                                    getActivity().finish();

                                }

                            }, 1000);

                        }

                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public void onPostError(int flag) {

    }





}
