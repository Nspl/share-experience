package com.shareexperence.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.desarrollodroide.libraryfragmenttransactionextended.FragmentTransactionExtended;
import com.rey.material.app.BottomSheetDialog;
import com.shareexperence.R;
import com.shareexperence.Utils.Font;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Gettersetter;
import com.shareexperence.Utils.Globals;
import com.shareexperence.Utils.Methods;
import com.shareexperence.activites.MessageCamera;
import com.shareexperence.adapter.SimpleGestureFilter;
import com.shareexperence.adapter.ViewPagerAdapter;
import com.shareexperence.redcontact.RedcontactFragment;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ContactFragment extends Fragment implements MainAsynListener<String>   {
    private SimpleGestureFilter detector;

    CountDownTimer Timer ;


    @Bind(R.id.view1)
    RelativeLayout view1;

    private static int counter = -1;
    private String stringVal;
    @Bind(R.id.text)
    TextView text;

    Bundle bundle;

 /*   @Bind(R.id.contact)
    ImageView contact;*/
    BottomSheetDialog mBottomSheetDialog;

    String user_id, title, thumbnail, id, video,Username;
    Gettersetter getset;
    ArrayList<Gettersetter> arrfeed;
        @Bind(R.id.pager)
    ViewPager pager;
    PageListener pageListener;
    ViewPagerAdapter adapter;


    boolean isRunning = true;

    OnSwipeTouchListener swipe ;

    static Positionvideo onclick;

    public static ContactFragment newInstance() {
        return new ContactFragment();
    }

    public ContactFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.contact, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        arrfeed= new ArrayList<>();

        GeneralValues.set_profile(getActivity(),"0");
        GeneralValues.set_logintype(getActivity(),"1");
        GeneralValues.set_msgtype(getActivity(),"0");
        GeneralValues.set_trial(getActivity(), "0");

        getActivity().setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        Font.setDefaultFont(getActivity(), "DEFAULT", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "MONOSPACE", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "SANS_SERIF", "font_maaxbold.otf");

    }


    @Override
    public void onResume() {
        super.onResume();

        Globals.getterList= new ArrayList<>();
        Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));
        Globals.getterList.add(new ParamsGetter("page_offset", "0"));

        new MainAsyncTask(getActivity(),Globals.Webservice.GetAllExpreience,1,ContactFragment.this,"Form",Globals.getterList).execute();



    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);

        Log.e("Distroy call or not","yess");
        try {



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e("onDetach call  onDetach","onDetach");

    }

    public void Forgot_dialog() {
        mBottomSheetDialog = new BottomSheetDialog(getActivity(), R.style.Material_App_BottomSheetDialog);
        View dialog = LayoutInflater.from(getActivity()).inflate(R.layout.cotact_dialog, null);


        ImageView blue = (ImageView) dialog.findViewById(R.id.blue);
        LinearLayout contactus = (LinearLayout) dialog.findViewById(R.id.contactus);
        TextView TouchRed = (TextView) dialog.findViewById(R.id.TouchRed);

        TextView Touchblue = (TextView) dialog.findViewById(R.id.Touchblue);

        TouchRed.setText("WITH " + arrfeed.get(counter).getStrName() );
        Touchblue.setText("WITH " + Globals.Videopostion.get(counter).getStrName() );


        LinearLayout cancel = (LinearLayout) dialog.findViewById(R.id.cancel);

        // if decline button is clicked, close the custom dialog
        blue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBottomSheetDialog.dismiss();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (Methods.checkPermissionForCamera(getActivity())){

                            if (Methods.checkPermissionForGallary(getActivity())) {
                                if (Methods.checkPermissionForAudio(getActivity())){
                                    if (Methods.checkPermissionForWritestorage(getActivity())){

                                        Log.e("Userid<><",Globals.Videopostion.get(counter).getId());

                                        Intent in = new Intent(getActivity(), MessageCamera.class);
                                        in.putExtra("userid",Globals.Videopostion.get(counter).getId());
                                        in.putExtra("tittle",Globals.Videopostion.get(counter).getStrtittle());
                                        in.putExtra("exprncid",Globals.Videopostion.get(counter).getExperinceId());
                                        getActivity().startActivity(in);
                                      //  getActivity().finish();

                                    }
                                    else{
                                        Methods.requestPermissionForWriteStorage(getActivity());
                                    }

                                } else{
                                    Methods.requestPermissionForAudio(getActivity());
                                }

                            }else{
                                Methods.requestPermissionForGallry(getActivity());
                            }

                        }else {
                            Methods.requestPermissionForCamera(getActivity());
                        }

                    }
                }, 400);


            }
        });

        // if decline button is clicked, close the custom dialog

        contactus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBottomSheetDialog.dismiss();


                bundle = new Bundle();
                bundle.putString("Name",Globals.Videopostion.get(counter).getStrName());
                bundle.putString("otherid",Globals.Videopostion.get(counter).getId());

                RedcontactFragment fragments = new RedcontactFragment();
                fragments.setArguments(bundle);

                Globals.FRAGMENT_MANAGER = getActivity().getFragmentManager();
                Globals.fragmentTransaction = Globals.FRAGMENT_MANAGER.beginTransaction();
                FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(getActivity(), Globals.fragmentTransaction, MyprofileFragment.newInstance(), fragments , Globals.frag_id);
                fragmentTransactionExtended.addTransition(FragmentTransactionExtended.FLIP_HORIZONTAL);
                fragmentTransactionExtended.commit();

             }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBottomSheetDialog.dismiss();

            }
        });


        mBottomSheetDialog.contentView(dialog)
                .show();

     }


    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {
         if (isSucess) {
            if (result != null) {
                try {
                    if (flag == 1) {

                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        if (status.equalsIgnoreCase("true")) {
                            text.setVisibility(View.GONE);
                            Globals.Videopostion.clear();
                            JSONArray ja = Globals.jsonObj.getJSONArray("data");
                            for (int arr = 0; arr < ja.length(); arr++) {
                                JSONObject json1 = ja.getJSONObject(arr);

                                user_id = json1.getString("user_id");
                                title = json1.getString("title");
                                thumbnail = json1.getString("thumbnail");
                                id = json1.getString("id");
                                video = json1.getString("video");
                                Username = json1.getString("full_name");


                                getset = new Gettersetter();
                                getset.setStrName(Username);

                                getset.setStrimage(thumbnail);
                                getset.setId(user_id);
                                getset.setStrtittle(title);
                                getset.setExperinceId(id);
                                getset.setFilevideo(video);

                                arrfeed.add(getset);


                                Globals.Videopostion.add(getset)  ;
                            }



                            adapter = new ViewPagerAdapter(getActivity(), arrfeed);
                            pager.setAdapter(adapter);
                            pager.setCurrentItem(0);
                            pageListener = new PageListener();
                            pager.setOnPageChangeListener(pageListener);
                             pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                                @Override
                                public void onPageSelected (int page)
                                {
                                  //  counter   =   pager.getCurrentItem() ;
                                    counter = page;
                                     //page changed

                                 //   Log.e("getCurrentItem=<>>>>", "  getCurrentItem " + pager.getCurrentItem());
                                    GeneralValues.set_trial(getActivity(), "1");
                                    onclick.getvideo(page);

                                }

                                @Override
                                public void onPageScrolled (int arg0, float arg1, int arg2)
                                {


                                }

                                @Override
                                public void onPageScrollStateChanged (int arg0)
                                {


                                }
                            });




                        } else {
                            text.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        }


    }

    @Override
    public void onPostError(int flag) {

    }


    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public void onPageSelected(int position) {
            Log.i("SeLECTED=====", ">>>page selected " + position);
         //   Imageposition = position;//currentPage = position;}}


            if (isRunning) {

                isRunning = false;
            } else {

                isRunning = true;
            }



        }
    }

    public class OnSwipeTouchListener implements View.OnTouchListener {

        private final GestureDetector gestureDetector;

        public OnSwipeTouchListener (Context ctx){
            gestureDetector = new GestureDetector(ctx, new GestureListener());
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return gestureDetector.onTouchEvent(event);
        }

        private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

            private static final int SWIPE_THRESHOLD = 100;
            private static final int SWIPE_VELOCITY_THRESHOLD = 100;

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                boolean result = false;
                try {
                    float diffY = e2.getY() - e1.getY();
                    float diffX = e2.getX() - e1.getX();
                    if (Math.abs(diffX) > Math.abs(diffY)) {
                        if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                            if (diffX > 0) {
                                onSwipeRight();
                            } else {
                                onSwipeLeft();
                            }
                        }
                        result = true;
                    }
                    else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffY > 0) {
                            onSwipeBottom();
                        } else {
                            onSwipeTop();
                        }
                    }
                    result = true;

                } catch (Exception exception) {
                    exception.printStackTrace();
                }
                return result;
            }
        }

        public void onSwipeRight() {
            //    Toast.makeText(getActivity(), "right", Toast.LENGTH_SHORT).show();

        }

        public void onSwipeLeft() {
            //     Toast.makeText(getActivity(), "left", Toast.LENGTH_SHORT).show();
        }

        public void onSwipeTop() {
            //      Toast.makeText(getActivity(), "top", Toast.LENGTH_SHORT).show();
        }

        public void onSwipeBottom() {
            //       Toast.makeText(getActivity(), "botom", Toast.LENGTH_SHORT).show();
        }
    }


    public void Timer(){
        Timer=  new CountDownTimer(6000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {
                String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
            }

            public void onFinish() {

                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }.start();
    }






    public static void setmListener(Positionvideo data) {

        onclick = data;
    }

    public interface Positionvideo {

        void getvideo(int Id);

    }


}
