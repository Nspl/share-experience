package com.shareexperence.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.shareexperence.R;
import com.shareexperence.Utils.Font;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Gettersetter;
import com.shareexperence.Utils.Globals;
import com.shareexperence.activites.ActivityGetagreementActivity;
import com.shareexperence.activites.SettingActivity;
import com.shareexperence.adapter.Adap_notification;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotificationListFragment extends Fragment implements MainAsynListener<String> {


    @Bind(R.id.back) LinearLayout back;
    @Bind(R.id.list)
    SwipeMenuListView list;
    @Bind(R.id.comment)
    TextView comment;

     Adap_notification adap ;
    Gettersetter getset ;
    ArrayList<Gettersetter> arrfeed ,listClone;
String Name,Eventname,Userid,Eventid,Label,Profile,Time,otherid,id,message ,owner_status;

    String notistatus  = "0";
    Bundle bundle ;


    public static NotificationListFragment newInstance() {

        return new NotificationListFragment();

    }

    public NotificationListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.notification_list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        arrfeed = new ArrayList<>();
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Font.setDefaultFont(getActivity(), "DEFAULT", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "MONOSPACE", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "SANS_SERIF", "font_maaxbold.otf");


        bundle = getActivity().getIntent().getExtras();
        if (bundle != null) {
            if (bundle.getString("key").equals("noti")) {

                notistatus = "1" ;
            }  else {

                notistatus = "0" ;
            }

        }


    }

  @OnClick(R.id.back) void setBack(){

      if (notistatus.equalsIgnoreCase("1")){
          startActivity(new Intent(getActivity(),SettingActivity.class));
          getActivity().finish();
      }else {

          getActivity().finish();
      }




  }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        ButterKnife.unbind(this);

    }

    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {
        if (isSucess) {
            if (result != null) {

                try {

                    if (flag==1) {
                        arrfeed.clear();
                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        arrfeed.clear();
                        if (status.equalsIgnoreCase("true")) {

                            comment.setVisibility(View.GONE);
                            JSONArray ja = Globals.jsonObj.getJSONArray("data");
                            for (int arr = 0; arr < ja.length(); arr++) {
                                JSONObject json1 = ja.getJSONObject(arr);

                                Name    = json1.getString("full_name");
                                Time   = json1.getString("created_date");
                                Eventname  = json1.getString("title");
                                 otherid  = json1.getString("sender_id");
                                 Eventid   = json1.getString("agreement_id");
                                Label   = json1.getString("label");
                                Profile    = json1.getString("image");

                                id = json1.getString("notification_id");
                                owner_status = json1.getString("owner_status");


                                message   = json1.getString("message");

                                getset = new Gettersetter();
                                getset.setStrName(Name);
                                getset.setDate(Time);
                                getset.setStrimage(Profile);
                                getset.setTag(message);
                                getset.setStrtittle(Eventname);
                                getset.setOtherid(otherid);
                                getset.setEventid(Eventid);
                                getset.setLabel(Label);
                                getset.setExperinceId(id);
                               getset.setOwnerstatus(owner_status);

                                arrfeed.add(getset);

                            }

                            Log.e("SIZE oF ARRAY", "---------------"+arrfeed.size());

                            adap = new Adap_notification(getActivity(),arrfeed);
                            list.setAdapter(adap);
                            adap.notifyDataSetChanged();

                            SwipeList() ;

                            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                                 /*   bundle = new Bundle();

                                    bundle.putString("agreementid",arrfeed.get(position).getEventid());
*/
                                    if (arrfeed.get(position).getLabel().equalsIgnoreCase("submitagreement")){
                                        Intent i = new Intent(getActivity(), ActivityGetagreementActivity
                                                .class);
                                        i.putExtra("agreementid",arrfeed.get(position).getEventid());
                                        i.putExtra("expid",arrfeed.get(position).getExperinceId());
                                        i.putExtra("owner_status",arrfeed.get(position).getOwnerstatus());
                                        startActivity(i);

                                    }else if(arrfeed.get(position).getLabel().equalsIgnoreCase("updateagreement")){
                                        Intent i = new Intent(getActivity(), ActivityGetagreementActivity
                                                .class);
                                        i.putExtra("agreementid",arrfeed.get(position).getEventid());
                                        i.putExtra("expid",arrfeed.get(position).getExperinceId());
                                        i.putExtra("owner_status",arrfeed.get(position).getOwnerstatus());
                                        startActivity(i);

                                    }else {

                                    }



                                }
                            });
                        }else {

                            comment.setVisibility(View.VISIBLE);

                            adap = new Adap_notification(getActivity(),arrfeed);
                            list.setAdapter(adap);
                            adap.notifyDataSetChanged();

                        }

                    }else  if (flag == 2){
                          Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        arrfeed.clear();
                        if (status.equalsIgnoreCase("true")) {
 Getreview();

                        }else {

                        }


                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public void onPostError(int flag) {

    }


    @Override
    public void onResume() {
        super.onResume();

        Getreview();
    }

    public  void Getreview(){

        Globals.getterList= new ArrayList<>();
        Globals.getterList.add(new ParamsGetter("user_id",  GeneralValues.get_user_id(getActivity())));

     new MainAsyncTask(getActivity(), Globals.Webservice.GetNoti,1,NotificationListFragment.this,"Form", Globals.getterList).execute();

    }

private  void SwipeList(){
    SwipeMenuCreator creator = new SwipeMenuCreator() {

        @Override
        public void create(SwipeMenu menu) {
            // create "open" item
/*            SwipeMenuItem openItem = new SwipeMenuItem(
                   getActivity());
            // set item background
            openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,
                    0xCE)));
            // set item width
            openItem.setWidth(dp2px(90));
            // set item title
            openItem.setTitle("Open");
            // set item title fontsize
            openItem.setTitleSize(18);
            // set item title font color
            openItem.setTitleColor(Color.WHITE);
            // add to menu
            menu.addMenuItem(openItem);*/

            // create "delete" item
            SwipeMenuItem deleteItem = new SwipeMenuItem(
                    getActivity());
            // set item background
            deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                    0x3F, 0x25)));
            // set item width
            deleteItem.setWidth(dp2px(90));
            // set a icon
            deleteItem.setIcon(R.drawable.ic_delete);
            // add to menu
            menu.addMenuItem(deleteItem);
        }
    };

// set creator
    list.setMenuCreator(creator);

    list.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
             switch (index) {
            /*    case 0:
                    // open
                    if (arrfeed.get(position).getLabel().equalsIgnoreCase("submitagreement")){
                        Intent i = new Intent(getActivity(), ActivityGetagreementActivity
                                .class);
                        i.putExtra("agreementid",arrfeed.get(position).getEventid());
                        i.putExtra("expid",arrfeed.get(position).getExperinceId());
                        i.putExtra("owner_status",arrfeed.get(position).getOwnerstatus());
                        startActivity(i);

                    }else if(arrfeed.get(position).getLabel().equalsIgnoreCase("updateagreement")){
                        Intent i = new Intent(getActivity(), ActivityGetagreementActivity
                                .class);
                        i.putExtra("agreementid",arrfeed.get(position).getEventid());
                        i.putExtra("expid",arrfeed.get(position).getExperinceId());
                        i.putExtra("owner_status",arrfeed.get(position).getOwnerstatus());
                        startActivity(i);

                    }else {

                    }

                    break;*/
                case 0:
                    // delete
//					delete(item);
                    Globals.getterList= new ArrayList<>();
                    Globals.getterList.add(new ParamsGetter("user_id",  GeneralValues.get_user_id(getActivity())));
                    Globals.getterList.add(new ParamsGetter("notification_id",  arrfeed.get(position).getExperinceId()));

                    new MainAsyncTask(getActivity(), Globals.Webservice.DeleteNoti,2,NotificationListFragment.this,"Form", Globals.getterList).execute();
                    break;
            }
            return false;
        }
    });

    // set SwipeListener
    list.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

        @Override
        public void onSwipeStart(int position) {
            // swipe start
        }

        @Override
        public void onSwipeEnd(int position) {
            // swipe end
        }
    });

    // set MenuStateChangeListener
    list.setOnMenuStateChangeListener(new SwipeMenuListView.OnMenuStateChangeListener() {
        @Override
        public void onMenuOpen(int position) {
        }

        @Override
        public void onMenuClose(int position) {
        }
    });

    // other setting
//		listView.setCloseInterpolator(new BounceInterpolator());

    // test item long click
    list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view,
                                       int position, long id) {
            Toast.makeText(getActivity(), position + " long click", Toast.LENGTH_SHORT).show();
            return false;
        }
    });

}



    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

}
