package com.shareexperence.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.desarrollodroide.libraryfragmenttransactionextended.FragmentTransactionExtended;
import com.desarrollodroide.libraryfragmenttransactionextended.SlidingRelativeLayout;
import com.rey.material.app.BottomSheetDialog;
import com.shareexperence.R;
import com.shareexperence.Utils.CommonUtils;
import com.shareexperence.Utils.Font;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Gettersetter;
import com.shareexperence.Utils.Globals;
import com.shareexperence.activites.ActivityDdexpActivity;
import com.shareexperence.activites.MainActivity;
import com.shareexperence.activites.MessageCamera;
import com.shareexperence.activites.Messages;
import com.shareexperence.activites.Profile;
import com.shareexperence.adapter.ChatAdapter;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SinglechatFragment extends Fragment implements MainAsynListener<String> {

    @Bind(R.id.main)
    SlidingRelativeLayout main;
    @Bind(R.id.experience_txt) ImageView experienceTxt;
    @Bind(R.id.profile) ImageView profile;
    @Bind(R.id.home) ImageView home;
    @Bind(R.id.msg) ImageView msg;
    @Bind(R.id.contact) RelativeLayout contact;
     @Bind(R.id.agreement) ImageView agreement;
    @Bind(R.id.name) TextView name;

    @Bind(R.id.profile_image) ImageView profile_image;
    @Bind(R.id.other)
    LinearLayout other;


    BottomSheetDialog mBottomSheetDialog;
    @Bind(R.id.list)
    ListView list;
    ArrayList<Gettersetter> arrchat;
    Gettersetter getset;
    String ID,Name,Image ,Time,Msgthumb,Msgvideo;
    ChatAdapter adap ;
Bundle bundle ;
    String otherid ,Tittle,expid,MSgid;

    public static SinglechatFragment newInstance() {
        return new SinglechatFragment();
    }

    public SinglechatFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.singlechat, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        arrchat = new ArrayList<>() ;
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Font.setDefaultFont(getActivity(), "DEFAULT", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "MONOSPACE", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "SANS_SERIF", "font_maaxbold.otf");

        bundle = getActivity().getIntent().getExtras();
        if (bundle != null) {


            try {

                otherid = bundle.getString("otherid");
                Name=bundle.getString("name");
                Image=bundle.getString("image");
                Tittle=bundle.getString("tittle");
                expid=bundle.getString("expid");


                Globals.ID = otherid ;
                Globals.Tittle = Tittle ;
                Globals.ExpID = expid ;
                //

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        name.setText(Name);

        try {
            CommonUtils.getDisplayImage(getActivity(),Image,profile_image);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @OnClick(R.id.other) void setother(){

        bundle = new Bundle();


        bundle.putString("key", "msg");
        bundle.putString("userid", otherid);


        ProfileFragment fragments = new ProfileFragment();
        fragments.setArguments(bundle);

        Globals.FRAGMENT_MANAGER = getActivity().getFragmentManager();
        Globals.fragmentTransaction = Globals.FRAGMENT_MANAGER.beginTransaction();
        FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended( getActivity(), Globals.fragmentTransaction, ContactFragment.newInstance(), fragments, Globals.frag_id);
        fragmentTransactionExtended.addTransition(FragmentTransactionExtended.SLIDE_HORIZONTAL);
        fragmentTransactionExtended.commit();


    }

    @OnClick(R.id.agreement) void setagree(){

        Agreement();

    }
    @OnClick(R.id.contact) void setcontact(){

        Intent in = new Intent(getActivity(),MessageCamera.class);
         in.putExtra("userid",otherid);
        in.putExtra("tittle",Tittle);
        in.putExtra("exprncid",expid);
        startActivity(in);


    }




    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

        public void  Agreement(){
        mBottomSheetDialog = new BottomSheetDialog(getActivity(), R.style.Material_App_BottomSheetDialog);
        View dialog = LayoutInflater.from(getActivity()).inflate(R.layout.agreement, null);



        TextView no = (TextView) dialog.findViewById(R.id.no);
        TextView yes = (TextView) dialog.findViewById(R.id.yes);
        TextView notyet = (TextView) dialog.findViewById(R.id.notyet);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBottomSheetDialog.dismiss();


                bundle = new Bundle();

                bundle.putString("otherid", Globals.ID);
                bundle.putString("name",Name);
                bundle.putString("image",Image);

                 bundle.putString("tittle", Globals.Tittle);
                bundle.putString("expid", Globals.ExpID);

                MeetingFragment fragments = new MeetingFragment();
                fragments.setArguments(bundle);

                Globals.FRAGMENT_MANAGER = getActivity().getFragmentManager();
                Globals.fragmentTransaction = Globals.FRAGMENT_MANAGER.beginTransaction();
                FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(getActivity(), Globals.fragmentTransaction, MyprofileFragment.newInstance(), fragments , Globals.frag_id);
                fragmentTransactionExtended.addTransition(FragmentTransactionExtended.SLIDE_HORIZONTAL);
                fragmentTransactionExtended.commit();

            }
        });

            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    mBottomSheetDialog.dismiss();


                    Delete() ;
                }
            });
            notyet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    mBottomSheetDialog.dismiss();

                 }
            });

         mBottomSheetDialog.contentView(dialog)
                .show();

    }

    @OnClick(R.id.home) void sethome(){

        Intent qn= new Intent (getActivity(), MainActivity.class);
        getActivity().startActivity(qn);

    }
    @OnClick(R.id.experience_txt)void setAddexp(){

        Intent qn= new Intent (getActivity(), ActivityDdexpActivity.class);
        getActivity().startActivity(qn);

    }

    @OnClick(R.id.profile) void profile(){


        Intent qn= new Intent (getActivity(), Profile.class);
        getActivity().startActivity(qn);


    }

    @OnClick(R.id.msg)void setmsg(){
        GeneralValues.set_msgtype(getActivity(),"0");

        Intent qn= new Intent (getActivity(), Messages.class);
        startActivity(qn);


    }

   public void Delete(){
       mBottomSheetDialog = new BottomSheetDialog(getActivity(), R.style.Material_App_BottomSheetDialog);
       View dialog = LayoutInflater.from(getActivity()).inflate(R.layout.delete_dialog, null);

       TextView ok = (TextView) dialog.findViewById(R.id.ok);
       TextView notyet = (TextView) dialog.findViewById(R.id.notyet);

       ok.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               mBottomSheetDialog.dismiss();

               Globals.getterList= new ArrayList<>();
               Globals.getterList.add(new ParamsGetter("delete_by", GeneralValues.get_user_id(getActivity())));
               Globals.getterList.add(new ParamsGetter("delete_to", Globals.ID));
             //  Globals.getterList.add(new ParamsGetter("message_id", MSgid));


               new MainAsyncTask(getActivity(), Globals.Webservice.DeleteSingleMessage,2,SinglechatFragment.this,"Form", Globals.getterList).execute();


           }
       });

       notyet.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               mBottomSheetDialog.dismiss();



           }
       });


       mBottomSheetDialog.contentView(dialog)
               .show();

   }

    @Override
    public void onResume() {
        super.onResume();

        Globals.getterList= new ArrayList<>();
        Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));
        Globals.getterList.add(new ParamsGetter("other_user_id", Globals.ID));
        Globals.getterList.add(new ParamsGetter("experience_id", Globals.ExpID));


        new MainAsyncTask(getActivity(), Globals.Webservice.Singlechat,1,SinglechatFragment.this,"Form", Globals.getterList).execute();

    }

    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {
        if (isSucess) {
            if (result != null) {
                 try {
                     if (flag==1) {

                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        if (status.equalsIgnoreCase("true")) {
                            arrchat.clear();
                            JSONArray ja = Globals.jsonObj.getJSONArray("data");
                            for (int arr = 0; arr < ja.length(); arr++) {
                                JSONObject json1 = ja.getJSONObject(arr);

                                 ID = json1.getString("receiver_id");
                                 Time = json1.getString("msg_time");
                                 Msgthumb= json1.getString("message_thumbnail");
                                 Msgvideo= json1.getString("message_video");
                               MSgid = json1.getString("id");




                                 getset = new Gettersetter();

                                 getset.setStrimage(Msgthumb);
                                 getset.setFilevideo(Msgvideo);
                                 getset.setId(ID);
                                 getset.setDate(Time);
                                 arrchat.add(getset);

                            }

                            adap = new ChatAdapter(getActivity(), arrchat);
                            list.setAdapter(adap);
                            scrollMyListViewToBottom();
                            adap.notifyDataSetChanged();


                        }else{
                            arrchat.clear();
                        }

                    }else if (flag==2){
                         Globals.jsonObj = new JSONObject(result);
                         String status = Globals.jsonObj.getString("status");
                         if (status.equalsIgnoreCase("true")) {

                             Intent qn= new Intent (getActivity(), Messages.class);
                             startActivity(qn);
                             getActivity().finish();

                         }else{
                             Toast.makeText(getActivity(),"Please check internet Connection",Toast.LENGTH_SHORT).show();
                         }



                     }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        }

    }

    @Override
    public void onPostError(int flag) {

    }

    private void scrollMyListViewToBottom() {
        list.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                list.setSelection(adap.getCount() - 1);
            }
        });
    }

}
