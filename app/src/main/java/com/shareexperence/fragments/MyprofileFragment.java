package com.shareexperence.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.VideoView;

import com.desarrollodroide.libraryfragmenttransactionextended.SlidingRelativeLayout;
import com.shareexperence.R;
import com.shareexperence.Utils.CommonUtils;
import com.shareexperence.Utils.Font;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Globals;
import com.shareexperence.activites.AgreementfoundActivity;
import com.shareexperence.activites.MyexperienceActivity;
import com.shareexperence.activites.Notification_Act;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyprofileFragment extends Fragment implements MainAsynListener<String> {

    @Bind(R.id.main)
    SlidingRelativeLayout main;
    @Bind(R.id.fm) FrameLayout fm;
    @Bind(R.id.videoView) VideoView videoView;
/*    @Bind(R.id.experience_txt) ImageView experienceTxt;*/
    /*@Bind(R.id.editprofile) ImageView editprofile;*/

    @Bind(R.id.r2) RelativeLayout r2;
    @Bind(R.id.n1) TextView n1;
    @Bind(R.id.name)
    TextView name;
     @Bind(R.id.age) TextView age;
    @Bind(R.id.n9) TextView n9;
    @Bind(R.id.Sex) TextView Sex;
    @Bind(R.id.n3) TextView n3;
    @Bind(R.id.from) TextView from;
    @Bind(R.id.n99) TextView n99;
    @Bind(R.id.phone) TextView phone;
    @Bind(R.id.n919) TextView n919;
     @Bind(R.id.privacy) TextView privacy;
    @Bind(R.id.appinfo) TextView appinfo;
    @Bind(R.id.Notificationhistory) TextView Notificationhistory;
    @Bind(R.id.agreefound) TextView agreefound;
    @Bind(R.id.myexprnc) TextView myexprnc;


/*    @Bind(R.id.home) TextView home;*/
    @Bind(R.id.switch1)
    Switch switch1;


/*    @Bind(R.id.msg)
    ImageView msg;*/
   String email,Name,location,gender ,Id,username,telephone,hash_tag_search,about,notification_status,Age;
    Bundle bundle;

    public static MyprofileFragment newInstance() {
        return new MyprofileFragment();
    }

    public MyprofileFragment() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.myprofile, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Font.setDefaultFont(getActivity(), "DEFAULT", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "MONOSPACE", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "SANS_SERIF", "font_maaxbold.otf");

    }

    @OnClick(R.id.Notificationhistory) void setNotificationhistory(){

        Intent in = new Intent(getActivity(), Notification_Act.class);
        getActivity().startActivity(in);

    }

    @OnClick(R.id.myexprnc) void myexprnc(){

        Intent in = new Intent(getActivity(), MyexperienceActivity.class);
        getActivity().startActivity(in);

    }


    @OnClick(R.id.agreefound) void setagreefound(){

        Intent in = new Intent(getActivity(), AgreementfoundActivity.class);
        getActivity().startActivity(in);

    }


    @OnClick(R.id.switch1) void setSwitch1(){

        if (notification_status.equalsIgnoreCase("Active")){

            Globals.getterList = new ArrayList<>();
            Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));
            Globals.getterList.add(new ParamsGetter("full_name", ""));
            Globals.getterList.add(new ParamsGetter("telephone",  ""));
            Globals.getterList.add(new ParamsGetter("username", ""));
            Globals.getterList.add(new ParamsGetter("gender", ""));
            Globals.getterList.add(new ParamsGetter("location", ""));
            Globals.getterList.add(new ParamsGetter("age", ""));
            Globals.getterList.add(new ParamsGetter("about", ""));
            Globals.getterList.add(new ParamsGetter("hash_tag_search", ""));
            Globals.getterList.add(new ParamsGetter("notification_status", "Inactive"));
            Globals.getterList.add(new ParamsGetter("image", ""));


            new MainAsyncTask(getActivity(), Globals.Webservice.Editprofile, 2, MyprofileFragment.this, "Multipart", Globals.getterList).execute();

        }else if (notification_status.equalsIgnoreCase("Inactive")){

            Globals.getterList = new ArrayList<>();
            Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));
            Globals.getterList.add(new ParamsGetter("full_name", ""));
            Globals.getterList.add(new ParamsGetter("telephone",  ""));
            Globals.getterList.add(new ParamsGetter("username", ""));
            Globals.getterList.add(new ParamsGetter("gender", ""));
            Globals.getterList.add(new ParamsGetter("location", ""));
            Globals.getterList.add(new ParamsGetter("age", ""));
            Globals.getterList.add(new ParamsGetter("about", ""));
            Globals.getterList.add(new ParamsGetter("hash_tag_search", ""));
            Globals.getterList.add(new ParamsGetter("notification_status", "Active"));
            Globals.getterList.add(new ParamsGetter("image", ""));

            new MainAsyncTask(getActivity(), Globals.Webservice.Editprofile, 2, MyprofileFragment.this, "Multipart", Globals.getterList,false).execute();

        }


    }


    @Override
    public void onResume() {
        super.onResume();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                try {
                    Globals.getterList= new ArrayList<>();
                    Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));

                    new MainAsyncTask(getActivity(), Globals.Webservice.Getprofile,1,MyprofileFragment.this,"Form", Globals.getterList).execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, 1000);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {
        if (isSucess) {
            if (result != null) {
                try {
                    if (flag==1) {

                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        if (status.equalsIgnoreCase("true")) {
                            r2.setVisibility(View.VISIBLE);
                            JSONArray ja = Globals.jsonObj.getJSONArray("data");
                            for (int arr = 0; arr < ja.length(); arr++) {
                                JSONObject json1 = ja.getJSONObject(arr);

                             email = json1.getString("email");
                                   Name= json1.getString("full_name");
                                    location= json1.getString("location");
                               gender = json1.getString("gender");
                                 Id= json1.getString("user_id");
                                Age = json1.getString("age");
                                telephone= json1.getString("telephone");
                                username= json1.getString("username");
                                hash_tag_search= json1.getString("hash_tag_search");
                                about= json1.getString("about");
                                notification_status= json1.getString("notification_status");

                            }


                            if (notification_status.equalsIgnoreCase("Active")){
                                switch1.setChecked(true);
                            }else{
                                switch1.setChecked(false);
                            }

                            name.setText(Name);
                            if (gender.equalsIgnoreCase("M")){
                                Sex.setText("Male");
                            }else if (gender.equalsIgnoreCase("F")){
                                Sex.setText("Female");
                            }
                            from.setText(email);

                         String ChangeAge =   CommonUtils.getAge(Age);

                            age.setText(ChangeAge);
                            phone.setText(telephone);


                        }else{
                            r2.setVisibility(View.VISIBLE);
                        }
                    }else if (flag==2){

                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        if (status.equalsIgnoreCase("true")) {

                            Globals.getterList= new ArrayList<>();
                            Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));


                            new MainAsyncTask(getActivity(), Globals.Webservice.Getprofile,1,MyprofileFragment.this,"Form", Globals.getterList).execute();

                        }
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public void onPostError(int flag) {

    }


}
