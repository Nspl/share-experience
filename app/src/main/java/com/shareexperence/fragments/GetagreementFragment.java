package com.shareexperence.fragments;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.shareexperence.R;
import com.shareexperence.Utils.Font;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Globals;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class GetagreementFragment extends Fragment implements MainAsynListener<String> {

    @Bind(R.id.main) RelativeLayout main;
    @Bind(R.id.name) TextView name;
    @Bind(R.id.textView9) TextView textView9;
    @Bind(R.id.othername) TextView othername;
    @Bind(R.id.ll_date) LinearLayout ll_date;
    @Bind(R.id.r1) LinearLayout r1;

    @Bind(R.id.date) TextView date;
    @Bind(R.id.location) EditText location;
    @Bind(R.id.at) EditText at;
    @Bind(R.id.forplace) TextView forplace;
    @Bind(R.id.Cancel) ImageView cancel;
    @Bind(R.id.submit) ImageView submit;
    @Bind(R.id.editsubmit) ImageView editsubmit;
Bundle bundle ;
String Agreementid ;
    private SweetAlertDialog pDialog_logout;
    String Othername,Date,Adress,Loc ,Tittle ,otherid,ExperinceID,expid,Ownerstats;
    Calendar myCalendar ;
    DatePickerDialog.OnDateSetListener dateSetListener ;

    Boolean editsbmt = false ;

    public static GetagreementFragment newInstance() {
        return new GetagreementFragment();
    }

    public GetagreementFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.getagreement, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        GeneralValues.set_logintype(getActivity(),"1") ;
        Globals.fragmentcheck = false;

        Log.e("cfbhhhjdhjhvc","--------Editboll---"+editsbmt);




        getActivity().setRequestedOrientation(
        ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Font.setDefaultFont(getActivity(), "DEFAULT", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "MONOSPACE", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "SANS_SERIF", "font_maaxbold.otf");


        bundle = getActivity().getIntent().getExtras();
        if (bundle != null) {

            Agreementid =     getActivity().getIntent().getStringExtra("agreementid");
            expid=     getActivity().getIntent().getStringExtra("expid");
            Ownerstats=     getActivity().getIntent().getStringExtra("owner_status");

            }


        myCalendar = Calendar.getInstance();

        dateSetListener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        if (Ownerstats.equalsIgnoreCase("true")){
            editsbmt = true ;

            cancel.setAlpha(0.6f);
            submit.setAlpha(0.6f);
            editsubmit.setAlpha(1f);
        }else if (Ownerstats.equalsIgnoreCase("false")){

            editsbmt = false ;

            cancel.setAlpha(1f);
            submit.setAlpha(1f);
            editsubmit.setAlpha(0.6f);
        }



      if (editsbmt == false){
          cancel.setAlpha(1f);
          submit.setAlpha(1f);
          editsubmit.setAlpha(0.6f);
      }else {
          cancel.setAlpha(0.6f);
          submit.setAlpha(0.6f);
          editsubmit.setAlpha(1f);
      }

        location.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.e("Touch",">> Touch  ><<");
                cancel.setAlpha(0.6f);
                submit.setAlpha(0.6f);
                editsubmit.setAlpha(1f);


         /*       if (Loc.equalsIgnoreCase(location.getText().toString())){
                    editsbmt = false ;

                  }else {*/

                    editsbmt = true ;


              //  }

                return false;
            }
        });
        at.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.e("Touch",">> Touch <><> ");
                cancel.setAlpha(0.6f);
                submit.setAlpha(0.6f);
                editsubmit.setAlpha(1f);


             /*   if (Adress.equalsIgnoreCase(at.getText().toString())) {
                    editsbmt = false;

                 } else {*/

                    editsbmt = true;

              //  }

                return false;
            }
        });


    }

    @OnClick(R.id.Cancel) void setCancel(){
        Log.e("cfbhhhjdhjhvc","--------Editboll---"+editsbmt);
        if (editsbmt == true){

        }else {

            pDialog_logout = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE);
            pDialog_logout.setCancelable(true);
            pDialog_logout.setTitleText("Cancel Agreement");
            pDialog_logout.setContentText("Do you want to cancel this agreement?");
            pDialog_logout.setConfirmText("Yes");
            pDialog_logout.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {

                    Globals.getterList = new ArrayList<>();
                    Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));
                    Globals.getterList.add(new ParamsGetter("agreement_id", Agreementid));
                    Globals.getterList.add(new ParamsGetter("status", "R"));
                    Globals.getterList.add(new ParamsGetter("notification_id", expid));
                    new MainAsyncTask(getActivity(), Globals.Webservice.ChangeAggreementStatus, 3, GetagreementFragment.this, "Form", Globals.getterList).execute();
                }
            });

            pDialog_logout.setCancelText("No");
            pDialog_logout.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {

                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog_logout.dismissWithAnimation();

                }
            });

            pDialog_logout.show();

        }

    }

    @OnClick(R.id.editsubmit) void seteditsubmit(){
        Log.e("cfbhhhjdhjhvc","--------Editboll---"+editsbmt);

        if (editsbmt == true){

            Globals.getterList = new ArrayList<>();
            Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));
            Globals.getterList.add(new ParamsGetter("other_user_id", otherid));
            Globals.getterList.add(new ParamsGetter("meeting_date", date.getText().toString()));
            Globals.getterList.add(new ParamsGetter("city", location.getText().toString().trim()));
            Globals.getterList.add(new ParamsGetter("address", at.getText().toString()));
            Globals.getterList.add(new ParamsGetter("agreement_id", Agreementid));
            Globals.getterList.add(new ParamsGetter("notification_id", expid));

            new MainAsyncTask(getActivity(), Globals.Webservice.Editagrement, 2, GetagreementFragment.this, "Form", Globals.getterList).execute();

        }else {


        }
    }

    @OnClick(R.id.submit) void setsubmit(){
        Log.e("cfbhhhjdhjhvc","--------Editboll---"+editsbmt);


        if (editsbmt == true){

        }else {


            pDialog_logout = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE);
            pDialog_logout.setCancelable(true);
            pDialog_logout.setTitleText("Accept Agreement");
            pDialog_logout.setContentText("Do you want to Accept this agreement?");
            pDialog_logout.setConfirmText("Yes");
            pDialog_logout.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {

                    Globals.getterList = new ArrayList<>();
                    Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));
                    Globals.getterList.add(new ParamsGetter("agreement_id", Agreementid));
                    Globals.getterList.add(new ParamsGetter("status", "A"));
                    Globals.getterList.add(new ParamsGetter("notification_id", expid));

                    new MainAsyncTask(getActivity(), Globals.Webservice.ChangeAggreementStatus, 4, GetagreementFragment.this, "Form", Globals.getterList).execute();
                    pDialog_logout.dismissWithAnimation();

                }
            });
            pDialog_logout.setCancelText("No");
            pDialog_logout.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {

                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {

                    pDialog_logout.dismissWithAnimation();

                }
            });

            pDialog_logout.show();
        }
    }



    @OnClick(R.id.ll_date) void setDate(){
        editsbmt =  true ;
        cancel.setAlpha(0.6f);
        submit.setAlpha(0.6f);
        editsubmit.setAlpha(1f);


        try {
         DatePickerDialog dialog =   new DatePickerDialog(getActivity(), dateSetListener, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)) ;

            dialog.getDatePicker().setMinDate(myCalendar.getTimeInMillis());
dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void updateLabel() {

        String myFormat = "dd.MM.yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        date.setText(sdf.format(myCalendar.getTime()));
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        Globals.getterList= new ArrayList<>();
        Globals.getterList.add(new ParamsGetter("agreement_id", Agreementid));
        Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));


        new MainAsyncTask(getActivity(), Globals.Webservice.GetAgreement,1,GetagreementFragment.this,"Form", Globals.getterList).execute();

    }

    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {
        if (isSucess) {
            if (result != null) {

                try {

                    if (flag==1) {

                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");

                        if (status.equalsIgnoreCase("true")) {
                            r1.setVisibility(View.VISIBLE);
                            JSONArray ja = Globals.jsonObj.getJSONArray("data");
                            for (int arr = 0; arr < ja.length(); arr++) {
                                JSONObject json1 = ja.getJSONObject(arr);

                                Othername    = json1.getString("full_name");
                                Date   = json1.getString("meeting_date");
                                Tittle  = json1.getString("title");
                                otherid  = json1.getString("user_id");
                                Loc   = json1.getString("city");
                                Adress   = json1.getString("address");
                                ExperinceID   = json1.getString("experience_id");

                            }

                            name.setText(GeneralValues.get_user_name(getActivity()));
                            othername.setText(Othername);
                            date.setText(Date);
                            at.setText(Adress);
                            location.setText(Loc);
                            forplace.setText(Tittle);

                         }else {
                            r1.setVisibility(View.VISIBLE);
                        }

                    }else if (flag == 2){

                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");

                        if (status.equalsIgnoreCase("true")) {


                            final SweetAlertDialog pDialog_logout = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                            pDialog_logout.setCancelable(false);
                            pDialog_logout.setCanceledOnTouchOutside(false);
                            pDialog_logout.setTitleText("Success");
                            pDialog_logout.setContentText("Agreement Update Successfully");
                            pDialog_logout.setConfirmText("OK");
                            pDialog_logout.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {

                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    pDialog_logout.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                          /*  Intent in = new Intent(getActivity(), Notification_Act.class);
                                            startActivity(in);*/
                                            getActivity().finish();

                                            pDialog_logout.dismissWithAnimation();

                                        }
                                    }, 1000);
                                    }
                                    });

                            pDialog_logout.show();

                            }

                    }else if (flag == 3){

                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");

                        if (status.equalsIgnoreCase("true")) {

                            final SweetAlertDialog pDialog_logout = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                            pDialog_logout.setCancelable(false);
                            pDialog_logout.setCanceledOnTouchOutside(false);
                            pDialog_logout.setTitleText("Success");
                            pDialog_logout.setContentText("Agreement Reject Successfully");
                            pDialog_logout.setConfirmText("OK");
                            pDialog_logout.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {

                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    pDialog_logout.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

//                                            Intent in = new Intent(getActivity(), Notification_Act.class);
//                                            startActivity(in);
                                            getActivity().finish();

                                            pDialog_logout.dismissWithAnimation();
                                        }
                                    }, 1000);
                                }
                            });

                            pDialog_logout.show();

                        }

                    }else if (flag == 4){

                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");

                        if (status.equalsIgnoreCase("true")) {
                            final SweetAlertDialog pDialog_logout = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                            pDialog_logout.setCancelable(false);
                            pDialog_logout.setCanceledOnTouchOutside(false);
                            pDialog_logout.setTitleText("Success");
                            pDialog_logout.setContentText("Agreement Accept Successfully");
                            pDialog_logout.setConfirmText("OK");
                            pDialog_logout.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {

                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    pDialog_logout.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                          /*  Intent in = new Intent(getActivity(), Notification_Act.class);
                                            startActivity(in);*/
                                            getActivity().finish();

                                            pDialog_logout.dismissWithAnimation();
                                        }
                                    }, 1000);
                                }
                            });

                            pDialog_logout.show();


                        }

                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public void onPostError(int flag) {

    }
}
