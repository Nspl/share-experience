package com.shareexperence.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.Display;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.desarrollodroide.libraryfragmenttransactionextended.SlidingRelativeLayout;
import com.shareexperence.R;
import com.shareexperence.Utils.Font;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Globals;
import com.shareexperence.Utils.Methods;
import com.shareexperence.activites.Camera;
import com.shareexperence.activites.MainActivity;
import com.shareexperence.activites.Messages;
import com.shareexperence.activites.Profile;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddexperienceFragment extends Fragment {

    @Bind(R.id.main)
    SlidingRelativeLayout main;
    @Bind(R.id.R1) RelativeLayout r1;
    @Bind(R.id.experience_txt) ImageView experienceTxt;

    @Bind(R.id.input_title) EditText inputTitle;
    @Bind(R.id.input_Keyword) EditText inputKeyword;
    @Bind(R.id.home) ImageView home;
    @Bind(R.id.contact)
    ImageView contact;
    @Bind(R.id.msg) ImageView msg;
    @Bind(R.id.profile) ImageView profile;
    @Bind(R.id.main1)
    LinearLayout main1;

    Animation emptyAnimation;


   // OnSwipeTouchListener swipe;
    GestureDetector gestureDetector;


    public static AddexperienceFragment newInstance() {
        return new AddexperienceFragment();
    }

    public AddexperienceFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.addexperience, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Font.setDefaultFont(getActivity(), "DEFAULT", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "MONOSPACE", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "SANS_SERIF", "font_maaxbold.otf");
        Globals.fragmentcheck = false ;

        main1.setTranslationY(-getWidth());
        main1.setAlpha(0);

        main1.animate().setDuration(600).translationYBy(getWidth()).alpha(1);

        emptyAnimation = AnimationUtils.loadAnimation(getActivity(),
                R.anim.empty_animate);



    }
    public int getWidth() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int w = display.getWidth();
        return w;
    }

    @OnClick(R.id.contact)void setcontact(){
        if (Methods.checkPermissionForCamera(getActivity())){

            if (Methods.checkPermissionForGallary(getActivity())) {
                if (Methods.checkPermissionForAudio(getActivity())){
                    if (Methods.checkPermissionForWritestorage(getActivity())){

                        if (inputTitle.getText().toString().length()==0) {
                            inputTitle.startAnimation(emptyAnimation);
                            Snackbar.make(getView(), "Please Add Title", Snackbar.LENGTH_SHORT).show();
                        }else{
                            Globals.Tittle= inputTitle.getText().toString() ;
                            Globals.Key= inputKeyword.getText().toString() ;

                            Intent in = new Intent(getActivity(), Camera.class);
                            getActivity().startActivity(in);

                        }




                    }
                    else{
                        Methods.requestPermissionForWriteStorage(getActivity());
                    }

                } else{
                    Methods.requestPermissionForAudio(getActivity());
                }

            }else{
                Methods.requestPermissionForGallry(getActivity());
            }

        }else {
            Methods.requestPermissionForCamera(getActivity());
        }



    }


    @OnClick(R.id.profile)void setprofile(){

        GeneralValues.set_profile(getActivity(),"1");

        Intent qn= new Intent (getActivity(), Profile.class);
        startActivity(qn);



    }

    @OnClick(R.id.msg)void setmsg(){
        GeneralValues.set_msgtype(getActivity(),"0");

        Intent qn= new Intent (getActivity(), Messages.class);
        startActivity(qn);


    }
    @OnClick(R.id.home)void sethome(){

        Intent qn= new Intent (getActivity(), MainActivity.class);
        getActivity().startActivity(qn);



    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }




}
