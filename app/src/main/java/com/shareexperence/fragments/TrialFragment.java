package com.shareexperence.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.shareexperence.R;
import com.shareexperence.Utils.Font;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.activites.Home;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TrialFragment extends Fragment {

    @Bind(R.id.up_arrow) ImageView upArrow;
    @Bind(R.id.next_arrow) ImageView nextArrow;
    @Bind(R.id.down_arrow) ImageView downArrow;
    Handler handler ;
    Runnable r;


    public static TrialFragment newInstance() {
        return new TrialFragment();
    }

    public TrialFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.trial, null);


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        GeneralValues.set_logintype(getActivity(),"1");

        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Font.setDefaultFont(getActivity(), "DEFAULT", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "MONOSPACE", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "SANS_SERIF", "font_maaxbold.otf");

        handler=new Handler();
        r=new Runnable() {
            @Override
            public void run() {

                Intent in = new Intent(getActivity(),Home.class);
                startActivity(in);
                getActivity().finish();
            }
        };
        handler.postDelayed(r,2000);


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        handler.removeCallbacks(r);
        ButterKnife.unbind(this);
    }
 
}
