package com.shareexperence.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.desarrollodroide.libraryfragmenttransactionextended.FragmentTransactionExtended;
import com.rey.material.app.BottomSheetDialog;
import com.shareexperence.R;
import com.shareexperence.Utils.CommonUtils;
import com.shareexperence.Utils.Font;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Globals;
import com.shareexperence.Utils.Methods;
import com.shareexperence.activites.MainActivity;
import com.shareexperence.activites.MessageCamera;
import com.shareexperence.activites.Messages;
import com.shareexperence.redcontact.RedcontactFragment;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment implements MainAsynListener<String>,EasyVideoCallback {

    @Bind(R.id.main)
 RelativeLayout main;
    @Bind(R.id.fm) FrameLayout fm;

    @Bind(R.id.profile_image)
    CircleImageView profileImage;
    @Bind(R.id.r2) RelativeLayout r2;
    @Bind(R.id.name) TextView name;
    @Bind(R.id.age) TextView age;
    @Bind(R.id.n3) TextView n3;
    @Bind(R.id.Sex) TextView sex;
    @Bind(R.id.about) TextView about;
    @Bind(R.id.home) ImageView home;
    @Bind(R.id.msg) ImageView msg;
    @Bind(R.id.from) TextView from;
    @Bind(R.id.ll_contact)
    LinearLayout ll_contact;
    /* @Bind(R.id.player_detail)
     VideoPlayerView player_detail;*/
    @Bind(R.id.player)
    EasyVideoPlayer player;
    @Bind(R.id.play)
    ImageView play;

    @Bind(R.id.backgd)
    RelativeLayout backgd;




    @Bind(R.id.thumb)
    ImageView thumb;


    String Userid ;
    BottomSheetDialog mBottomSheetDialog;
    Bundle bundle ;
    String email,Name,location,gender ,Id,username,telephone,hash_tag_search,About,Age,image,video,profile_thumbnail,Tittle,EXPID;




    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    public ProfileFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        GeneralValues.set_msgtype(getActivity(),"0");


        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Font.setDefaultFont(getActivity(), "DEFAULT", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "MONOSPACE", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "SANS_SERIF", "font_maaxbold.otf");


        bundle = getArguments();
        if (bundle != null) {


            if ( bundle.getString("key").equals("other")){


                try {

                    Userid = bundle.getString("userid");
                    Tittle=  bundle.getString("tittle");
                    EXPID= bundle.getString("exprncid");


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }else if ( bundle.getString("key").equals("msg")){
                Userid = bundle.getString("userid");


            }





        }

        player.hideControls();
        player.setCallback(this);
        player.disableControls();


    }

    @OnClick (R.id.ll_contact) void
    setLl_contact(){

        if (Userid.equalsIgnoreCase(GeneralValues.get_user_id(getActivity()))){

        }else {
            Forgot_dialog();
        }



    }


    @OnClick(R.id.play) void setplay(){
        Log.e("vnfevhefjvle","vjefvevjv");

        play.setVisibility(View.VISIBLE);
        //  mVideoPlayerManager.playNewVideo(null, player_detail,video );
        thumb.setVisibility(View.GONE);
        player.setVisibility(View.VISIBLE);

        Uri myUri = Uri.parse(video) ;
        player.setSource(myUri);
    }

    @Override
    public void onResume() {
        super.onResume();
        Globals.getterList= new ArrayList<>();
        Globals.getterList.add(new ParamsGetter("user_id", Userid));


        new MainAsyncTask(getActivity(), Globals.Webservice.Getprofile,1,ProfileFragment.this,"Form", Globals.getterList).execute();
    }


    @OnClick(R.id.home)void sethome(){
        Intent qn= new Intent (getActivity(), MainActivity.class);
        startActivity(qn);
        getActivity().finish();

    }


    @OnClick(R.id.msg)void setmsg(){

        GeneralValues.set_msgtype(getActivity(),"0");

        Intent qn= new Intent (getActivity(), Messages.class);
        startActivity(qn);
        getActivity().finish();
    }




    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }



    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {
        if (isSucess) {
            if (result != null) {
                try {
                    if (flag==1) {

                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        if (status.equalsIgnoreCase("true")) {

                            JSONArray ja = Globals.jsonObj.getJSONArray("data");
                            for (int arr = 0; arr < ja.length(); arr++) {
                                JSONObject json1 = ja.getJSONObject(arr);

                                email = json1.getString("email");
                                Name= json1.getString("full_name");
                                location= json1.getString("location");
                                gender = json1.getString("gender");
                                Id= json1.getString("user_id");
                                Age = json1.getString("age");
                                telephone= json1.getString("telephone");
                                username= json1.getString("username");
                                hash_tag_search= json1.getString("hash_tag_search");
                                About= json1.getString("about");
                                image= json1.getString("image");

                                video= json1.getString("video");
                                profile_thumbnail= json1.getString("profile_thumbnail");
                            }


                            backgd.setBackgroundColor(Color.parseColor("#84BD00"));


                            if (video.equalsIgnoreCase("")&&profile_thumbnail.equalsIgnoreCase("")){
                                play.setVisibility(View.INVISIBLE);
                                thumb.setVisibility(View.INVISIBLE);
                            }else {
                                thumb.setVisibility(View.VISIBLE);
                                play.setVisibility(View.VISIBLE);
                            }



                            name.setText(Name);
                            if (gender.equalsIgnoreCase("M")){
                                sex.setText("Male");
                            }else if (gender.equalsIgnoreCase("F")){
                                sex.setText("Female");
                            }
                            from.setText(location);
                            about.setText(About);


                            try {
                                CommonUtils.getDisplayImage(getActivity(),image,profileImage);
                                CommonUtils.getDisplayImage(getActivity(),profile_thumbnail,thumb);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                String changeAge =      CommonUtils.getAge(Age);

                                 age.setText(changeAge);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }


                        }else{

                        }
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public void onPostError(int flag) {

    }

    public void Forgot_dialog() {
        mBottomSheetDialog = new BottomSheetDialog(getActivity(), R.style.Material_App_BottomSheetDialog);
        View dialog = LayoutInflater.from(getActivity()).inflate(R.layout.cotact_dialog, null);


        LinearLayout withname = (LinearLayout) dialog.findViewById(R.id.withname);
        LinearLayout contactus = (LinearLayout) dialog.findViewById(R.id.contactus);
        TextView TouchRed = (TextView) dialog.findViewById(R.id.TouchRed);

        TextView Touchblue = (TextView) dialog.findViewById(R.id.Touchblue);

        TouchRed.setText("WITH " + Name );
        Touchblue.setText("WITH " + Name);

        LinearLayout cancel = (LinearLayout) dialog.findViewById(R.id.cancel);

        // if decline button is clicked, close the custom dialog
        withname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBottomSheetDialog.dismiss();


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (Methods.checkPermissionForCamera(getActivity())){

                            if (Methods.checkPermissionForGallary(getActivity())) {
                                if (Methods.checkPermissionForAudio(getActivity())){
                                    if (Methods.checkPermissionForWritestorage(getActivity())){

                                        Intent in = new Intent(getActivity(), MessageCamera.class);
                                        in.putExtra("userid",Userid);
                                        in.putExtra("tittle",Tittle);
                                        in.putExtra("exprncid",EXPID);
                                        getActivity().startActivity(in);

                                    }
                                    else{
                                        Methods.requestPermissionForWriteStorage(getActivity());
                                    }

                                } else{
                                    Methods.requestPermissionForAudio(getActivity());
                                }

                            }else{
                                Methods.requestPermissionForGallry(getActivity());
                            }

                        }else {
                            Methods.requestPermissionForCamera(getActivity());
                        }

                    }
                }, 400);


            }
        });

        // if decline button is clicked, close the custom dialog

        contactus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBottomSheetDialog.dismiss();


                bundle = new Bundle();
                bundle.putString("Name",Name);
                bundle.putString("otherid",Id);



                RedcontactFragment fragments = new RedcontactFragment();
                fragments.setArguments(bundle);

                Globals.FRAGMENT_MANAGER = getActivity().getFragmentManager();
                Globals.fragmentTransaction = Globals.FRAGMENT_MANAGER.beginTransaction();
                FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(getActivity(), Globals.fragmentTransaction, MyprofileFragment.newInstance(), fragments , Globals.frag_id);
                fragmentTransactionExtended.addTransition(FragmentTransactionExtended.FLIP_HORIZONTAL);
                fragmentTransactionExtended.commit();


            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBottomSheetDialog.dismiss();

            }
        });


        mBottomSheetDialog.contentView(dialog)
                .show();


    }




    @Override
    public void onStarted(EasyVideoPlayer player) {

    }

    @Override
    public void onPaused(EasyVideoPlayer player) {

    }

    @Override
    public void onPreparing(EasyVideoPlayer player) {

    }

    @Override
    public void onPrepared(EasyVideoPlayer player) {
        play.setVisibility(View.INVISIBLE);
        thumb.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(EasyVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(EasyVideoPlayer player) {
        try {
            this.player.reset();
        } catch (Exception e) {
            e.printStackTrace();
        }

        play.setVisibility(View.INVISIBLE);
        thumb.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onRetry(EasyVideoPlayer player, Uri source) {

    }

    @Override
    public void onSubmit(EasyVideoPlayer player, Uri source) {

    }
}
