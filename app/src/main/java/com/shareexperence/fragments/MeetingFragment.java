package com.shareexperence.fragments;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.desarrollodroide.libraryfragmenttransactionextended.FragmentTransactionExtended;
import com.desarrollodroide.libraryfragmenttransactionextended.SlidingRelativeLayout;
import com.shareexperence.R;
import com.shareexperence.Utils.Font;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Globals;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class MeetingFragment extends Fragment implements MainAsynListener<String> {

    @Bind(R.id.main)
    SlidingRelativeLayout main;
    @Bind(R.id.name) TextView name;
    @Bind(R.id.textView9) TextView textView9;
    @Bind(R.id.othername) TextView othername;
    @Bind(R.id.date) TextView date;
    @Bind(R.id.ll_date)
    LinearLayout ll_date;
    private SweetAlertDialog pDialog_logout;

    @Bind(R.id.location) EditText location;
    @Bind(R.id.at) EditText at;
    @Bind(R.id.forplace) TextView forplace;
    @Bind(R.id.Cancel) ImageView cancel;
    @Bind(R.id.submit)
    ImageView submit;
Bundle bundle ;
    Calendar myCalendar ;
    DatePickerDialog.OnDateSetListener dateSetListener ;
String otherid ,Name,Tittle,expid,image ;

    public static MeetingFragment newInstance() {

        return new MeetingFragment();

    }

    public MeetingFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.meeting, null);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Font.setDefaultFont(getActivity(), "DEFAULT", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "MONOSPACE", "font_maaxbold.otf");
        Font.setDefaultFont(getActivity(), "SANS_SERIF", "font_maaxbold.otf");



        Globals.fragmentcheck = false ;
        GeneralValues.set_logintype(getActivity(),"1");


        bundle = getArguments();
        if (bundle != null) {

            try {

                otherid = bundle.getString("otherid");
                Name=bundle.getString("name");
                 Tittle=bundle.getString("tittle");
                expid=bundle.getString("expid");
                image=bundle.getString("image");

                Globals.ID = otherid ;
                Tittle = Tittle ;
                //

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        name.setText(GeneralValues.get_user_name(getActivity()));

        othername.setText(Name);
         forplace.setText(Tittle);


        myCalendar = Calendar.getInstance();

        dateSetListener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

    }

    @OnClick(R.id.Cancel) void setCancel(){
        pDialog_logout = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE);
        pDialog_logout.setCancelable(true);
        pDialog_logout.setTitleText("Cancel Agreement");
        pDialog_logout.setContentText("Do you want to cancel this agreement?");
        pDialog_logout.setConfirmText("Yes");
        pDialog_logout.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {

                pDialog_logout.dismissWithAnimation();

                getFragmentManager().popBackStack();

            }
        });
        pDialog_logout.setCancelText("No");
        pDialog_logout.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {

            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog_logout.dismissWithAnimation();

            }
        });

        pDialog_logout.show();

    }

    @OnClick(R.id.submit) void setsubmit(){


        if (date.getText().toString().length() == 0)
        {
            Snackbar.make(getView(), "Please Select Date", Snackbar.LENGTH_SHORT).show();

        }else if(location.getText().toString().length() == 0){
            Snackbar.make(getView(), "Please enter Town Name", Snackbar.LENGTH_SHORT).show();

        }


        else if(at.getText().toString().length()==0){
            Snackbar.make(getView(), "Please enter Specify Address", Snackbar.LENGTH_SHORT).show();

        }
       else {
            pDialog_logout = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE);
            pDialog_logout.setCancelable(true);
            pDialog_logout.setTitleText("Send Agreement");
            pDialog_logout.setContentText("Do you want to Send this agreement?");
            pDialog_logout.setConfirmText("Yes");
            pDialog_logout.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    Globals.getterList = new ArrayList<>();
                    Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(getActivity())));
                    Globals.getterList.add(new ParamsGetter("other_user_id", otherid));
                    Globals.getterList.add(new ParamsGetter("meeting_date", date.getText().toString()));
                    Globals.getterList.add(new ParamsGetter("city", location.getText().toString().trim()));
                    Globals.getterList.add(new ParamsGetter("address", at.getText().toString()));
                    Globals.getterList.add(new ParamsGetter("experience_id", expid));

                    new MainAsyncTask(getActivity(), Globals.Webservice.SubmitAgreement, 1, MeetingFragment.this, "Form", Globals.getterList).execute();
                    pDialog_logout.dismissWithAnimation();
                }
            });
            pDialog_logout.setCancelText("No");
            pDialog_logout.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {

                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog_logout.dismissWithAnimation();

                }
            });

            pDialog_logout.show();

        }
    }


    @OnClick(R.id.ll_date) void setll_date(){
        Log.e("vdcvvfdjv","dgcgejhfkhfv");

        DatePickerDialog dialog  =   new DatePickerDialog(getActivity(), dateSetListener, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)) ;
      dialog.getDatePicker().setMinDate(myCalendar.getTimeInMillis());
        dialog.show();
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();

        ButterKnife.unbind(this);


    }


    private void updateLabel() {

        String myFormat = "dd.MM.yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        date.setText(sdf.format(myCalendar.getTime()));
    }


    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {
        if (isSucess) {
            if (result != null) {
                try {
                    if (flag==1) {

                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        if (status.equalsIgnoreCase("true")) {


                            final SweetAlertDialog pDialog_logout = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                            pDialog_logout.setCancelable(false);
                            pDialog_logout.setCanceledOnTouchOutside(false);
                            pDialog_logout.setTitleText("Success");
                            pDialog_logout.setContentText("Agreement Submit Successfully !");
                            pDialog_logout.setConfirmText("OK");
                            pDialog_logout.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {

                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    pDialog_logout.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            bundle = new Bundle();

                                            bundle.putString("otherid",otherid);
                                            bundle.putString("name",Name);
                                            bundle.putString("image",image);

                                            bundle.putString("tittle",Tittle);
                                            bundle.putString("expid",expid);

                                            SinglechatFragment fragments = new SinglechatFragment();
                                            fragments.setArguments(bundle);

                                            Globals.FRAGMENT_MANAGER = getActivity().getFragmentManager();
                                            Globals.fragmentTransaction = Globals.FRAGMENT_MANAGER.beginTransaction();
                                            FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(getActivity(), Globals.fragmentTransaction, MyprofileFragment.newInstance(), fragments , Globals.frag_id);
                                            fragmentTransactionExtended.addTransition(FragmentTransactionExtended.SLIDE_HORIZONTAL);
                                            fragmentTransactionExtended.commit();


                                            pDialog_logout.dismissWithAnimation();
                                        }
                                    }, 1000);
                                }
                            });

                            pDialog_logout.show();










                        }else{

                        }

                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        }

    }

    @Override
    public void onPostError(int flag) {

    }
}
