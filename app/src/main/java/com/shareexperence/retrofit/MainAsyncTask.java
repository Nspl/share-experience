package com.shareexperence.retrofit;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.shareexperence.R;
import com.shareexperence.Utils.CommonUtils;

import java.util.List;

public class MainAsyncTask extends AsyncTask<String, Void, String> {

	MainAsynListener<String> listener;
	int receivedId;
	boolean  isSuccess = false;
	Context context;
	public CommonFunctions sSetconnection;
	String url, tag;
	List<ParamsGetter> getterList;
	Boolean isProgress;
	Dialog dialogBox;

	/**
	 * @param context
	 * @param url
	 * @param receivedId
	 * @param listener

	 */
	public MainAsyncTask(Context context, String url, int receivedId,
						 MainAsynListener<String> listener, String tag, List<ParamsGetter> getterList) {
		
		this.context = context;
		this.url = url;
		this.receivedId = receivedId;
		this.listener = listener;
		this.tag=tag;
		this.getterList=getterList;
		this.isProgress=true;
	}
public MainAsyncTask(Context context, String url, int receivedId,
						 MainAsynListener<String> listener, String tag, List<ParamsGetter> getterList,Boolean isProgress) {

		this.context = context;
		this.url = url;
		this.receivedId = receivedId;
		this.listener = listener;
		this.tag=tag;
		this.getterList=getterList;
		this.isProgress=isProgress;
	}

	@Override
	protected void onPreExecute() {
		sSetconnection = new CommonFunctions();
		if(isProgress) {
			/*if (context instanceof AppCompatActivity) {
				((AppCompatActivity) context).findViewById(R.id.prgrss_tab).setVisibility(View.VISIBLE);
				((CustomProgressBar) ((AppCompatActivity) context).findViewById(R.id.prgrss_tab)).startLoading();
			} else if (context instanceof FragmentActivity) {
				((FragmentActivity) context).findViewById(R.id.prgrss_tab).setVisibility(View.VISIBLE);
				((CustomProgressBar) ((FragmentActivity) context).findViewById(R.id.prgrss_tab)).startLoading();

			}*/
			try {
				if(dialogBox!=null){
					if (dialogBox.isShowing()) {
						cancelCommonDialog();

					}
				}

				showCommonDialog(context,"Processing...");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected String doInBackground(String... arg0) {
		String mResult = null;

//		JSONObject json = new JSONObject();

		
		try {
			if(CommonUtils.getConnectivityStatus(context)) {
				//mResult = CommonFunctions.getOkHttpClient(url, receivedId, tag, getterList);
		 	mResult = CommonFunctions.getOkHttpClient(url, receivedId, tag, getterList);
			}else{

				Handler handler = new Handler(Looper.getMainLooper());

				handler.post(new Runnable() {

					@Override
					public void run() {
 			        CommonUtils.openInternetDialog(context);
					}
				});


			}

			if (mResult != null) {
					isSuccess = true;
			} else {
				Handler handler = new Handler(Looper.getMainLooper());

				handler.post(new Runnable() {

				        @Override
				        public void run() {
//				        	CommonUtilities.showMessage("Please Try Again..", context,Toast.LENGTH_SHORT);
				            Toast.makeText(context, "Please Try Again..", Toast.LENGTH_SHORT).show();
				        }
				    });     
           
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return mResult;
	}

	/**
	 * activateDriverCheckResponse 1 = flag(Email does not Exist) 2 = Error with
	 * HTTP connection 3 = Error while convert into string 4 = Failure 5 = Email
	 * Already Exist
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	protected void onPostExecute(String _result) {
		try {

				if (isSuccess) {
					if(_result==null){                
					Handler handler = new Handler(Looper.getMainLooper());

					handler.post(new Runnable() {

					        @Override
					        public void run() {
//					        	CommonUtilities.showMessage("Please Try Again..", context,Toast.LENGTH_SHORT);
					            Toast.makeText(context, "Please Try Again..", Toast.LENGTH_SHORT).show();
					        }
					    }); 
					}
					Log.e("result><><><>",""+_result);
				listener.onPostSuccess(_result, receivedId, isSuccess);
				} else {
					listener.onPostError(receivedId);
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(isProgress) {
		/*	if (context instanceof AppCompatActivity) {
				((AppCompatActivity) context).findViewById(R.id.prgrss_tab).setVisibility(View.GONE);
				((CustomProgressBar) ((AppCompatActivity) context).findViewById(R.id.prgrss_tab)).stopLoading();
			} else if (context instanceof FragmentActivity) {
				((FragmentActivity) context).findViewById(R.id.prgrss_tab).setVisibility(View.GONE);
				((CustomProgressBar) ((FragmentActivity) context).findViewById(R.id.prgrss_tab)).stopLoading();

			}*/
			try {
				cancelCommonDialog();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

    @Override
    protected void onCancelled() {
    	// TODO Auto-generated method stub

    	super.onCancelled();

    }
    // cancel progress dialog

	public  void showCommonDialog(Context mContext, String message) {
		 dialogBox = new Dialog(mContext,android.R.style.Theme_Translucent_NoTitleBar);
		dialogBox.setContentView(R.layout.layout_progress_bar);
		dialogBox.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialogBox.setCancelable(false);
	    TextView mMessage = (TextView) dialogBox.findViewById(R.id.message);
		mMessage.setText(message);

		try {
			if(!((Activity) context).isFinishing())
			{
				dialogBox.show();
				//show dialog
			}else{
				Log.e("FINISHED","FINISHED");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public  void cancelCommonDialog() {
		try {
			if (dialogBox != null) {
				if (dialogBox.isShowing()) {
					dialogBox.cancel();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}

