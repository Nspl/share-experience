package com.shareexperence.retrofit;

public interface MainAsynListener<T> {

	void onPostSuccess(T result, int flag, boolean isSucess);

	void onPostError(int flag);

}
