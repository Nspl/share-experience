package com.shareexperence.retrofit;


import android.annotation.SuppressLint;
import android.util.Log;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.List;


@SuppressLint("SimpleDateFormat")
public class CommonFunctions{

	//http://142.4.10.93/~vooap/share_experience/webservices/
 	private static String baseUrl = "http://www.xurf5terre.it/xurf/webservices/" ;
	static MainAsynListener<String> listener;

	public CommonFunctions() {

	}

	/*public Call<T>  getRetrofitClient(Class<T> gettersetter) {
		if (gitApiInterface == null) {

			OkHttpClient okClient = new OkHttpClient();

			Retrofit client = new Retrofit.Builder()
					.baseUrl(baseUrl)
					.client(okClient)

					.addConverterFactory(JacksonConverterFactory.create())
					.build();
			gitApiInterface = client.create(WebservicesInterface.class);
		}

		return gitApiInterface;
	}*/
	public static String getOkHttpClient(String Url,int flag, String tag, List<ParamsGetter> getters) {
		String strResponse="";
		try {
			OkHttpClient okk=new OkHttpClient();
			Request request=null;
		Log.e("WEbsrvice inkkk",baseUrl+Url);

			if(tag.equals("")) {
				 request = new Request.Builder()
						.url(baseUrl+Url)
						.build();
			}
			if(tag.equalsIgnoreCase("Form")){
				FormEncodingBuilder formbody= new FormEncodingBuilder();
				for (int i = 0; i <getters.size() ; i++) {
					Log.e("KEY VLAUES",">>>>    "+getters.get(i).getKey()+"            "+getters.get(i).getValues());
					formbody.add(getters.get(i).getKey(),getters.get(i).getValues());
				}
				RequestBody req=formbody.build();
				 request = new Request.Builder()
 						 .addHeader("Content-Type", "application/json; charset=utf-8")
						.url(baseUrl+Url)
						.post(req)
						.build();
			}
			if(tag.equalsIgnoreCase("Multipart")){
				final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
				final MediaType MEDIA_TYPE_Video = MediaType.parse("video/*");
				MultipartBuilder builder=new MultipartBuilder();
				for (int i = 0; i <getters.size() ; i++) {
					if(getters.get(i).getFile()!=null) {
						if (getters.get(i).getFile().getAbsolutePath().endsWith(".png") || getters.get(i).getFile().getAbsolutePath().endsWith(".jpg")) {
							builder.type(MultipartBuilder.FORM).addFormDataPart(getters.get(i).getKey(), getters.get(i).getFile().getName(), RequestBody.create(MEDIA_TYPE_PNG, getters.get(i).getFile()));
						}
						if (getters.get(i).getFile().getAbsolutePath().endsWith(".mp4") || getters.get(i).getFile().getAbsolutePath().endsWith(".mpeg")|| getters.get(i).getFile().getAbsolutePath().endsWith(".3gp")|| getters.get(i).getFile().getAbsolutePath().endsWith(".avi")) {
							builder.type(MultipartBuilder.FORM).addFormDataPart(getters.get(i).getKey(), getters.get(i).getFile().getName(), RequestBody.create(MEDIA_TYPE_Video, getters.get(i).getFile()));
							Log.e("ENds With",">>>  "+getters.get(i).getFile().getAbsolutePath());
						}
					}else {
 						Log.e("Key SEND Without Image",getters.get(i).getKey() + "<><>><"+getters.get(i).getValues());
						builder.type(MultipartBuilder.FORM).addFormDataPart(getters.get(i).getKey(), getters.get(i).getValues());
					}
				}
				RequestBody req=builder.build();
				request = new Request.Builder()
						.url(baseUrl+Url)

						.post(req)
						.build();

			}

			Response response = okk.newCall(request).execute();
			strResponse=response.body().string();
			Log.e("String Response",">>>>   "+strResponse);
		} catch (IOException e) {
			e.printStackTrace();
			listener.onPostError(flag);
		}

 		return strResponse;
	}

 }
