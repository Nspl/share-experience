package com.shareexperence.twitter;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Window;

import com.shareexperence.Utils.Globals;
import com.shareexperence.fragments.SignupFragment;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;

import oauth.signpost.OAuthProvider;
import oauth.signpost.basic.DefaultOAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import twitter4j.DirectMessage;
import twitter4j.IDs;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;


public class TwitterApp
{
	public static Twitter mTwitter;
	public static TwitterSession mSession;
    public AccessToken mAccessToken;
    private CommonsHttpOAuthConsumer mHttpOauthConsumer;
    private OAuthProvider mHttpOauthprovider;
    private ProgressDialog mProgressDlg;
    private TwDialogListener mListener;
    private Activity context;

   static String twitterId = "";

   //  public static final String  OAUTH_CALLBACK_SCHEME = "http";
//     public static final String  OAUTH_CALLBACK_HOST = "twitterapp.com/";
     public static final String  CALLBACK_URL= "http://wallflyer.com/wallflyer/webservices/addWallflyer"; //OAUTH_CALLBACK_SCHEME + "://" + OAUTH_CALLBACK_HOST;
     private static final String TWITTER_ACCESS_TOKEN_URL = "https://api.twitter.com/oauth/access_token";
     private static final String TWITTER_AUTHORZE_URL = "https://api.twitter.com/oauth/authorize";
     private static final String TWITTER_REQUEST_URL = "https://api.twitter.com/oauth/request_token";
     public static String userSecret = "", userKey = "";
     String access_token_url;
     
     IDs followersIds;   
     long lCursor = -1;
     public String contactid, nameList, profileimageUrl;
     public static String userId = "", secret = "";
     public static long[] iCheck;
     
     
    //http://twitterapp.com/
    public TwitterApp(Activity context, String consumerKey, String secretKey) {
        this.context = context;
       // // Log.e("test","Twitterapp");
        mTwitter = new TwitterFactory().getInstance();
        mSession = new TwitterSession(context);
      //  // Log.e("test",mSession.toString());
        mProgressDlg = new ProgressDialog(context);

        mProgressDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);

        Globals.mConsumerKey = consumerKey;
        Globals.mSecretKey = secretKey;

        mHttpOauthConsumer = new CommonsHttpOAuthConsumer(Globals.mConsumerKey,
        		Globals.mSecretKey);
        
        String request_url=TWITTER_REQUEST_URL;
        access_token_url=TWITTER_ACCESS_TOKEN_URL;
        String authorize_url=TWITTER_AUTHORZE_URL;
        
        mHttpOauthprovider = new DefaultOAuthProvider(
                request_url,
                access_token_url,
                authorize_url);
        mAccessToken = mSession.getAccessToken();
    	Log.e("", "---mAccessToken: " +mAccessToken);
         configureToken();
    }

    public void setListener(TwDialogListener listener) {
        mListener = listener;
    }

    private void configureToken() {
    	
    	if (mAccessToken != null) {
    	//	Log.e("", "---mConsumerKey: " +Constant.mConsumerKey + "--- mSecretKey: "+Constant.mSecretKey);
            mTwitter.setOAuthConsumer(Globals.mConsumerKey, Globals.mSecretKey);
            mTwitter.setOAuthAccessToken(mAccessToken);
         
        }
    }

    public boolean hasAccessToken() {
        return (mAccessToken == null) ? false : true;
    }

    public void resetAccessToken() {
        if (mAccessToken != null) {
            mSession.resetAccessToken();

            mAccessToken = null;
        }
    }

    
    public static String getUsername() {
    	// Log.e("Username",mSession.getUsername().toString());
        return mSession.getUsername();

    }

    public void updateStatus(String status) throws Exception {
        try {
            mTwitter.updateStatus(status);
        } catch (TwitterException e) {
            throw e;
        }
    }

    public static String getTwitterID (){

        return twitterId;

    }
    public void authorize() {
        mProgressDlg.setMessage("Initializing ...");
        mProgressDlg.show();

        new Thread() 
        {
            @Override
            public void run()
            {
                String authUrl = "";
                int what = 1;
                try
                {
                    authUrl = mHttpOauthprovider.retrieveRequestToken(mHttpOauthConsumer, CALLBACK_URL);
                     Log.e("AuhToken=====","AuhToken=====: "+authUrl);
                    what = 0;
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
                mHandler.sendMessage(mHandler.obtainMessage(what, 1, 0, authUrl));
            }
        }.start();
    }

    public void processToken(String callbackUrl)
    {
        try {
            mProgressDlg.setMessage("Finalizing ...");
            mProgressDlg.show();

        }catch (Exception e){

        }

        final String verifier = getVerifier(callbackUrl);
        
        new Thread()
        {
            @Override
            public void run()
            {
                int what = 1;

                try {
                    mHttpOauthprovider.retrieveAccessToken(mHttpOauthConsumer,verifier);

                    mAccessToken = new AccessToken(
                            mHttpOauthConsumer.getToken(),
                            mHttpOauthConsumer.getTokenSecret());

                    
                  //  userKey = mHttpOauthConsumer.getToken().substring(11, mHttpOauthConsumer.getToken().length());
                    userKey = mHttpOauthConsumer.getToken();
                    userSecret = mHttpOauthConsumer.getTokenSecret();

                    twitterId = userKey;
                    getTwitterID();
                    Log.e("UserKey=======",""+userKey);

                   // String[] aa =  userKey.split("-"); 
                    
                  //   Log.e("UserKey2=======",""+userKey.substring(11, userKey.length()));
                    
                   
                  //  Log.e("userSecret=======","userSecret====: "+userSecret);
                 //   Log.e("userKey=======","userKey====: "+userKey);
                    
                  //   Log.e("mSecretKey=======",""+Constant.mSecretKey);
                  	
                 //    Log.e("Globally.SECRET========",""+Globally.SECRET );


                      Log.e("===getUserId: ","===getUserId: "+mAccessToken.getUserId() );
                     Globals.SECRET = userSecret;  //Globally.mSecretKey;
                     Globals.TOKEN = userKey;
                     
                    configureToken();
                    SignupFragment.twitterId=userKey;
                    User user = mTwitter.verifyCredentials();

                    Log.e("", "getName========" +user.getName());

                    mSession.storeAccessToken(mAccessToken, user.getName());
           
                    what = 0;
                    
                    
                    do {
                        followersIds=mTwitter.getFollowersIDs(mAccessToken.getUserId(), lCursor);
                       }while ((lCursor = followersIds.getNextCursor()) != 0);


                   do
                    {
                        Log.e("===getUserId: ","===foll Id: "+followersIds.getIDs( ));
                	   iCheck = followersIds.getIDs();
                  for (long i : followersIds.getIDs())
                   {   
                  
                	 // contactid  = mTwitter.showUser(i).getId()+"";
                     // nameList = mTwitter.showUser(i).getName();
                    //  profileimageUrl = mTwitter.showUser(i).getProfileImageURL();
                   //   displayName = mTwitter.showUser(i).getScreenName();
                   
	  		        Log.e("", "frnd id: " + mTwitter.showUser(i).getId());
	  		      Log.e("", "frnd getName: " + mTwitter.showUser(i).getName());
	  		    Log.e("", "frnd getScreenName: " + mTwitter.showUser(i).getScreenName());
                 }
                  
                
                   }while(followersIds.hasNext());
                   
                   
                } catch (Exception e) {
                    e.printStackTrace();
                }

                mHandler.sendMessage(mHandler.obtainMessage(what, 2, 0));
            }
        }.start();
    }

    private String getVerifier(String callbackUrl) {
        String verifier = "";

        try {
            callbackUrl = callbackUrl.replace("twitterapp", "http");

            URL url = new URL(callbackUrl);
            String query = url.getQuery();

            String array[] = query.split("&");

            for (String parameter : array) {
                String v[] = parameter.split("=");

                if (URLDecoder.decode(v[0]).equals(
                        oauth.signpost.OAuth.OAUTH_VERIFIER)) {
                    verifier = URLDecoder.decode(v[1]);
                    break;
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return verifier;
    }

    private void showLoginDialog(String url) {
        final TwDialogListener listener = new TwDialogListener() {

            public void onComplete(String value) {
                processToken(value);
            }

            public void onError(String value) {
                mListener.onError("Failed opening authorization page");
            }
        };

        new TwitterDialog(context, url, listener).show();
    }

    
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
           if(mProgressDlg.isShowing()) {
               mProgressDlg.dismiss();
           }

            if (msg.what == 1) {
                if (msg.arg1 == 1)
                    mListener.onError("Error getting request token");
                else
                    mListener.onError("Error getting access token");
            } else {
                if (msg.arg1 == 1)
                    showLoginDialog((String) msg.obj);
                else
                    mListener.onComplete("");
            }
        }
    };

    public interface TwDialogListener {
        public void onComplete(String value);

        public void onError(String value);
    }
	
    
   public void sendMessageToFriend(ArrayList<String> list){
		/*Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse("https://api.twitter.com/1.1/direct_messages/new.json?text=" + "hello" +
		"%2C%20tworld.%20welcome%20to%201.1.&screen_name= " + "theseancook" ));
		startActivity(i);
		*/
	   
	 //  mTwitter = TwitterFactory.getSingleton();
	//   Status status = mTwitter.updateStatus(latestStatus);
	    
    Twitter	mTwitter = new TwitterFactory().getInstance();
    	 mTwitter.getAuthorization().isEnabled();
     	DirectMessage message;
	    for(int i=0; i<list.size(); i++){
	    	try {
				message = mTwitter.sendDirectMessage(list.get(i), "Hello App Test");
				// System.out.println("Sent: " + " to @" + message.getRecipientScreenName());
			} catch (TwitterException e) {
				e.printStackTrace();
			}
	      
	    }
	    												
	    
	   
																																																													
    
    	
    	
		/* OAuthRequest req;
		 OAuthService s;
		 
		 s = new ServiceBuilder()
		        .provider(TwitterApp.class)
		        .apiKey(APIKEY)
		        .apiSecret(APISECRET)
		        .callback(CALLBACK)
		        .build();
		 req = new OAuthRequest(Verb.POST, "https://api.twitter.com/1/direct_messages/new.json?user_id="+user_id+"&text=my app test");

		s.signRequest(MyTwitteraccesToken, req);
		Response response = req.send();

		   if (response.getBody() != null) {
		    String t=response.getBody();
		    Log.w("twittersent","twittersent"+t);
*/		
    	
    }
	      
		   

	 /*public  int postToTwitteragain(final String msg,String FriendScreenname) {
	        try {
	              String message = msg+"\u0040"+FriendScreenname;  //there has not white space between unicode of @ and Scrrenname      
	            ConfigurationBuilder confbuilder  = new ConfigurationBuilder(); 
	            confbuilder.setOAuthAccessToken(TwitterSession.token).setOAuthAccessTokenSecret(TwitterSession.tokenSecret) 
	                      .setOAuthConsumerKey(mConsumerKey).setOAuthConsumerSecret(mSecretKey); 
	           mTwitter = new TwitterFactory().getOAuthAuthorizedInstance(mConsumerKey, mSecretKey,access_token_url);
	            Log.d("review size","review"+message.length());
	            Status status = (Status) mTwitter.updateStatus(message);
	          Log.d("status",":"+status.toString());
	                 return 1;

	         } catch (Exception e) {
	            e.printStackTrace();
	            return 0;
	        }

	       }
	 */
    
}
    //http://androidcodeexamples.blogspot.in/2011/12/how-to-integrate-twitter-in-android.html