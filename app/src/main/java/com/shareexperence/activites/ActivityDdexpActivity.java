package com.shareexperence.activites;

import android.app.Activity;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.shareexperence.R;
import com.shareexperence.Utils.Globals;
import com.shareexperence.fragments.AddexperienceFragment;

import butterknife.Bind;
import butterknife.ButterKnife;



public class ActivityDdexpActivity    extends Activity {

    @Bind(R.id.frag_container) FrameLayout fragContainer;
    private static final int TIME_INTERVAL = 2000;
    private long mBackPressed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ddexp);
        ButterKnife.bind(this);

        Globals.frag_id = R.id.frag_container;


            if (savedInstanceState == null) {
                Globals.FRAGMENT_MANAGER = getFragmentManager();
                Globals.fragmentTransaction = Globals.FRAGMENT_MANAGER.beginTransaction();
                Globals.fragmentTransaction.add(Globals.frag_id, AddexperienceFragment.newInstance(), "login")
                        .addToBackStack("blink").commit();

            }




    }

    @Override
    public void onBackPressed() {

        if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        } else {

                finish();


        }
    }

}
