package com.shareexperence.activites;

import android.app.Activity;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.shareexperence.R;
import com.shareexperence.Utils.Globals;
import com.shareexperence.fragments.GetagreementFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ActivityGetagreementActivity extends Activity  {
     private static final int TIME_INTERVAL = 2000;
    private long mBackPressed;
    @Bind(R.id.main) RelativeLayout main;
    @Bind(R.id.frag_container) FrameLayout fragContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_getagreement);
        ButterKnife.bind(this);

        Globals.frag_id = R.id.frag_container;


        if (savedInstanceState == null) {
            Globals.FRAGMENT_MANAGER = getFragmentManager();
            Globals.fragmentTransaction = Globals.FRAGMENT_MANAGER.beginTransaction();
            Globals.fragmentTransaction.add(Globals.frag_id, GetagreementFragment.newInstance(), "login")
                    .addToBackStack("blink").commit();

        }




    }

    @Override
    public void onBackPressed() {

        if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        } else {

            finish();


        }
    }

}
