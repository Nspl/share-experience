package com.shareexperence.activites;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.shareexperence.R;
import com.shareexperence.Utils.Globals;
import com.shareexperence.fragments.NotificationListFragment;

public class Notification_Act extends Activity {

    String notistatus  = "0";
    Bundle bundle ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_);


        bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.getString("key").equals("noti")) {

                notistatus = "1" ;
            }  else {

                notistatus = "0" ;
            }

            }

        if (savedInstanceState == null) {
            Globals.FRAGMENT_MANAGER = getFragmentManager();
            Globals.fragmentTransaction = Globals.FRAGMENT_MANAGER.beginTransaction();
            Globals.fragmentTransaction.add(Globals.frag_id, NotificationListFragment.newInstance(), "login")
                    .addToBackStack("blink").commit();


        }



    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Log.e("Status of noti",notistatus) ;

        if (notistatus.equalsIgnoreCase("1")){
            startActivity(new Intent(Notification_Act.this,SettingActivity.class));
            finish();
        }else {

        finish();
        }
    }
}
