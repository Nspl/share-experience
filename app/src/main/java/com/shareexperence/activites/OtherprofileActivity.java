package com.shareexperence.activites;

import android.app.Activity;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.shareexperence.R;
import com.shareexperence.Utils.Globals;
import com.shareexperence.fragments.ProfileFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

public class OtherprofileActivity extends Activity  {

    @Bind(R.id.frag_container) FrameLayout fragContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otherprofile);
        ButterKnife.bind(this);

        Globals.frag_id = R.id.frag_container;



            if (savedInstanceState == null) {
                Globals.FRAGMENT_MANAGER = getFragmentManager();
                Globals.fragmentTransaction = Globals.FRAGMENT_MANAGER.beginTransaction();
                Globals.fragmentTransaction.add(Globals.frag_id, ProfileFragment.newInstance(), "login")
                        .addToBackStack("blink").commit();

            }


    }


    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        } else {


            finish();
        }

 /*       if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        } else {
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                finish();
            }else {
                try {
                    Snackbar.make(getCurrentFocus(),"Tap back button again in order to exit", Snackbar.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(Profile.this, "Tap back button again in order to exit", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
            mBackPressed = System.currentTimeMillis();
        }*/
    }


}
