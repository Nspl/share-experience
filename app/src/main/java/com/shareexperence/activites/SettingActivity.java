package com.shareexperence.activites;

import android.app.Activity;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.shareexperence.R;
import com.shareexperence.Utils.Globals;
import com.shareexperence.fragments.MyprofileFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SettingActivity extends Activity  {

    @Bind(R.id.frag_container) FrameLayout fragContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting);
        ButterKnife.bind(this);

        Globals.frag_id = R.id.frag_container;

            if (savedInstanceState == null) {
                Globals.FRAGMENT_MANAGER = getFragmentManager();
                Globals.fragmentTransaction = Globals.FRAGMENT_MANAGER.beginTransaction();
                Globals.fragmentTransaction.add(Globals.frag_id, MyprofileFragment.newInstance(), "login")
                        .addToBackStack("blink").commit();

        }
    }


    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        } else {


            finish();
        }
      }


}
