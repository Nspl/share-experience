package com.shareexperence.activites;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.shareexperence.R;
import com.shareexperence.Utils.Font;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Gettersetter;
import com.shareexperence.Utils.Globals;
import com.shareexperence.adapter.Adap_myexperince;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyexperienceActivity extends Activity  implements MainAsynListener<String>{

    @Bind(R.id.main) RelativeLayout main;
    @Bind(R.id.back) LinearLayout back;
    @Bind(R.id.list) SwipeMenuListView list;
    @Bind(R.id.comment) TextView comment;

    Adap_myexperince adap ;
    Gettersetter getset ;
    ArrayList<Gettersetter> arrfeed ,listClone;
    String Name,Eventname,Userid,Eventid ,id, thumbnail,video;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myexperience);
        ButterKnife.bind(this);


        arrfeed = new ArrayList<>();
         setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Font.setDefaultFont(this, "DEFAULT", "font_maaxbold.otf");
        Font.setDefaultFont(this, "MONOSPACE", "font_maaxbold.otf");
        Font.setDefaultFont(this, "SANS_SERIF", "font_maaxbold.otf");

    }
    @OnClick(R.id.back) void setBack(){

         finish();

    }
    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {
        if (isSucess) {
            if (result != null) {

                try {

                    if (flag==1) {
                        arrfeed.clear();
                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        arrfeed.clear();
                        if (status.equalsIgnoreCase("true")) {

                            comment.setVisibility(View.GONE);
                            JSONArray ja = Globals.jsonObj.getJSONArray("data");
                            for (int arr = 0; arr < ja.length(); arr++) {
                                JSONObject json1 = ja.getJSONObject(arr);

                                  Eventname  = json1.getString("title");
                                 Eventid   = json1.getString("experience_id");
                                thumbnail = json1.getString("thumbnail");
                                        video= json1.getString("video");



                                getset = new Gettersetter();
                                    getset.setStrtittle(Eventname);
                                 getset.setEventid(Eventid);
                                 getset.setStrimage(thumbnail);
                                getset.setFilevideo(video);

                                arrfeed.add(getset);

                            }

                            Log.e("SIZE oF ARRAY", "---------------"+arrfeed.size());

                            adap = new Adap_myexperince(MyexperienceActivity.this,arrfeed);
                            list.setAdapter(adap);
                            adap.notifyDataSetChanged();

                            SwipeList() ;

                            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                    Intent intent = new Intent(
                                            Intent.ACTION_VIEW,
                                            Uri.parse(arrfeed.get(position).getFilevideo()));
                                    intent.setDataAndType(Uri
                                                    .parse(arrfeed.get(position).getFilevideo()),
                                            "video/mp4");
                                    startActivity(intent);




                                }
                            });
                        }else {
                            comment.setVisibility(View.VISIBLE);
                        }

                    }else  if (flag == 2){
                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        arrfeed.clear();
                        if (status.equalsIgnoreCase("true")) {
                            Getreview();

                        }else {

                        }


                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public void onPostError(int flag) {

    }


    @Override
    public void onResume() {
        super.onResume();

        Getreview();
    }

    public  void Getreview(){

        Globals.getterList= new ArrayList<>();
        Globals.getterList.add(new ParamsGetter("user_id",  GeneralValues.get_user_id(MyexperienceActivity.this)));

        new MainAsyncTask(MyexperienceActivity.this, Globals.Webservice.MyExperiences,1,MyexperienceActivity.this,"Form", Globals.getterList).execute();

    }

    private  void SwipeList(){
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        MyexperienceActivity.this);
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,
                        0xCE)));
                // set item width
                openItem.setWidth(dp2px(90));
                // set item title
                openItem.setTitle("Open");
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        MyexperienceActivity.this);
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));
                // set a icon
                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

// set creator
        list.setMenuCreator(creator);

        list.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // open
                        Intent intent = new Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse(arrfeed.get(position).getFilevideo()));
                        intent.setDataAndType(Uri
                                        .parse(arrfeed.get(position).getFilevideo()),
                                "video/mp4");
                         startActivity(intent);

                        break;
                    case 1:
                    // delete

                        Globals.getterList= new ArrayList<>();
                        Globals.getterList.add(new ParamsGetter("user_id",  GeneralValues.get_user_id(MyexperienceActivity.this)));
                        Globals.getterList.add(new ParamsGetter("experience_id",  arrfeed.get(position).getEventid()));

                        new MainAsyncTask(MyexperienceActivity.this, Globals.Webservice.DeletemyExp,2,MyexperienceActivity.this,"Form", Globals.getterList).execute();

                        break;
                }
                return false;
            }
        });

        // set SwipeListener
        list.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

            @Override
            public void onSwipeStart(int position) {
                // swipe start
            }

            @Override
            public void onSwipeEnd(int position) {
                // swipe end
            }
        });

        // set MenuStateChangeListener
        list.setOnMenuStateChangeListener(new SwipeMenuListView.OnMenuStateChangeListener() {
            @Override
            public void onMenuOpen(int position) {
            }

            @Override
            public void onMenuClose(int position) {
            }
        });

        // other setting
//		listView.setCloseInterpolator(new BounceInterpolator());

        // test item long click
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                Toast.makeText(MyexperienceActivity.this, position + " long click", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

    }



    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

}
