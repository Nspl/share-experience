package com.shareexperence.activites;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Globals;

/**
 * Created by netset on 11/8/16.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;


    public static String TAG="MyFirebaseInstanceIDService";
    @Override
    public void onTokenRefresh() {

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Refreshed token=========: " + refreshedToken);
        Globals.sGCMID=refreshedToken;
        GeneralValues.set_gcmid(getApplicationContext(),refreshedToken);
        Log.e("GET DEVICE ID","======"+GeneralValues.get_gcmid(getApplicationContext()));

    }


}
