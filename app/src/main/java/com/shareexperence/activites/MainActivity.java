package com.shareexperence.activites;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.shareexperence.R;
import com.shareexperence.Utils.Globals;
import com.shareexperence.fragments.ActivityHomeFragment;



public class MainActivity  extends FragmentActivity {

    private static final int TIME_INTERVAL = 2000;
    private long mBackPressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);



        Globals.frag_id = R.id.frag_container;


            if (savedInstanceState == null) {

                Globals.FRAGMENT_MANAGER = getFragmentManager();
                Globals.fragmentTransaction = Globals.FRAGMENT_MANAGER.beginTransaction();
                Globals.fragmentTransaction.add(Globals.frag_id, ActivityHomeFragment.newInstance(), "login")
                        .addToBackStack("blink").commit();

            }





    }

    @Override
    public void onBackPressed() {

        if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        } else {
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                finish();
            }else {
                try {
                    Snackbar.make(getCurrentFocus(),"Tap back button again in order to exit", Snackbar.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, "Tap back button again in order to exit", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
            mBackPressed = System.currentTimeMillis();
        }
    }

}
