package com.shareexperence.activites;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.shareexperence.video.FileUtils;

public class ImgLoader extends Application
		{
	 
	@Override
	public void onCreate() {
		super.onCreate();
		
		try {
			// UNIVERSAL IMAGE LOADER SETUP


			ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
					getApplicationContext())

					.memoryCache(new WeakMemoryCache())
					.discCacheSize(50 * 1024 * 1024).build();

			ImageLoader.getInstance().init(config);
			// END - UNIVERSAL IMAGE LOADER SETUP
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		try {
			FileUtils.createApplicationFolder();
		} catch (Exception e) {
			e.printStackTrace();
		}

	} 


	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);
	}


}
