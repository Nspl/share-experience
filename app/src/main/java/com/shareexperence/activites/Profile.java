package com.shareexperence.activites;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.shareexperence.R;
import com.shareexperence.Utils.Globals;
import com.shareexperence.fragments.EditmyprofileFragment;

public class Profile  extends FragmentActivity {

    private static final int TIME_INTERVAL = 2000;
    private long mBackPressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Globals.frag_id = R.id.frag_container;




            if (savedInstanceState == null) {

                Globals.FRAGMENT_MANAGER = getFragmentManager();
                Globals.fragmentTransaction = Globals.FRAGMENT_MANAGER.beginTransaction();
                Globals.fragmentTransaction.add(Globals.frag_id, EditmyprofileFragment.newInstance(), "login")
                        .addToBackStack("blink").commit();



         }
    }


    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        } else {


            finish();
        }


    }


}
