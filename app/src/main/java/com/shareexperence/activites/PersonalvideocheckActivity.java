package com.shareexperence.activites;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.shareexperence.R;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Globals;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONObject;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PersonalvideocheckActivity extends Activity implements MainAsynListener<String> ,EasyVideoCallback {

    String qq ;
/*    @Bind(R.id.player_detail)
    VideoPlayerView playerDetail;*/
@Bind(R.id.player) EasyVideoPlayer player;
    @Bind(R.id.close)
    ImageView close;

    @Bind(R.id.submit)
    ImageView submit;
    File Videos,Image ;
    String user_id, title, thumbnail, id, video, Username;
    Bundle bundle ;
  /*  private VideoPlayerManager<MetaData> mVideoPlayerManager = new SingleVideoPlayerManager(new PlayerItemChangeListener() {
        @Override
        public void onPlayerItemChanged(MetaData metaData) {

        }
    });*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personalvideocheck);
        ButterKnife.bind(this);


        bundle = getIntent().getExtras();
        if (bundle != null) {


            video = getIntent().getStringExtra("Video");
            thumbnail = getIntent().getStringExtra("thumb");
            title= getIntent().getStringExtra("tittle");

            Log.e("Thumbnail><>",thumbnail);

        }
        Videos = new File(video) ;

        Image = new File(thumbnail);




        new VideoCompressor().execute();

        player.hideControls();
        player.setCallback(this);
        player.disableControls();

        Uri myUri = Uri.parse(video) ;
        player.setSource(myUri);


 /*       playerDetail.addMediaPlayerListener(new SimpleMainThreadMediaPlayerListener() {
            @Override
            public void onVideoPreparedMainThread() {
                // We hide the cover when video is prepared. Playback is about to start


                Log.e("preparing video", "prepare");
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onVideoStoppedMainThread() {
                // We show the cover when video is stopped

                Log.e("Stoped video", "Stoped");
                try {


                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onBufferingUpdateMainThread(int percent) {
                super.onBufferingUpdateMainThread(percent);

                Log.e("buffering----", "bufeer");
                try {



                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onVideoCompletionMainThread() {
                // We show the cover when video is completed


                try {

                    Log.e("completddd----", "compltetttt");
                    mVideoPlayerManager.playNewVideo(null, playerDetail, qq);

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        mVideoPlayerManager.playNewVideo(null, playerDetail, video);

*/

    }

    @OnClick(R.id.close) void setClose(){

        //  Globals.fragmentcheck = true ;
        File fdelete = new File(video);
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                System.out.println("file Deleted :" + video);
            } else {
                System.out.println("file not Deleted :" + video);
            }
        }


        File fdelete1 = new File(thumbnail);
        if (fdelete1.exists()) {
            if (fdelete1.delete()) {
                System.out.println("file Deleted :" + thumbnail);
            } else {
                System.out.println("file not Deleted :" + thumbnail);
            }
        }
        Intent in = new Intent(PersonalvideocheckActivity.this,ProfilevideoCamera.class);
        startActivity(in);
        finish();
    }

    @OnClick(R.id.submit) void setSubmit(){
        //
        Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(PersonalvideocheckActivity.this)));
        Globals.getterList.add(new ParamsGetter("full_name", ""));
        Globals.getterList.add(new ParamsGetter("telephone",  ""));
        Globals.getterList.add(new ParamsGetter("username", ""));
        Globals.getterList.add(new ParamsGetter("gender", ""));
        Globals.getterList.add(new ParamsGetter("location", ""));
        Globals.getterList.add(new ParamsGetter("age", ""));
        Globals.getterList.add(new ParamsGetter("about", ""));
        Globals.getterList.add(new ParamsGetter("hash_tag_search", ""));
        Globals.getterList.add(new ParamsGetter("notification_status", ""));


        if (Videos != null) {
            Log.e("Send image path...", "" + Videos);

            Globals.getterList.add(new ParamsGetter("video", Videos));
        }

        if (Image != null) {
            Log.e("Send image path...", "" + Image);

            Globals.getterList.add(new ParamsGetter("profile_thumbnail", Image));
        }


        new MainAsyncTask(PersonalvideocheckActivity.this, Globals.Webservice.Editprofile, 1, PersonalvideocheckActivity.this, "Multipart", Globals.getterList).execute();


    }

    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {
        if (isSucess) {
            if (result != null) {

                try {
                    Globals.jsonObj = new JSONObject(result);
                    String status = Globals.jsonObj.getString("status");
                    String msg = Globals.jsonObj.getString("message");

                    if (status.equalsIgnoreCase("true")
                            ) {

                        File fdelete = new File(video);
                        if (fdelete.exists()) {
                            if (fdelete.delete()) {
                                System.out.println("file Deleted :" + video);
                            } else {
                                System.out.println("file not Deleted :" + video);
                            }
                        }


                        File fdelete1 = new File(thumbnail);
                        if (fdelete1.exists()) {
                            if (fdelete1.delete()) {
                                System.out.println("file Deleted :" + thumbnail);
                            } else {
                                System.out.println("file not Deleted :" + thumbnail);
                            }
                        }

                        Intent qn = new Intent(PersonalvideocheckActivity.this,Profile.class);
                        startActivity(qn);
                        finish();

                    }

                } catch (Exception e) {
                    // TODO: handle exception
                }
            }

        }
    }

    @Override
    public void onPostError(int flag) {

    }

    @Override
    public void onStarted(EasyVideoPlayer player) {

    }

    @Override
    public void onPaused(EasyVideoPlayer player) {

    }

    @Override
    public void onPreparing(EasyVideoPlayer player) {

    }

    @Override
    public void onPrepared(EasyVideoPlayer player) {

    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(EasyVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(EasyVideoPlayer player) {
        this.player.reset();

         Uri myUri = Uri.parse(video) ;
        this.player.setSource(myUri);

        this.player.setCallback(this);
    }

    @Override
    public void onRetry(EasyVideoPlayer player, Uri source) {

    }

    @Override
    public void onSubmit(EasyVideoPlayer player, Uri source) {

    }

    class VideoCompressor extends AsyncTask<Void,Void,File> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
/* progressBar = new ProgressDialog(GetChat_Activity.this);
progressBar.setTitle("Please wait while video is compressed");
progressBar.show();
progressBar.setCancelable(false);
Log.e("compressing", "Start video compression");*/
        }
        @Override
        protected File doInBackground(Void... voids) {
            Log.e("video before", "" + Videos.length());
            return com.shareexperence.video.MediaController.getInstance().convertVideo(video) ;



            /* com.shareexperence.video.MediaController.getInstance().convertVideo(video);*/
        }
        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
        @Override
        protected void onPostExecute(File compressed) {
            super.onPostExecute(compressed);
// progressBar.dismiss();
            Videos = compressed;
// homework_image.setImageBitmap(thumbBitmap);
            Log.e("video after", "" + Videos.length());
            qq = Videos.toString() ;
// dialog_Image.show();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(PersonalvideocheckActivity.this,ProfilevideoCamera.class);
        startActivity(in);
        finish();
    }

}