package com.shareexperence.activites;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.shareexperence.R;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Globals;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MessagevideocheckActivity extends Activity implements MainAsynListener<String>,EasyVideoCallback {
    @Bind(R.id.player)
    EasyVideoPlayer player;
    String qq ;
/*    @Bind(R.id.player_detail)
    VideoPlayerView playerDetail;*/
    @Bind(R.id.close)
    ImageView close;

    @Bind(R.id.submit)
    ImageView submit;
    File Videos,Image ;
    String user_id, title, thumbnail, id, video, Otherid,expid;
    Bundle bundle ;
    String mydate, output;
    Dialog dialog_Forgot;
    CountDownTimer timer, counter;
    private long mInterval, seconds;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.messagevideocheck);
        ButterKnife.bind(this);


        bundle = getIntent().getExtras();
        if (bundle != null) {

            video = getIntent().getStringExtra("Video");
            thumbnail = getIntent().getStringExtra("thumb");
            Otherid= getIntent().getStringExtra("Otherid");
            title= getIntent().getStringExtra("tittle");
            expid= getIntent().getStringExtra("expid");


           Globals.ID = Otherid ;
           Globals.ExpID = expid ;

         }
        Videos = new File(video) ;

        Image = new File(thumbnail);


        new VideoCompressor().execute();

        player.hideControls();
        player.setCallback(this);
        player.disableControls();

        Uri myUri = Uri.parse(video) ;
        player.setSource(myUri);



     }

    @OnClick(R.id.close) void setClose(){

        File fdelete = new File(video);
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                System.out.println("file Deleted :" + video);
            } else {
                System.out.println("file not Deleted :" + video);
            }
        }


        File fdelete1 = new File(thumbnail);
        if (fdelete1.exists()) {
            if (fdelete1.delete()) {
                System.out.println("file Deleted :" + thumbnail);
            } else {
                System.out.println("file not Deleted :" + thumbnail);
            }
        }

        Intent in = new Intent(MessagevideocheckActivity.this,MessageCamera.class);
        startActivity(in);
        finish();
    }


     @OnClick(R.id.submit) void setSubmit(){
         Dialog();
         counter = new MessagevideocheckActivity.MyCount(4500, 1000);
         counter.start();

    }

    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {
        if (isSucess) {
            if (result != null) {
                 try {

                    Globals.jsonObj = new JSONObject(result);
                    String status = Globals.jsonObj.getString("status");
                    String msg = Globals.jsonObj.getString("message");

                    if (status.equalsIgnoreCase("true") && msg.equalsIgnoreCase("Message Sent successfully")) {

                        GeneralValues.set_msgtype(MessagevideocheckActivity.this,"1");

                        Globals.ID = Otherid ;

                        File fdelete = new File(video);
                        if (fdelete.exists()) {
                            if (fdelete.delete()) {
                                System.out.println("file Deleted :" + video);
                            } else {
                                System.out.println("file not Deleted :" + video);
                            }
                        }


                        File fdelete1 = new File(thumbnail);
                        if (fdelete1.exists()) {
                            if (fdelete1.delete()) {
                                System.out.println("file Deleted :" + thumbnail);
                            } else {
                                System.out.println("file not Deleted :" + thumbnail);
                            }
                        }

                        Intent qn = new Intent(MessagevideocheckActivity.this,Messages.class);
                        qn.putExtra("otherid",Otherid);
                        qn.putExtra("tittle",title);
                        qn.putExtra("expid",expid);

                        startActivity(qn);
                        finish();
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                }
            }

        }
    }


    @Override
    public void onPostError(int flag) {

    }

    @Override
    public void onStarted(EasyVideoPlayer player) {

    }

    @Override
    public void onPaused(EasyVideoPlayer player) {

    }

    @Override
    public void onPreparing(EasyVideoPlayer player) {

    }

    @Override
    public void onPrepared(EasyVideoPlayer player) {

    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(EasyVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(EasyVideoPlayer player) {
        this.player.reset();

        Log.e("EVP-Sample", "PLay Next IF");
        Uri myUri = Uri.parse(video) ;
        this.player.setSource(myUri);

        this.player.setCallback(this);
    }

    @Override
    public void onRetry(EasyVideoPlayer player, Uri source) {

    }

    @Override
    public void onSubmit(EasyVideoPlayer player, Uri source) {

    }



    class VideoCompressor extends AsyncTask<Void,Void,File> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
        @Override
        protected File doInBackground(Void... voids) {
            Log.e("video before", "" + Videos.length());
            return com.shareexperence.video.MediaController.getInstance().convertVideo(video) ;

         }
        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
        @Override
        protected void onPostExecute(File compressed) {
            super.onPostExecute(compressed);
             Videos = compressed;
             Log.e("video after", "" + Videos.length());
            qq = Videos.toString() ;
         }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(MessagevideocheckActivity.this,MessageCamera.class);
        startActivity(in);
        finish();
    }

    public void Dialog() {
        dialog_Forgot = new Dialog(MessagevideocheckActivity.this);
        dialog_Forgot.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_Forgot.setContentView(R.layout.pleasewait_dialog);
        dialog_Forgot.setCanceledOnTouchOutside(true);
        dialog_Forgot.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        dialog_Forgot.onBackPressed();
        dialog_Forgot.show();

        TextView Internetlayout = (TextView) dialog_Forgot
                .findViewById(R.id.textView1);

        ProgressBar Gpslayout = (ProgressBar) dialog_Forgot
                .findViewById(R.id.progressBar);
    }



    public String formatTime(long millis) {
        output = "";
        seconds = millis / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        seconds = seconds % 60;

        hours = hours % 60;

        String secondsD = String.valueOf(seconds);
        String minutesD = String.valueOf(minutes);
        String hoursD = String.valueOf(hours);

        if (seconds < 10)
            secondsD = "0" + seconds;
        if (minutes < 10)
            minutesD = "0" + minutes;

        output = minutesD + " : " + secondsD;

        return output;
    }

    public class MyCount extends CountDownTimer {
        Context mContext;

        public MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        public void onTick(long millisUntilFinished) {
            mInterval = millisUntilFinished;

        }

        public void onFinish() {
            counter.cancel();
            dialog_Forgot.dismiss();


            try {
                Globals.getterList= new ArrayList<>();
                Globals.getterList.add(new ParamsGetter("sender_id", GeneralValues.get_user_id(MessagevideocheckActivity.this)));
                Globals.getterList.add(new ParamsGetter("receiver_id", Otherid));
                Globals.getterList.add(new ParamsGetter("experience_id", expid));
                //
                if (Videos != null) {

                    Globals.getterList.add(new ParamsGetter("video", Videos));
                }

                if (Image != null) {

                    Globals.getterList.add(new ParamsGetter("thumbnail", Image));
                }

                new MainAsyncTask(MessagevideocheckActivity.this, Globals.Webservice.SendMsg, 1, MessagevideocheckActivity.this, "Multipart", Globals.getterList).execute();
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

    }




}