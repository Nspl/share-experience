package com.shareexperence.activites;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.shareexperence.R;
import com.shareexperence.Utils.Font;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Gettersetter;
import com.shareexperence.Utils.Globals;
import com.shareexperence.adapter.Adap_foundagree;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AgreementfoundActivity extends Activity implements MainAsynListener<String> {

    @Bind(R.id.main) RelativeLayout main;
    @Bind(R.id.back) LinearLayout back;
    @Bind(R.id.list) ListView list;
    @Bind(R.id.comment) TextView comment;


    Adap_foundagree adap ;
    Gettersetter getset ;
    ArrayList<Gettersetter> arrfeed ,listClone;
    String Name,Eventname,Userid,Eventid,Label,Profile,Time,otherid,message ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agreementfound);
        ButterKnife.bind(this);

        arrfeed = new ArrayList<>();
       setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Font.setDefaultFont(this, "DEFAULT", "font_maaxbold.otf");
        Font.setDefaultFont(this, "MONOSPACE", "font_maaxbold.otf");
        Font.setDefaultFont(this, "SANS_SERIF", "font_maaxbold.otf");

    }

    @OnClick(R.id.back) void setBack(){

        finish();

    }



    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {
        if (isSucess) {
            if (result != null) {

                try {

                    if (flag==1) {
                        arrfeed.clear();
                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");
                        arrfeed.clear();
                        if (status.equalsIgnoreCase("true")) {

                            comment.setVisibility(View.GONE);
                            JSONArray ja = Globals.jsonObj.getJSONArray("data");
                            for (int arr = 0; arr < ja.length(); arr++) {
                                JSONObject json1 = ja.getJSONObject(arr);


                                Eventid   = json1.getString("agreement_id");

                                message   = json1.getString("msg");

                                getset = new Gettersetter();

                                getset.setTag(message);

                                getset.setEventid(Eventid);

                                arrfeed.add(getset);

                            }

                            Log.e("SIZE oF ARRAY", "---------------"+arrfeed.size());

                            adap = new Adap_foundagree(this,arrfeed);
                            list.setAdapter(adap);
                            adap.notifyDataSetChanged();



                            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                                    Intent i = new Intent(AgreementfoundActivity.this, FoundfreementdetailActivity
                                            .class);
                                    i.putExtra("agreementid",arrfeed.get(position).getEventid());
                                    startActivity(i);




                                }
                            });
                        }else {
                            comment.setVisibility(View.VISIBLE);
                        }

                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public void onPostError(int flag) {

    }


    @Override
    public void onResume() {
        super.onResume();

        Getreview();
    }

    public  void Getreview(){

        Globals.getterList= new ArrayList<>();
        Globals.getterList.add(new ParamsGetter("user_id",  GeneralValues.get_user_id(AgreementfoundActivity.this)));

        new MainAsyncTask(AgreementfoundActivity.this, Globals.Webservice.Foundagreement,1,AgreementfoundActivity.this,"Form", Globals.getterList).execute();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
