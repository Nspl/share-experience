package com.shareexperence.activites;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.desarrollodroide.libraryfragmenttransactionextended.SlidingRelativeLayout;
import com.shareexperence.R;
import com.shareexperence.Utils.Font;
import com.shareexperence.Utils.GeneralValues;
import com.shareexperence.Utils.Globals;
import com.shareexperence.retrofit.MainAsynListener;
import com.shareexperence.retrofit.MainAsyncTask;
import com.shareexperence.retrofit.ParamsGetter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class FoundfreementdetailActivity extends Activity implements MainAsynListener<String> {

    @Bind(R.id.main) SlidingRelativeLayout main;
    @Bind(R.id.r1) LinearLayout r1;
    @Bind(R.id.name) TextView name;
    @Bind(R.id.textView9) TextView textView9;
    @Bind(R.id.othername) TextView othername;
    @Bind(R.id.ll_date) LinearLayout llDate;
    @Bind(R.id.date) TextView date;
    @Bind(R.id.location) TextView location;
    @Bind(R.id.at) TextView at;
    @Bind(R.id.forplace) TextView forplace;

    Bundle bundle ;
    String Agreementid ;
    private SweetAlertDialog pDialog_logout;
    String Othername,Date,Adress,Loc ,Tittle ,otherid,ExperinceID,expid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.foundfreementdetail);
        ButterKnife.bind(this);

         setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Font.setDefaultFont(this, "DEFAULT", "font_maaxbold.otf");
        Font.setDefaultFont(this, "MONOSPACE", "font_maaxbold.otf");
        Font.setDefaultFont(this, "SANS_SERIF", "font_maaxbold.otf");
        bundle =  getIntent().getExtras();
        if (bundle != null) {

            Agreementid =     getIntent().getStringExtra("agreementid");
         }



    }
    @Override
    public void onResume() {
        super.onResume();

        Globals.getterList= new ArrayList<>();
        Globals.getterList.add(new ParamsGetter("agreement_id", Agreementid));
        Globals.getterList.add(new ParamsGetter("user_id", GeneralValues.get_user_id(FoundfreementdetailActivity.this)));


        new MainAsyncTask(FoundfreementdetailActivity.this, Globals.Webservice.GetAgreement,1,FoundfreementdetailActivity.this,"Form", Globals.getterList).execute();

    }

    @Override
    public void onPostSuccess(String result, int flag, boolean isSucess) {
        if (isSucess) {
            if (result != null) {

                try {

                    if (flag==1) {

                        Globals.jsonObj = new JSONObject(result);
                        String status = Globals.jsonObj.getString("status");

                        if (status.equalsIgnoreCase("true")) {
                            r1.setVisibility(View.VISIBLE);
                            JSONArray ja = Globals.jsonObj.getJSONArray("data");
                            for (int arr = 0; arr < ja.length(); arr++) {
                                JSONObject json1 = ja.getJSONObject(arr);

                                Othername    = json1.getString("full_name");
                                Date   = json1.getString("meeting_date");
                                Tittle  = json1.getString("title");
                                otherid  = json1.getString("user_id");
                                Loc   = json1.getString("city");
                                Adress   = json1.getString("address");
                                ExperinceID   = json1.getString("experience_id");

                            }

                            name.setText(GeneralValues.get_user_name(FoundfreementdetailActivity.this));
                            othername.setText(Othername);
                            date.setText(Date);
                            at.setText(Adress);
                            location.setText(Loc);
                            forplace.setText(Tittle);

                        }else {
                            r1.setVisibility(View.VISIBLE);
                        }

                    }




                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public void onPostError(int flag) {

    }
}


