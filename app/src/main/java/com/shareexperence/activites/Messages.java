package com.shareexperence.activites;

import android.app.Activity;
import android.os.Bundle;

import com.shareexperence.R;
import com.shareexperence.Utils.Globals;
import com.shareexperence.fragments.MessagesFragment;

public class Messages extends Activity {
    private static final int TIME_INTERVAL = 2000;
    private long mBackPressed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        Globals.frag_id = R.id.frag_container;
       // Log.e("Messaage status",GeneralValues.get_msgtype(Messages.this));


             if (savedInstanceState == null) {

                Globals.FRAGMENT_MANAGER = getFragmentManager();
                Globals.fragmentTransaction = Globals.FRAGMENT_MANAGER.beginTransaction();
                Globals.fragmentTransaction.add(Globals.frag_id, MessagesFragment.newInstance(), "login")
                        .addToBackStack("blink").commit();

            }



    }

    @Override
    public void onBackPressed() {

        if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        }
        else {


            finish();
        }
    }




}
