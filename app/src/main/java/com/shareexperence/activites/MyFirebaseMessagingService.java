package com.shareexperence.activites;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.shareexperence.R;
import com.shareexperence.Utils.Globals;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by netset on 16/7/16.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    Intent intent;
    PendingIntent pendingIntent;
    private static final String TAG = "MyFirebaseMsgService";

    String eventname = "", msg = "blank", lavel = "", name = "",tittlepush;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage) {

        // TODO(developer): Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        Log.e("------data-----", "" + remoteMessage);
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Map<String, String> data = remoteMessage.getData();
        Log.e("--data--", "" + data);

        sendNotification(data);
    }

    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(Map<String, String> messageBody) {

        Log.e("---messagebody---" + messageBody, TAG);
        try {
        //     intent = new Intent(this, Notification_Act.class);
             Globals.jsonObj = new JSONObject(messageBody);

            name =  Globals.jsonObj.getString("message");
          //  eventname =  Globals.jsonObj.getString("name");
            lavel=  Globals.jsonObj.getString("label");

        } catch (Exception e) {
            e.printStackTrace();
        }

if (lavel.equals("submitagreement")){


    intent = new Intent(this, Notification_Act.class);
    intent.putExtra("key", "noti");
    tittlepush = "Agreement Request" ;

    msg = name   ;


 } else if (lavel.equals("sendmessage")){

    intent = new Intent(this, Messages.class);
    intent.putExtra("key", "noti");
    tittlepush = "New Message" ;

    msg = name   ;

}else if (lavel.equals("updateagreement")){
    intent = new Intent(this, Notification_Act.class);
    intent.putExtra("key", "noti");
    tittlepush = "Update Agreement" ;

    msg = name   ;



}else if (lavel.equals("acceptagreement")){
    intent = new Intent(this, Notification_Act.class);
    intent.putExtra("key", "noti");
    tittlepush = "Accept Agreement" ;

    msg = name   ;

}else if (lavel.equals("rejectagreement")){
    intent = new Intent(this, Notification_Act.class);
    intent.putExtra("key", "noti");
    tittlepush = "Reject Agreement" ;

    msg = name   ;

}




  /* else if (lavel.equalsIgnoreCase("payment done")) {
                intent = new Intent(this, HostTabActivity.class);
                intent.putExtra("to", "myorders");
                payment_amount = messageBody.get("amount");
                String info2 = messageBody.get("info");
                CultureMeal.e("----info---" + info2, TAG);
                try {
                    JSONObject jsonObject = new JSONObject(info);
                    msg = jsonObject.getString("tittle");
                    name = jsonObject.getString("name");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                msg = "Payment of your order " + msg + " of " + "$" + payment_amount + "\n" + " is done by " + name;
            } else if (lavel.equalsIgnoreCase("Host comment")) {
                intent = new Intent(this, HostTabActivity.class);
                intent.putExtra("to", "hostprofile");
                comment = messageBody.get("comment");
                rating = messageBody.get("rating");
                try {
                    JSONObject jsonObject = new JSONObject(info);
                    msg = jsonObject.getString("tittle");
                    name = jsonObject.getString("name");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                msg = name + " commented " + comment + " on " + msg + " meal and give rating " + rating;
            } else if (lavel.equalsIgnoreCase("guest comment")) {
                intent = new Intent(this, GuestTabActivity.class);
                intent.putExtra("to", "guestprofile");
                comment = messageBody.get("comments");
                rating = messageBody.get("rate");
                try {
                    JSONObject jsonObject = new JSONObject(info);
                    msg = jsonObject.getString("tittle");
                    name = jsonObject.getString("name");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                msg = name + " commented " + comment + "  and give rating " + rating + " to you. \n You also give rating and comment to your food in your requests list.";
            }*/

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0  /*Request code*/, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Resources res = this.getResources();
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setContentTitle(tittlepush)
                .setContentText(msg)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher))
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}