package com.shareexperence.activites;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.shareexperence.R;
import com.shareexperence.Utils.CommonUtils;
import com.shareexperence.Utils.Globals;
import com.shareexperence.Utils.Methods;

import java.io.File;
import java.io.IOException;

public class Camera extends  Activity implements View.OnTouchListener, View.OnClickListener {
    private android.hardware.Camera myCamera;
    private MyCameraSurfaceView myCameraSurfaceView;
    private MediaRecorder mediaRecorder;
    HoldToLoadLayout holdToLoadLayout;
    ImageView myButton,cancel_button,msg;
    private SurfaceHolder mHolder;
    boolean recording;
    public static int orientation;
    Bitmap bitmap;
    Canvas canvas;
    Paint paint;
    ImageView front_cam ;
    boolean found = false;
    int i;
    int AllMargin = 20;
    RectF rectf;
    int Corners = 15, width, height;
    int currentCameraId = 1;
    Dialog dialog;
    Button stopreview = null,send,cancel   ;
    String outputFileName ;
    File outFile ;
    ImageView flash ;
    boolean isStop = false, hasFlash;
    private boolean flashmode = false;

    ImageView home ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        recording = false;


        if (Methods.checkPermissionForCamera(Camera.this)){
            myCamera = getCameraInstance();
            if(myCamera == null){
                Toast.makeText(Camera.this,
                        "Fail to get Camera",
                        Toast.LENGTH_LONG).show();
            }

        }else{
            Methods.requestPermissionForCamera(Camera.this);
        }



        myCameraSurfaceView = new MyCameraSurfaceView(this, myCamera);
        FrameLayout myCameraPreview = (FrameLayout)findViewById(R.id.videoview);
        myCameraPreview.addView(myCameraSurfaceView);

        myButton = (ImageView)findViewById(R.id.button2);
        //  myButton.setClickable(false);
        myButton.setOnTouchListener(this);


        holdToLoadLayout = (HoldToLoadLayout) findViewById(R.id.holdToLoadLayout);


        if (holdToLoadLayout != null) {

            Log.e("Duration><<<>< "," "+holdToLoadLayout.getDuration());

            if (holdToLoadLayout.getDuration() != 0){
                holdToLoadLayout.setStrokeWidth(2);
                holdToLoadLayout.setStrokeAlpha(255);
                holdToLoadLayout.setPlayReverseAnimation(false);
                holdToLoadLayout.setStopWhenFilled(false);
                holdToLoadLayout.setColorAnimator(Color.BLUE, Color.RED);
                holdToLoadLayout.setStartAngle(0);
                holdToLoadLayout.setDuration(30000);
                holdToLoadLayout.setFillListener(new HoldToLoadLayout.FillListener() {
                    @Override
                    public void onFull() {
                        stopRecording();

                        try {
                            myButton.setImageResource(R.drawable.camerabutnwhite);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        Bitmap resizedbitmap = ThumbnailUtils.createVideoThumbnail(outputFileName, MediaStore.Video.Thumbnails.MINI_KIND);
                        File thumbfile = new File(CommonUtils.saveimagetosdcard(Camera.this, resizedbitmap));


                        Intent in = new Intent(Camera.this, DemovideoActivity.class);
                        in.putExtra("Video", outFile.toString());
                        in.putExtra("thumb", thumbfile.toString());
                        in.putExtra("tittle", Globals.Tittle);
                        startActivity(in);
                        finish();

                        // dialog.show();
                    }

                    @Override
                    public void onEmpty() {
                        Log.e("Log of empty", "empty");
                    }

                    @Override
                    public void onAngleChanged(float angle) {

                    }

                    @Override
                    public void onPause() {

                        if (outFile.length() <=100){


                            Intent in = new Intent(Camera.this,Camera.class);
                            startActivity(in);
                            finish();
                            Toast.makeText(Camera.this, "Please hold a button for Save video",Toast.LENGTH_SHORT).show();
                        }else {
                            try {
                                myButton.setImageResource(R.drawable.camerabutnwhite);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                stopRecording();



                                Bitmap resizedbitmap = ThumbnailUtils.createVideoThumbnail(outputFileName, MediaStore.Video.Thumbnails.MINI_KIND);
                                File thumbfile = new File(CommonUtils.saveimagetosdcard(Camera.this, resizedbitmap));


                                Intent in = new Intent(Camera.this, DemovideoActivity.class);
                                in.putExtra("Video", outFile.toString());
                                in.putExtra("thumb", thumbfile.toString());
                                in.putExtra("tittle", Globals.Tittle);
                                startActivity(in);
                                finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }


                    }
                });

            }
        }
        home=(ImageView) findViewById(R.id.home);
        front_cam =(ImageView) findViewById(R.id.front_cam);
        front_cam.setOnClickListener(this);

        flash=(ImageView) findViewById(R.id.flash);
        flash.setOnClickListener(this);

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_sendvideo);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setTitle("Send Video");
        send = (Button) dialog.findViewById(R.id.send);
        cancel = (Button) dialog.findViewById(R.id.cancel);

        send.setOnClickListener(this);
        cancel.setOnClickListener(this);

        msg=(ImageView)findViewById(R.id.msg);


        hasFlash = this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
            flash.setVisibility(View.INVISIBLE);
        }

        if (currentCameraId ==  1 ) {
            flash.setVisibility(View.INVISIBLE);
        }

        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Camera.this,Camera.class);
                startActivity(in);
                finish();
            }
        });



        msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iqn = new Intent(Camera.this,Messages.class);
                startActivity(iqn);
                finish();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ipn = new Intent(Camera.this,Home.class);
                startActivity(ipn);
                finish();
            }
        });


    }




    private android.hardware.Camera getCameraInstance(){
        // TODO Auto-generated method stub
        android.hardware.Camera c = null;
        try {
            c = android.hardware.Camera.open(currentCameraId);

            c.setDisplayOrientation(90);// attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    private boolean prepareMediaRecorder(){
        outputFileName = Environment.getExternalStorageDirectory() + "/DCIM/Camera/MyVideo.mp4";
        outFile = new File(outputFileName);



      //  CamcorderProfile cpHigh = CamcorderProfile.get(CamcorderProfile.QUALITY_QVGA);


        myCamera = getCameraInstance();
        mediaRecorder = new MediaRecorder();

        myCamera.unlock();
        mediaRecorder.setCamera(myCamera);

        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);


        try {
            mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_720P));
        } catch (Exception e) {
            e.printStackTrace();
        }
        mediaRecorder.setOutputFile(outputFileName);
        mediaRecorder.setMaxDuration(30000);

        if(currentCameraId == 0) {
            mediaRecorder.setOrientationHint(90);// Set max duration 60 sec.
        }else{
            mediaRecorder.setOrientationHint(270);
        }

        // mediaRecorder.setMaxFileSize(5000000); // Set max file size 5M

   /*     mediaRecorder.setPreviewDisplay(myCameraSurfaceView.getHolder().getSurface());

*/


        mediaRecorder.setPreviewDisplay(myCameraSurfaceView.getHolder().getSurface());
        //  mediaRecorder.setOrientationHint(90);
        Log.e("kghnkjglk",""+orientation);

        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            releaseMediaRecorder();
            return false;
        }
        return true;

    }
    // ==================== Turn Flash light on/off =====================

    void flashOnOff() {
        if (!flashmode) {
            // Enable vibrate
            try {
                myCamera.stopPreview();
                android.hardware.Camera.Parameters p = myCamera.getParameters();
                if (hasFlash) {
                    p.setFlashMode(android.hardware.Camera.Parameters.FLASH_MODE_AUTO);
                }

                myCamera.setParameters(p);
                myCamera.startPreview();
                flashmode = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
             try {
                flashmode = false;
                myCamera.stopPreview();
                android.hardware.Camera.Parameters p = myCamera.getParameters();
                if (hasFlash) {
                    p.setFlashMode(android.hardware.Camera.Parameters.FLASH_MODE_OFF);
                }
                myCamera.setParameters(p);
                myCamera.startPreview();

                flashmode = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        releaseMediaRecorder();       // if you are using MediaRecorder, release it first
        releaseCamera();              // release the camera immediately on pause event
    }

    private void releaseMediaRecorder(){
        if (mediaRecorder != null) {
            mediaRecorder.reset();   // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = null;
            myCamera.lock();           // lock camera for later use
        }
    }

    private void releaseCamera(){
        if (myCamera != null){
            myCamera.release();        // release the camera for other applications
            myCamera = null;
        }
    }

    public class MyCameraSurfaceView extends SurfaceView implements SurfaceHolder.Callback{




        public MyCameraSurfaceView(Context context, android.hardware.Camera camera) {
            super(context);
            myCamera = camera;

            // Install a SurfaceHolder.Callback so we get notified when the
            // underlying surface is created and destroyed.
            mHolder = getHolder();
            mHolder.addCallback(this);
            // deprecated setting, but required on Android versions prior to 3.0
            mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int weight,
                                   int height) {
            // If your preview can change or rotate, take care of those events here.
            // Make sure to stop the preview before resizing or reformatting it.

            if (mHolder.getSurface() == null){
                // preview surface does not exist
                return;
            }

            // stop preview before making changes
            try {
                myCamera.stopPreview();
            } catch (Exception e){
                // ignore: tried to stop a non-existent preview
            }

            // make any resize, rotate or reformatting changes here

            // start preview with new settings
            try {

                myCamera.setPreviewDisplay(mHolder);
                myCamera.startPreview();

            } catch (Exception e){
            }
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            // TODO Auto-generated method stub
            // The Surface has been created, now tell the camera where to draw the preview.


            try {
                myCamera.setPreviewDisplay(holder);
                myCamera.startPreview();
                myCamera.setDisplayOrientation(90);
            } catch (Exception e) {
                e.printStackTrace();
            }



        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            // TODO Auto-generated method stub

        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {

        myButton.setClickable(false);

        try {
            myButton.setImageResource(R.drawable.whitefullcamera);
        } catch (Exception e) {
            e.printStackTrace();
        }


        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:


                if (Methods.checkPermissionForGallary(Camera.this)) {

                    if (recording) {
                        // stop recording and release camera
                        mediaRecorder.stop();  // stop the recording
                        releaseMediaRecorder(); // release the MediaRecorder object

                        //Exit after saved
                        finish();
                    } else {

                        //Release Camera before MediaRecorder start
                        releaseCamera();

                        if (!prepareMediaRecorder()) {
                            Toast.makeText(Camera.this,
                                    "Fail in prepareMediaRecorder()!\n - Ended -",
                                    Toast.LENGTH_LONG).show();
                            finish();
                        }

                        mediaRecorder.start();
                        recording = true;

                    }
                }else{
                    Methods.requestPermissionForGallry(Camera.this);
                }

                break;


            case MotionEvent.ACTION_UP:

                mediaRecorder.stop();

                break;
        }
        return false;

    }


    public void CreateBitmap() {

        bitmap = Bitmap.createBitmap(400, 250, Bitmap.Config.RGB_565);

    }

    public void CreateCanvas() {

        canvas = new Canvas(bitmap);

        canvas.drawColor(Color.TRANSPARENT);

    }

    public void CreatePaint() {

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        paint.setStyle(Paint.Style.FILL);

        paint.setColor(Color.GRAY);

        paint.setAntiAlias(true);

    }

    public void stopRecording() {
        if (mediaRecorder != null) {
            mediaRecorder.setOnErrorListener(null);
            mediaRecorder.setOnInfoListener(null);
            try {
                mediaRecorder.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
            releaseRecorder();
// recordingMsg.setText("Pause");
            releaseCamera();
        }
    }

    public void releaseRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.release();
            mediaRecorder = null;
        }
    }

    @Override
    public void onClick(View view) {

        switch(view.getId()){
            case R.id.front_cam:
                if(myCamera !=null) {
                    myCamera.stopPreview();
                    myCamera.release();
                }

                if(currentCameraId == android.hardware.Camera.CameraInfo.CAMERA_FACING_BACK){
                    currentCameraId = android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT;
                    Log.d(">>currentCameraId", ">>currentCameraId: " + currentCameraId);

                    if (currentCameraId ==  1  ) {
                        flash.setVisibility(View.INVISIBLE);
                    }else if(currentCameraId ==  0  ){
                        flash.setVisibility(View.VISIBLE);
                    }


                }
                else {
                    currentCameraId = android.hardware.Camera.CameraInfo.CAMERA_FACING_BACK;
                    Log.d(">>currentCameraId", ">>currentCameraId: " + currentCameraId);

                    if (currentCameraId ==  0  ) {
                        flash.setVisibility(View.VISIBLE);
                    }else if(currentCameraId ==  1  ){
                        flash.setVisibility(View.INVISIBLE);
                    }


                }

                myCamera = android.hardware.Camera.open(currentCameraId);
                myCamera.setDisplayOrientation(90);


                try {
                    myCamera.setPreviewDisplay(mHolder);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                myCamera.startPreview();


                break;

        /*
*/


            case R.id.send:

                if (dialog != null) {
                    dialog.dismiss();

                    Log.e("Log of output",""+outFile);

                    Bitmap   resizedbitmap = ThumbnailUtils.createVideoThumbnail(outputFileName, MediaStore.Video.Thumbnails.MINI_KIND);
                    File   thumbfile = new File(CommonUtils.saveimagetosdcard(Camera.this,resizedbitmap));




                    dialog.dismiss();
                    onCreate(null);


                    dialog = null;
                }

                break;

            case R.id.cancel:
                dialog.dismiss();
                onCreate(null);

                break;
            case R.id.flash:

                if (Methods.checkPermissionForFlash(Camera.this)){

                    flashOnOff() ;

                }
                else{

                    Methods.requestPermissionForFlash(Camera.this);
                }


                break;

        }

    }


    public static void setCameraDisplayOrientation(Activity activity,
                                                   int cameraId, android.hardware.Camera camera) {

        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();

        android.hardware.Camera.getCameraInfo(cameraId, info);

        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        Camera.orientation=result;
        camera.setDisplayOrientation(result);
    }


    @Override
    protected void onResume() {
        super.onResume();



        Display display = ((WindowManager)
                getApplicationContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = display.getRotation();
        Log.d(">>rotation", ">>>rotation: " + rotation);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }



}